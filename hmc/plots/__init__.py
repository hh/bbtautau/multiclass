# coding: utf-8

"""
Collection of plotting functions.
"""

__all__ = [
    "confusion_plot", "multi_roc_plot", "feature_plot", "correlation_plot", "bin_opt_plot",
    "plot_roc_curves", "plot_feature_ranking",
]


import re
import math
import json
import functools
import array
from collections import OrderedDict

import six
from six.moves import zip

from hmc.util import (
    import_root, create_file_dir, iter_tree, hist_to_array, hist_to_graph, get_graph_maximum,
    update_graph_values,
)


hmc_style = None


def with_hmc_style(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        global hmc_style
        import plotlib.root as r

        if hmc_style is None:
            hmc_style = r.styles.copy("default", "hmc_style")
            hmc_style.style.ErrorX = 0
            hmc_style.x_axis.TitleOffset = 1.22
            hmc_style.y_axis.TitleOffset = 1.48
            hmc_style.legend.TextSize = 20
            hmc_style.style.legend_dy = 0.035
            hmc_style.style.legend_y2 = 0.93

        with r.styles.use("hmc_style"):
            return func(*args, **kwargs)
    return wrapper


@with_hmc_style
def feature_plot(tchains, feature_name, expression, binning, pdf_output_path=None,
        root_output_path=None, yields_output_path=None, tree_name=None, selection=None,
        feature_selection=None, default=None, missing_values=None, x_title="FEATURE",
        y_title="Events", x_labels=None, x_log=False, y_log=False, y_min=None, y_max=None,
        is_flag=False, legend_titles=None, colors=None, show_underflow=True, show_overflow=True,
        stack=False, signal_names=None, data_names=None, hide_data=False, data_range=(-1.e7, 1.e7),
        show_ratio=True, qcd_shape_files=None, qcd_title="QCD", qcd_color="yellow",
        normalize_signals_before_feature_selection=False, show_signal_scale=True,
        category_text=None, top_right_text=None):
    ROOT = import_root()
    import plotlib.root as r
    from plotlib.util import create_random_name
    from order.util import join_root_selection as jrs
    from scinum import Number

    EMPTY = -1.e5

    # get binning quantities
    if isinstance(binning, tuple):
        # (n, xmin, xmax) triplet
        n_bins, x_min, x_max = binning
        binning_args = tuple(binning)
    else:
        # edges
        n_bins = len(binning) - 1
        x_min = binning[0]
        x_max = binning[-1]
        binning_args = n_bins, array.array("f", binning)

    # some input checks
    n = len(tchains)
    assert(pdf_output_path or root_output_path or yields_output_path)
    assert(legend_titles is None or len(legend_titles) == n)
    assert(colors is None or len(colors) == n)
    assert(x_labels is None or len(x_labels) == n_bins)
    assert(not qcd_shape_files or not data_names or len(data_names) == 1)

    # prepare legend titles
    if not legend_titles:
        legend_titles = list(tchains.keys())

    # prepare colors
    if not colors:
        colors = list(range(2, 2 + n))

    # prepare category text
    if category_text:
        if not isinstance(category_text, (list, tuple)):
            category_text = [category_text]
    else:
        category_text = []

    # prepare signal names
    if not signal_names:
        signal_names = []
    elif not isinstance(signal_names, (list, tuple)):
        signal_names = [signal_names]

    # prepare selections
    selection = jrs(selection, op="and") if selection else ""
    feature_selection = jrs(feature_selection, op="and") if feature_selection else ""

    # valudate ratio
    if show_ratio and (not data_names or not stack):
        show_ratio = False

    # setup the canvas and pad
    divide = (1, 2) if show_ratio else (1,)
    canvas, pads = r.routines.create_canvas(divide=divide, pad_props={"Logx": x_log, "Logy": y_log})
    pad1, pad2 = (pads + [None])[:2]

    # setup pad dimensions when the ratio plot is drawn
    pad1_style = r.styles.copy(r.styles.current_style_name, "pad1_style")
    if show_ratio:
        # update pad1 style
        pad1_style.pad.BottomMargin = 0.3
        pad1_style.x_axis.TitleOffset = 100
        pad1_style.x_axis.LabelOffset = 100

    # use the pad1 style
    r.styles.push("pad1_style")
    r.setup_style()
    r.setup_pad(pad1)
    pad1.cd()

    # create a dummy hist that is used to setup axes, etc
    hist_title = ";{};{}".format(x_title, y_title)
    dummy_hist = ROOT.TH1F(create_random_name("dummy"), hist_title, *binning_args)
    r.setup_hist(dummy_hist, pad=pad1)
    dummy_hist.GetYaxis().SetMaxDigits(5)
    if is_flag:
        dummy_hist.GetXaxis().SetNdivisions(n_bins)
    if x_labels:
        for i, x_label in enumerate(x_labels):
            dummy_hist.GetXaxis().SetBinLabel(i + 1, x_label)

    # helpers to setup histogram styles
    def setup_signal_hist(hist, color):
        r.setup_hist(hist, color=color, color_flags="lm")
        hist.hmc_hist_type = "signal"
        hist.hmc_legend_style = "l"

    def setup_background_hist(hist, color):
        if stack:
            r.setup_hist(hist, color=color, color_flags="mf",
                props={"LineWidth": 1, "LineColor": r.styles.colors.black})
            hist.hmc_legend_style = "f"
        else:
            r.setup_hist(hist, color=color, color_flags="lm")
            hist.hmc_legend_style = "l"
        hist.hmc_hist_type = "background"

    def setup_data_hist(hist, color):
        r.setup_hist(hist, color=color, color_flags="lm", props={"MarkerStyle": 20,
            "BinErrorOption": ROOT.TH1.kPoisson if stack else ROOT.TH1.kNormal})
        hist.hmc_legend_style = "lp"
        hist.hmc_hist_type = "data"

    # create histograms, setup and fill histograms
    background_names = []
    background_hists = []
    signal_hists = []
    data_hists = []
    all_hists = []
    draw_labels = []
    y_max_central = 0.
    n_s = 0.
    n_b = 0.
    n_d = 0.

    for (name, tchain), color, legend_title in zip(tchains.items(), colors, legend_titles):
        hist = ROOT.TH1F(create_random_name(name), legend_title, *binning_args)
        hist.hmc_process_name = name
        hist.Sumw2()

        # fill via projection, where tchain is allowed to be a list
        _tchains = tchain if isinstance(tchain, (list, tuple)) else [tchain]
        n_nofs, n_fs = 0., 0.
        for _tchain in _tchains:
            # combine selections with weight
            weight = getattr(_tchain, "hmc_weight", "")
            sel = getattr(_tchain, "hmc_selection", "")
            sel_weight = jrs(jrs(sel, selection, feature_selection, op="and"), weight, op="mul")
            sel_weight_nofs = jrs(jrs(sel, selection, op="and"), weight, op="mul")

            # when stacking and signal normalization should happen before the feature selection
            # is applied, store the number of entries with and without the feature selection
            if stack and feature_selection and name in signal_names \
                    and normalize_signals_before_feature_selection:
                n_nofs += _tchain.GetEntries(sel_weight_nofs)
                n_fs += _tchain.GetEntries(sel_weight)

            # fill the histogram
            if isinstance(expression, six.string_types):
                tmp_name = create_random_name(name)
                tmp_hist = ROOT.TH1F(tmp_name, "", *binning_args)
                tmp_hist.Sumw2()
                _tchain.Project(tmp_name, expression, sel_weight)
                hist.Add(tmp_hist)
            else:
                for values in iter_tree(_tchain, expressions=[expression], defaults=[default],
                        selection=sel_weight, missing_values=missing_values,
                        optimize_branches=False):
                    hist.Fill(values[0])

            # move underflow / overflow bin contents
            if show_underflow:
                r.show_hist_underflow(hist, clear=True)
            if show_overflow:
                r.show_hist_overflow(hist, clear=True)

        # save the original yield and the error
        yield_error = ROOT.Double()
        hist.hmc_yield = hist.IntegralAndError(0, hist.GetNbinsX() + 1, yield_error)
        hist.hmc_yield_error = float(yield_error)

        # register the scale value
        hist.hmc_scale = 1.

        # compute the feature selection efficiency
        if stack and name in signal_names and normalize_signals_before_feature_selection:
            hist.hmc_fs_eff = (n_fs / n_nofs) if n_nofs > 0 else 1.

        # style and store
        if name in signal_names:
            setup_signal_hist(hist, color)
            signal_hists.append(hist)
            all_hists.append(hist)
            n_s += hist.hmc_yield

        elif name in data_names:
            setup_data_hist(hist, color)
            data_hists.append(hist)
            n_d += hist.hmc_yield

        else:
            setup_background_hist(hist, color)
            background_names.append(name)
            background_hists.append(hist)
            all_hists.append(hist)
            n_b += hist.hmc_yield

    # infer qcd
    # note: to propagate uncertainties correctly, the qcd shape must actually be saved as a
    # TGraphErrors object, but as we do not need uncertainties right now, just use a histogram
    if qcd_shape_files and not data_hists:
        raise Exception("cannot infer QCD shape, no data histogram found")
    if qcd_shape_files and len(data_hists) == 1:
        # open all qcd shape files
        files = {region: ROOT.TFile(path, "READ") for region, path in qcd_shape_files.items()}

        # helper to extract the qcd shape in a region
        def get_qcd(region):
            d_hist = files[region].Get("histograms/" + data_names[0])
            if not d_hist:
                raise Exception("data histogram '{}' not found for region '{}' in tfile {}".format(
                    data_names[0], region, files[region]))

            b_hists = []
            for b_name in background_names:
                b_hist = files[region].Get("histograms/" + b_name)
                if not b_hist:
                    raise Exception("background histogram '{}' not found in region '{}'".format(
                        b_name, region))
                b_hists.append(b_hist)

            qcd_hist = d_hist.Clone(create_random_name("qcd_" + region))
            for hist in b_hists:
                qcd_hist.Add(hist, -1.)

            return qcd_hist

        # do the qcd extrapolation
        qcd_hist = get_qcd("os_inviso").Clone(create_random_name("qcd"))
        n_ss_iso = get_qcd("ss_iso").Integral()
        n_ss_inviso = get_qcd("ss_inviso").Integral()
        if not n_ss_iso or not n_ss_inviso:
            print("qcd normalization failed, integrals are {} (ss_iso) and {} (ss_inviso)".format(
                n_ss_iso, n_ss_inviso))
            scaling = 0.
        else:
            scaling = n_ss_iso / n_ss_inviso
        qcd_hist.Scale(scaling)

        # kill negative numbers
        for i in range(1, qcd_hist.GetNbinsX() + 1):
            if qcd_hist.GetBinContent(i) < 0:
                qcd_hist.SetBinContent(i, 0.)

        # store and style
        qcd_hist.hmc_process_name = "qcd"
        yield_error = ROOT.Double()
        qcd_hist.hmc_yield = qcd_hist.IntegralAndError(0, qcd_hist.GetNbinsX() + 1, yield_error)
        qcd_hist.hmc_yield_error = float(yield_error)
        qcd_hist.hmc_scale = 1.
        qcd_hist.SetTitle(qcd_title)
        setup_background_hist(qcd_hist, qcd_color)
        background_hists.append(qcd_hist)
        background_names.append("qcd")
        all_hists.append(qcd_hist)
        n_b += qcd_hist.hmc_yield

    print("n_s: {:.3f}, n_b: {:.3f}, n_d: {:.3f}".format(n_s, n_b, n_d))

    # add data hists to all hists after qcd was potentially added
    if not hide_data:
        all_hists.extend(data_hists)

    # normalize
    if stack:
        # normalize each signal to sum of backgrounds
        for hist in signal_hists:
            itg = hist.hmc_yield
            scale = 1.
            if itg:
                scale = (n_b / itg) if n_b else 1.
                if normalize_signals_before_feature_selection:
                    scale *= hist.hmc_fs_eff
                    print("{} signal eff: {:.4f}".format(hist.hmc_process_name, hist.hmc_fs_eff))
                hist.Scale(scale)
            hist.hmc_scale = scale
    else:
        # basic normalization
        for hist in all_hists:
            scale = 1. / (hist.hmc_yield or 1.)
            hist.Scale(scale)
            hist.hmc_scale = scale

    # replace data hists with graphs
    data_graphs = []
    def update_data_point(i, x, y):
        # apply data range, remove zeros
        if x < data_range[0] or x > data_range[1] or y == 0:
            return x, EMPTY
        else:
            return x, y
    for hist in data_hists:
        graph = hist_to_graph(hist, remove_zeros=False, errors=True, asymm=True, overflow=False,
            underflow=False, attrs=["hmc_process_name", "hmc_hist_type", "hmc_legend_style"])
        update_graph_values(graph, update_data_point)
        r.setup_graph(graph)
        graph.SetTitle(hist.GetTitle())
        data_graphs.append(graph)
        all_hists = [(graph if h == hist else h) for h in all_hists]

    # create the background stack and define the histogram objects to draw
    if stack:
        background_stack = ROOT.THStack(create_random_name("stack"), "stack")
        for hist in background_hists[::-1]:
            background_stack.Add(hist)
        background_stack.hmc_hist_type = "background"
        draw_hists = [background_stack] + signal_hists[::-1]
        if not hide_data:
            draw_hists.extend(data_graphs[::-1])
    else:
        draw_hists = all_hists[::-1]

    # create the legend
    entries = [(hist, hist.GetTitle(), hist.hmc_legend_style) for hist in all_hists]
    if show_ratio:
        # add a dummy entry for mc stats
        mc_unc_dummy = ROOT.TH1F(create_random_name("mc_unc"), "MC stat.", *binning_args)
        r.setup_hist(mc_unc_dummy, color=16, color_flags="f", props={"LineWidth": 0})
        entries.append((mc_unc_dummy, mc_unc_dummy.GetTitle(), "f"))
    n_entries = len(entries)
    if n_entries <= 4:
        n_cols = 1
    elif n_entries <= 8:
        n_cols = 2
    else:
        n_cols = 3
    n_rows = int(math.ceil(float(n_entries) / n_cols))
    col_width = 0.125 if stack and signal_names else 0.125  # use 0.25 when signal scales are shown
    legend_x2 = 0.96
    legend_x1 = legend_x2 - n_cols * col_width
    legend = ROOT.TLegend(*r.calculate_legend_coords(n_rows, dy=0.0425, x1=legend_x1, x2=legend_x2))
    r.setup_legend(legend, {"TextSize": 20, "NColumns": n_cols})
    r.fill_legend(legend, entries)

    # set limits
    if y_min is None:
        y_min = 1.e-3 if y_log else 0.
    if y_max is None:
        f = 1.35 if n_rows <= 3 else 1.45
        y_max_central = max(
            (get_graph_maximum(obj, True) if isinstance(obj, ROOT.TGraph) else obj.GetMaximum())
            for obj in draw_hists
        )
        y_max = (y_max_central**f * y_min**(1 - f)) if y_log else ((y_max_central - y_min) * f)
    dummy_hist.SetMinimum(y_min)
    dummy_hist.SetMaximum(y_max)

    # cms labels
    draw_labels.extend(r.routines.create_cms_labels(postfix="preliminary"))

    # category text, amended by the signal scales when stacking
    if stack and show_signal_scale and signal_hists:
        scale_text = []
        for hist in signal_hists:
            scale = hist.hmc_scale
            if scale < 100:
                scale = "{:.1f}".format(scale)
            elif scale < 10000:
                scale = "{}".format(int(round(scale)))
            else:
                scale = "{:.2e}".format(scale).replace("+0", "").replace("+", "")
            scale_text.append("{} x{}".format(hist.GetTitle(), scale))
        category_text.append("#scale[0.75]{{{}}}".format(",  ".join(scale_text)))
    if category_text:
        for i, cat_text in enumerate(category_text):
            draw_labels.append(r.routines.create_top_left_label(cat_text, h_offset=0.03,
                v_offset=0.07 + i * r.styles.legend_dy, props={"TextSize": 20}))

    # custom top right label
    if top_right_text:
        draw_labels.append(r.routines.create_top_right_label(top_right_text,
            props={"TextSize": 20}))

    # draw everything in pad1
    dummy_hist.Draw("HIST")
    for hist in draw_hists:
        option = "HIST,SAME" if hist.hmc_hist_type != "data" else "PEZ,SAME"
        hist.Draw(option)
    legend.Draw()
    for label in draw_labels:
        label.Draw()

    r.styles.pop()

    # draw in pad2
    if show_ratio:
        pad2_style = r.styles.copy(r.styles.current_style_name, "pad2_style")
        pad2_style.pad.TopMargin = 1. - pad1_style.pad.BottomMargin
        pad2_style.y_axis.LabelSize = 19
        pad2_style.y_axis.CenterTitle = True

        r.styles.push("pad2_style")
        r.setup_style()
        r.setup_pad(pad2)
        pad2.cd()

        # create a dummy hist
        hist_title2 = ";{};{}".format(x_title, "Data / MC")
        dummy_hist2 = ROOT.TH1F(create_random_name("dummy"), hist_title2, *binning_args)
        r.setup_hist(dummy_hist2, pad=pad2, props={"Minimum": 0.25, "Maximum": 1.75})
        r.setup_y_axis(dummy_hist2.GetYaxis(), pad2, props={"Ndivisions": 3})

        # combine background bin contents with corrent MC error propagation
        b_nums = []
        for h in background_hists:
            x, y, e = hist_to_array(h, errors=True, asymm=False, overflow=False, underflow=False)
            b_nums.append(Number(y, {h.hmc_process_name: e}))
        b_num = sum(b_nums)
        b_vals = b_num()
        b_errs = b_num("up", diff=True, factor=True)

        # data / mc graph and mc uncertainty graph
        data_graph = data_graphs[0]
        ratio_graph = ROOT.TGraphAsymmErrors(n_bins)
        mc_unc_graph = ROOT.TGraphErrors(n_bins)
        r.setup_graph(ratio_graph)
        r.setup_graph(mc_unc_graph, props={"FillStyle": 1001, "LineColor": 0, "MarkerColor": 0,
            "MarkerSize": 0., "FillColor": 16})
        for i in range(n_bins):
            # get the data point
            x, d = ROOT.Double(), ROOT.Double()
            data_graph.GetPoint(i, x, d)
            # get the background contribution
            b = b_vals[i]
            # set the ratio
            if d == EMPTY or b == 0:
                ratio_graph.SetPoint(i, x, EMPTY)
            else:
                ratio_graph.SetPoint(i, x, d / b)
                ratio_graph.SetPointEYhigh(i, data_graph.GetErrorYhigh(i) / b)
                ratio_graph.SetPointEYlow(i, data_graph.GetErrorYlow(i) / b)
            # set the mc uncertainty
            if b == 0:
                mc_unc_graph.SetPoint(i, x, EMPTY)
            else:
                mc_unc_graph.SetPoint(i, x, 1.)
                mc_unc_graph.SetPointError(i, dummy_hist2.GetBinWidth(i + 1) / 2., b_errs[i])

        # grid lines
        lines = []
        for y in [0.5, 1.0, 1.5]:
            l = ROOT.TLine(x_min, y, x_max, y)
            r.setup_line(l, props={"NDC": False, "LineStyle": 3, "LineWidth": 1, "LineColor": 1})
            lines.append(l)

        # draw
        dummy_hist2.Draw("HIST")
        mc_unc_graph.Draw("2,SAME")
        for line in lines:
            line.Draw()
        if not hide_data:
            ratio_graph.Draw("PEZ,SAME")

        r.styles.pop()

    # save the canvas
    r.update_canvas(canvas)
    if pdf_output_path:
        canvas.SaveAs(create_file_dir(pdf_output_path))

    # once the canvas is updated, revert all histogram scales before writing root file and yields
    for hist in signal_hists + background_hists + data_hists:
        scale = hist.hmc_scale or 1.
        hist.Scale(1. / scale)

    # save histograms in root file
    if root_output_path:
        f = ROOT.TFile(create_file_dir(root_output_path), "RECREATE")
        f.cd()

        # canvas
        canvas.Write("canvas")

        # histograms
        hist_dir = f.mkdir("histograms")
        hist_dir.cd()
        for hist in signal_hists + background_hists + data_hists:
            hist.Write(hist.hmc_process_name)

        f.Close()

    # save yields, including underflow and overflow bins
    if yields_output_path:
        yields = OrderedDict()
        for hist in signal_hists + background_hists + data_hists:
            yields[hist.hmc_process_name] = {
                "value": hist.hmc_yield,
                "error": hist.hmc_yield_error,
                "entries": getattr(hist, "hmc_entries", hist.GetEntries()),
            }
        with open(create_file_dir(yields_output_path), "w") as f:
            json.dump(yields, f, indent=4)


@with_hmc_style
def multi_roc_plot(output_path, fprs, tprs, class_names, x_title=None, y_title=None, inverted=False,
        y_max=None, legend_titles=None, colors=None, line_styles=None, aucs=None,
        category_text=None, top_right_text=None):
    ROOT = import_root()
    import plotlib.root as r
    import numpy as np

    # some input checks
    n = len(class_names)
    assert(len(fprs) == n)
    assert(len(tprs) == n)
    assert(all(fpr.ndim == 1 for fpr in fprs))
    assert(all(tpr.ndim == 1 for tpr in tprs))
    assert(all(fpr.shape == tpr.shape for fpr, tpr in zip(fprs, tprs)))
    assert(legend_titles is None or len(legend_titles) == n)
    assert(colors is None or len(colors) == n)
    assert(aucs is None or len(aucs) == n)
    assert(line_styles is None or len(line_styles) == n)

    # setup the canvas and pad
    r.setup_style()
    canvas, (pad,) = r.routines.create_canvas(pad_props={"Logy": inverted})
    pad.cd()

    # axis titles
    if x_title is None:
        x_title = "False positive rate" if not inverted else "True positive rate"
    if y_title is None:
        y_title = "True positive rate" if not inverted else "1 / False positive rate"

    # prepare legend titles
    if not legend_titles:
        legend_titles = list(class_names)

    # prepare colors
    if colors is None:
        colors = list(range(2, 2 + n))

    # prepare aucs
    if aucs is None:
        import sklearn.metrics
        aucs = [sklearn.metrics.auc(fpr, tpr) for fpr, tpr in zip(fprs, tprs)]

    # prepare line styles
    if line_styles is None:
        line_styles = n * [1]

    # create a dummy histogram for axes
    hist = ROOT.TH1F("dummy", ";{};{}".format(x_title, y_title), 5, 0., 1.)

    draw_labels = []

    # setup hist and axes
    r.setup_hist(hist, pad=pad)

    graphs = []
    _y_max = 1.0
    for fpr, tpr, class_namec, color, lstyle in zip(fprs, tprs, class_names, colors, line_styles):
        if inverted:
            x, y = tpr, 1. / np.clip(fpr, 1e-7, 1.)
        else:
            x, y = fpr, tpr
        _y_max = max(_y_max, y[0])
        graph = ROOT.TGraph(len(fpr), x, y)
        r.setup_graph(graph, {"MarkerSize": 0, "LineStyle": lstyle}, color=color)
        graphs.append(graph)

    # set y range
    yrange = (0., 1.) if not inverted else (1., y_max or _y_max)
    hist.GetYaxis().SetRangeUser(*yrange)

    # create the legend
    legend_y2 = None if inverted else 0.8
    legend = ROOT.TLegend(*r.calculate_legend_coords(n, x1=0.62, y2=legend_y2))
    r.setup_legend(legend)
    for graph, legend_title, auc in zip(graphs, legend_titles, aucs):
        legend.AddEntry(graph, "{} #scale[0.85]{{(AUC {:5.3f})}}".format(legend_title, auc))

    # cms labels
    draw_labels.extend(r.routines.create_cms_labels(postfix="preliminary"))

    # custom top right label
    if top_right_text:
        draw_labels.append(r.routines.create_top_right_label(top_right_text))

    # category text
    if category_text:
        if not isinstance(category_text, (list, tuple)):
            category_text = [category_text]
        h_offset = 0.08 if inverted else 0.5
        v_offset = 0.075 if inverted else 0.22 + n * r.styles.legend_dy
        for i, cat_text in enumerate(category_text):
            draw_labels.append(r.routines.create_top_left_label(cat_text, h_offset=h_offset,
                v_offset=v_offset + i * r.styles.legend_dy))

    # draw everything
    hist.Draw()
    for graph in graphs[::-1]:
        graph.Draw("SAME")
    legend.Draw()
    for label in draw_labels:
        label.Draw()

    r.update_canvas(canvas)
    canvas.SaveAs(create_file_dir(output_path))


@with_hmc_style
def confusion_plot(output_path, data, process_labels, class_labels, subtract_data=None,
        skip_uncertainties=False, digits=3, auto_color=True, z_title="Accuracy (row-normalized)",
        top_right_text=None):
    """
    Creates a confusion plot given a numpy array *counts* and the corresponding *process_labels* and
    *class_labels*. When *data* is a numpy array, it is interpreted as the counts per true class in
    each row, meaning that value ``data[i, j]`` contains the number of events of the true class
    ``i`` that were classified as class ``j``. When *data* is a tuple of two numpy arrays with
    identical shape, the arrays are interpreted as counts (or weighted yields) and their errors.
    """
    ROOT = import_root()
    import plotlib.root as r
    import numpy as np
    from scinum import Number, UP

    # interpret data
    if isinstance(data, (list, tuple)) and len(data) == 2:
        counts, errors = data
    else:
        counts = data
        errors = counts.astype(np.float32)**0.5

    # interpret subtract_data
    if subtract_data is not None:
        if isinstance(subtract_data, (list, tuple)) and len(subtract_data) == 2:
            subtract_counts, subtract_errors = subtract_data
        else:
            subtract_counts = subtract_data
            subtract_errors = subtract_counts.astype(np.float32)**0.5

    # some input checks
    assert(counts.ndim == 2)
    n_processes = counts.shape[0]
    n_classes = counts.shape[1]
    assert(errors is None or counts.shape == errors.shape)
    assert(n_processes == len(process_labels))
    assert(n_classes == len(class_labels))

    # define errors when not defined
    if errors is None:
        errors = counts.astype(np.float32)**0.5

    # prepare fractions with uncertainties
    values = []
    uncs = []
    for i in range(n_processes):
        # assume plain poisson error
        numbers = [
            Number(float(counts[i, j]), {"row{}_col{}".format(i, j): float(errors[i, j])})
            for j in range(n_classes)
        ]
        # subtract counts (e.g. for qcd estimation)
        if subtract_data is not None:
            diffs = []
            for j in range(n_classes):
                sub_num = Number(float(subtract_counts[i, j]),
                    {"row{}_col{}_sub".format(i, j): float(subtract_errors[i, j])})
                diff = numbers[j] - sub_num
                if diff < 0:
                    diff.nominal = 1.e-5
                diffs.append(diff)
            numbers = diffs
        n_total = sum(numbers)
        fractions = [(((num + 0.00001) / n_total) if n_total else (num * 0.)) for num in numbers]
        values.append([f() for f in fractions])
        uncs.append([f(UP, diff=True) for f in fractions])

    # helper to create a value label text
    def value_text(i, j):
        def fmt(v):
            s = "{{:.{}f}}".format(digits).format(v)
            return s if re.sub(r"(0|\.)", "", s) else ("<" + s[:-1] + "1")
        if skip_uncertainties:
            return fmt(values[i][j])
        else:
            return "#splitline{{ {}}}{{#scale[0.85]{{#pm {}}}}}".format(
                fmt(values[i][j]), fmt(uncs[i][j]))

    # calculate canvas dimensions and margins to achieve a certain box size per bin
    # and create a new plot style
    custom_style = r.styles.copy(r.styles.current_style_name, "custom_style")
    box_px = 80.
    rm_px = r.styles.canvas_width * 0.16
    lm_px = r.styles.canvas_width * float(r.styles.pad.LeftMargin)
    tm_px = r.styles.canvas_height * float(r.styles.pad.TopMargin)
    bm_px = r.styles.canvas_height * float(r.styles.pad.BottomMargin)
    custom_style.canvas_width = int(lm_px + n_classes * box_px + rm_px)
    custom_style.canvas_height = int(bm_px + n_processes * box_px + tm_px)
    custom_style.pad.RightMargin = rm_px / custom_style.canvas_width
    custom_style.pad.LeftMargin = lm_px / custom_style.canvas_width
    custom_style.pad.TopMargin = tm_px / custom_style.canvas_height
    custom_style.pad.BottomMargin = bm_px / custom_style.canvas_height
    r.styles.push("custom_style")

    # setup canvas and pad
    r.setup_style()
    canvas, (pad,) = r.routines.create_canvas()
    pad.cd()

    # create and fill the histogram
    binning = (n_classes, -0.5, n_classes - 0.5, n_processes, -0.5, n_processes - 0.5)
    hist = ROOT.TH2F("confusion", ";Predicted process;True process;" + z_title, *binning)
    draw_labels = []

    # loop through rows and columns
    for i, row in enumerate(values):
        for j, value in enumerate(row):
            # translate from indices to bin centers
            x = j
            y = n_processes - i - 1

            # set values
            hist.Fill(x, y, value)

            # create value labels
            value_label = ROOT.TLatex(x, y, value_text(i, j))
            color = ROOT.kBlack
            if auto_color:
                if callable(auto_color):
                    color = auto_color(value)
                else:
                    color = ROOT.kWhite if value < 0.05 else ROOT.kBlack
            r.tools.setup_latex(value_label, {"NDC": False, "TextAlign": 22, "TextSize": 18,
                "TextColor": color})
            draw_labels.append(value_label)

            # set x axis labels
            if i == 0:
                hist.GetXaxis().SetBinLabel(j + 1, class_labels[j])

        # set y axis labels
        hist.GetYaxis().SetBinLabel(n_processes - i, process_labels[i])

    # setup hist and axes
    r.setup_hist(hist)
    r.setup_x_axis(hist.GetXaxis(), pad, {"TitleOffset": r.get_stable_distance("v", 1.2),
        "Ndivisions": n_classes})
    r.setup_y_axis(hist.GetYaxis(), pad, {"TitleOffset": r.get_stable_distance("h", 1.4),
        "Ndivisions": n_processes, "LabelOffset": 0.001})
    r.setup_z_axis(hist.GetZaxis(), pad, {"TitleOffset": r.get_stable_distance("h", 1.1),
        "Ndivisions": 5 if n_processes < 4 else 10, "RangeUser": (0., 1.)})

    # cms labels
    draw_labels.extend(r.routines.create_cms_labels(postfix="preliminary"))

    # custom top right label
    if top_right_text:
        draw_labels.append(r.routines.create_top_right_label(top_right_text))

    # draw everything
    hist.Draw("COLZ")
    for label in draw_labels:
        label.Draw()

    r.update_canvas(canvas)
    canvas.SaveAs(create_file_dir(output_path))


@with_hmc_style
def correlation_plot(output_path, tchain, expression, x_binning, y_binning, tree_name=None,
        selection=None, default=None, missing_values=None, draw_style="COLZ", draw_1d=True,
        normalize=False, x_title="FEATURE", y_title="DNN FEATURE", z_title=None, x_labels=None,
        y_labels=None, category_text=None, top_right_text=None):
    ROOT = import_root()
    import plotlib.root as r
    from plotlib.util import create_random_name

    # some input checks
    assert(x_labels is None or len(x_labels) == x_binning[0])
    assert(y_labels is None or len(y_labels) == y_binning[0])

    # some of the styles depend on the draw style
    draw_z = draw_style.lower() == "colz"

    # setup the canvas and pad
    r.setup_style()
    pad_right_margin = 0.16 if draw_z else r.styles.pad.RightMargin
    canvas, (pad,) = r.routines.create_canvas(pad_props={"RightMargin": pad_right_margin})
    pad.cd()

    # default z title
    if z_title is None:
        if normalize == "x":
            z_title = "Column-normalized events"
        elif normalize == "y":
            z_title = "Row-normalized events"
        elif normalize == "x_max":
            z_title = "Column-scaled events"
        elif normalize == "y_max":
            z_title = "Row-scaled events"
        elif normalize:
            z_title = "Normalized events"
        else:
            z_title = "Events"

    # create the histogram
    hist2d_title = ";{};{};{}".format(x_title, y_title, z_title)
    hist2d_name = create_random_name("2d_correlation")
    hist2d = ROOT.TH2F(hist2d_name, hist2d_title, *(x_binning + y_binning))
    r.setup_hist(hist2d, pad=pad)

    # also create a superimposed histogram for the x axis expression
    if draw_1d:
        hist1d_name = create_random_name("1d_stats")
        hist1d = ROOT.TH1F(hist1d_name, "", *x_binning)
        r.setup_hist(hist1d, pad=pad)

    # fill via projection
    tchain.Project(hist2d_name, expression, selection or "")
    if draw_1d:
        tchain.Project(hist1d_name, expression.split(":")[-1], selection or "")

    # determine the correlation
    rho = hist2d.GetCorrelationFactor()

    # normalize the correlation hist
    if normalize in ["x", "x_max"]:
        for bx in range(1, hist2d.GetNbinsX() + 1):
            fn = max if normalize == "x_max" else sum
            norm = float(fn(hist2d.GetBinContent(bx, by) for by in range(1, hist2d.GetNbinsY() + 1)))
            if not norm:
                continue
            for by in range(1, hist2d.GetNbinsY() + 1):
                hist2d.SetBinContent(bx, by, hist2d.GetBinContent(bx, by) / norm)
    elif normalize in ["y", "y_max"]:
        for by in range(1, hist2d.GetNbinsY() + 1):
            fn = max if normalize == "y_max" else sum
            norm = float(fn(hist2d.GetBinContent(bx, by) for bx in range(1, hist2d.GetNbinsX() + 1)))
            if not norm:
                continue
            for bx in range(1, hist2d.GetNbinsX() + 1):
                hist2d.SetBinContent(bx, by, hist2d.GetBinContent(bx, by) / norm)
    elif normalize:
        hist2d.Scale(1. / (hist2d.Integral() or 1))

    # rescale the 1d histogram so that its y range is compatible with the actual y axis
    if draw_1d:
        max1d = hist1d.GetMaximum()
        hist1d.Scale(0.9 / max1d)

    # custom bin labels
    if x_labels:
        for i, x_label in enumerate(x_labels):
            hist2d.GetXaxis().SetBinLabel(i + 1, x_label)
    if y_labels:
        for i, y_label in enumerate(y_labels):
            hist2d.GetYaxis().SetBinLabel(i + 1, y_label)

    # cms labels
    draw_labels = []
    draw_labels.extend(r.routines.create_cms_labels(postfix="preliminary"))

    # correlation label
    h_offset = 0.15 if draw_z else 0.015
    rho_label = r.routines.create_top_right_label("#rho = {:.3f}".format(rho), h_offset=h_offset,
        v_offset=0.065)
    draw_labels.append(rho_label)

    # category text
    if category_text:
        if not isinstance(category_text, (list, tuple)):
            category_text = [category_text]
        for i, cat_text in enumerate(category_text):
            draw_labels.append(r.routines.create_top_left_label(cat_text, h_offset=0.03,
                v_offset=0.075 + i * r.styles.legend_dy))

    # custom top right label
    if top_right_text:
        draw_labels.append(r.routines.create_top_right_label(top_right_text, h_offset=h_offset))

    # draw everything
    hist2d.Draw(draw_style)
    if draw_1d:
        hist1d.Draw("SAME,HIST")
    for label in draw_labels:
        label.Draw()

    r.update_canvas(canvas)
    canvas.SaveAs(create_file_dir(output_path))


@with_hmc_style
def bin_opt_plot(output_path, feature_name, s_vals, b_vals, full_edges, opt_edges,
        x_title="FEATURE", y_title="Events", category_text=None, top_right_text=None):
    ROOT = import_root()
    import plotlib.root as r
    from plotlib.util import create_random_name

    # prepare some values
    n_mini_bins = len(full_edges) - 1
    n_opt_bins = len(opt_edges) - 1

    # some input checks
    assert(s_vals.shape[0] == n_mini_bins)
    assert(b_vals.shape[0] == n_mini_bins)

    # prepare category text
    if category_text:
        if not isinstance(category_text, (list, tuple)):
            category_text = [category_text]
    else:
        category_text = []

    # setup the canvas and pad
    r.setup_style()
    canvas, (pad,) = r.routines.create_canvas()
    pad.cd()

    # create a dummy hist that is used to setup axes, etc
    hist_title = ";{};{}".format(x_title, y_title)
    binning_args = n_opt_bins, array.array("f", opt_edges)
    dummy_hist = ROOT.TH1F(create_random_name("dummy"), hist_title, *binning_args)
    r.setup_hist(dummy_hist, pad=pad)

    # create signal and background hists
    s_hist = ROOT.TH1F(create_random_name("s"), "Signal", *binning_args)
    b_hist = ROOT.TH1F(create_random_name("b"), "Background", *binning_args)
    r.setup_hist(s_hist, color="blue", color_flags="lm")
    r.setup_hist(b_hist, color="red", color_flags="lfm")

    # fill values
    for hist, vals in [(s_hist, s_vals), (b_hist, b_vals)]:
        for i, v in enumerate(vals):
            x = full_edges[i] + 0.5 * (full_edges[i + 1] - full_edges[i])
            hist.Fill(x, v)

    # move underlfow and overflow
    r.show_hist_underflow(hist, clear=True)
    r.show_hist_overflow(hist, clear=True)

    # scale s to b
    s_scale = (b_hist.Integral() / s_hist.Integral()) if s_hist.Integral() else 1.
    s_hist.Scale(s_scale)

    # y range
    y_max = max(s_hist.GetMaximum(), b_hist.GetMaximum())
    dummy_hist.SetMinimum(0.)
    dummy_hist.SetMaximum(y_max * 1.2)

    # create the legend
    legend = ROOT.TLegend(*r.calculate_legend_coords(2, dy=0.0425))
    r.setup_legend(legend)
    r.fill_legend(legend, [(s_hist, "Signal #scale[0.85]{{x{:.1f}}}".format(s_scale)), b_hist])

    # cms labels
    draw_labels = []
    draw_labels.extend(r.routines.create_cms_labels(postfix="preliminary"))

    # category text
    if category_text:
        for i, cat_text in enumerate(category_text):
            draw_labels.append(r.routines.create_top_left_label(cat_text, h_offset=0.03,
                v_offset=0.07 + i * 0.0425))

    # custom top right label
    if top_right_text:
        draw_labels.append(r.routines.create_top_right_label(top_right_text))

    # draw everything
    dummy_hist.Draw("HIST")
    b_hist.Draw("HIST,SAME")
    s_hist.Draw("HIST,SAME")
    legend.Draw()
    for label in draw_labels:
        label.Draw()

    # save the canvas
    r.update_canvas(canvas)
    canvas.SaveAs(create_file_dir(output_path))


def plot_roc_curves(fprs, tprs, labels, output_path):
    # output path should be a pattern such as "ROC_{}.pdf"
    import matplotlib
    matplotlib.use("Agg")
    from matplotlib import pyplot as plt

    for i, (fpr, tpr, label) in enumerate(zip(fprs, tprs, labels)):
        plt.figure(i + 1)
        plt.plot([0, 1], [0, 1], "k--")
        plt.plot(fpr, tpr)
        plt.xlabel("False Positive Rate")
        plt.ylabel("True Positive Rate")
        plt.title(label)
        plt.savefig(create_file_dir(output_path).format(label))


def plot_feature_ranking(feature_names, acc_means, save_pattern):
    import matplotlib
    matplotlib.use("Agg")
    from matplotlib import pyplot as plt
    plt.figure(figsize=(15, max([0.3 * len(feature_names), 10])))
    plt.barh(feature_names, acc_means)
    plt.xlabel("Score")
    plt.savefig(create_file_dir(save_pattern))
