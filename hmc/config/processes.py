# coding: utf-8

"""
Definition of physics processes, cross sections, branching ratios and other numeric quantities.
Physics constants. If not stated otherwise, cross sections are given in pb. Values are taken from:
- https://twiki.cern.ch/twiki/bin/view/CMS/StandardModelCrossSectionsat13TeVInclusive?rev=18
- https://twiki.cern.ch/twiki/bin/view/CMS/SummaryTable1G25ns?rev=151
- https://twiki.cern.ch/twiki/bin/view/LHCPhysics/CERNYellowReportPageBR?rev=22
- https://twiki.cern.ch/twiki/bin/view/LHCPhysics/LHCHXSWG?rev=348
"""


__all__ = []

from scinum import Number, REL
import order as od


#
# constants
#

# misc
N_LEPS = Number(3)

# masses
M_Z = Number(91.1876, {"m_z": 0.0021})
M_H = Number(125.35, {"m_h": 0.15})

# branching ratios
BR_W_HAD = Number(0.6741, {"br_w": 0.0027})
BR_W_LEP = 1 - BR_W_HAD
BR_WW_SL = 2 * BR_W_HAD.mul(BR_W_LEP, rho=-1, inplace=False)
BR_WW_DL = BR_W_LEP**2.
BR_WW_FH = BR_W_HAD**2.
BR_Z_CLEP = Number(0.033658, {"br_z_clep": 0.000023}) * N_LEPS
BR_H_BB = Number(0.5760, {
    "br_h_th": (REL, 0.0065),
    "br_h_mq": (REL, 0.0073, 0.0075),
    "br_h_as": (REL, 0.0078, 0.0080),
})
BR_H_TAUTAU = Number(0.0622, {
    "br_h_th": (REL, 0.0117, 0.116),
    "br_h_mq": (REL, 0.0097, 0.0099),
    "br_h_as": (REL, 0.0061),
})
BR_HH_BB_TAUTAU = 2 * BR_H_BB * BR_H_TAUTAU


#
# Data
#

process_data = od.Process(
    "data", 1,
    label="Data",
    color=(0, 0, 0),
    is_data=True,
)

process_data_mutau = process_data.add_process(
    "data_mutau", 11,
    label=r"Data $\mu\tau_{h}$",
    color=(0, 0, 0),
    is_data=True,
)

process_data_etau = process_data.add_process(
    "data_etau", 12,
    label=r"Data $e\tau_{h}$",
    color=(0, 0, 0),
    is_data=True,
)

process_data_tautau = process_data.add_process(
    "data_tautau", 13,
    label=r"Data $\tau_{h}$\tau_{h}$",
    color=(0, 0, 0),
    is_data=True,
)


#
# HH processes
# (decay into bb tautau implied, cross section for m_h = 125.09 GeV)
# https://twiki.cern.ch/twiki/bin/view/LHCPhysics/LHCHXSWGHH?rev=53#Current_recommendations_for_HH_c
#

process_hh = od.Process(
    "hh", 1000,
    label="HH",
    aux={"is_signal": True},
)

process_hh_ggf = process_hh.add_process(
    "hh_ggf", 1100,
    label=r"$HH_{ggF}$",
    color=(12, 63, 148),
    xsecs={
        13: Number(0.03105, {
            "scale": (REL, 0.022, 0.05),
            "pdf_alphas": (REL, 0.03),
            "m_top": (REL, 0.026),
        }) * BR_HH_BB_TAUTAU,
    },
    aux={"is_signal": True},
)

# collection process for all ggF BSM points defined below
process_hh_ggf_bsm = process_hh.add_process(
    "hh_ggf_bsm", 1150,
    label=r"$HH_{ggF}^{BSM}$",
    aux={"is_signal": True, "is_bsm": True},
)

process_hh_ggf_0 = process_hh_ggf_bsm.add_process(
    "hh_ggf_0", 1151,
    label=r"$HH_{VBF}^{0}$",
    color=process_hh_ggf.color,
    xsecs=process_hh_ggf.xsecs,
    aux={"is_signal": True, "is_bsm": True},
)

process_hh_ggf_2p45 = process_hh_ggf_bsm.add_process(
    "hh_ggf_2p45", 1152,
    label=r"$HH_{VBF}^{2.45}$",
    color=process_hh_ggf.color,
    xsecs=process_hh_ggf.xsecs,
    aux={"is_signal": True, "is_bsm": True},
)

process_hh_ggf_5 = process_hh_ggf_bsm.add_process(
    "hh_ggf_5", 1153,
    label=r"$HH_{VBF}^{5}$",
    color=process_hh_ggf.color,
    xsecs=process_hh_ggf.xsecs,
    aux={"is_signal": True, "is_bsm": True},
)

process_hh_vbf = process_hh.add_process(
    "hh_vbf", 1200,
    label=r"$HH_{VBF}$",
    color=(85, 136, 224),
    xsecs={
        13: Number(0.001723, {
            "scale": (REL, 0.0003, 0.0004),
            "pdf_alphas": (REL, 0.021),
        }) * BR_HH_BB_TAUTAU,
    },
    aux={"is_signal": True},
)

# collection process for all VBF BSM points defined below
process_hh_vbf_bsm = process_hh.add_process(
    "hh_vbf_bsm", 1300,
    label=r"$HH_{VBF}^{BSM}$",
    aux={"is_signal": True, "is_bsm": True},
)

process_hh_vbf_0p5_1_1 = process_hh_vbf_bsm.add_process(
    "hh_vbf_0p5_1_1", 1310,
    label=r"$HH_{VBF}^{(0.5,1,1)}$",
    color=process_hh_vbf.color,
    xsecs=process_hh_vbf.xsecs,
    aux={"is_signal": True, "is_bsm": True},
)

process_hh_vbf_1p5_1_1 = process_hh_vbf_bsm.add_process(
    "hh_vbf_1p5_1_1", 1320,
    label=r"$HH_{VBF}^{(1.5,1,1)}$",
    color=process_hh_vbf.color,
    xsecs=process_hh_vbf.xsecs,
    aux={"is_signal": True, "is_bsm": True},
)

process_hh_vbf_1_0_1 = process_hh_vbf_bsm.add_process(
    "hh_vbf_1_0_1", 1330,
    label=r"$HH_{VBF}^{(1,0,1)}$",
    color=process_hh_vbf.color,
    xsecs=process_hh_vbf.xsecs,
    aux={"is_signal": True, "is_bsm": True},
)

process_hh_vbf_1_2_1 = process_hh_vbf_bsm.add_process(
    "hh_vbf_1_2_1", 1340,
    label=r"$HH_{VBF}^{(1,2,1)}$",
    color=process_hh_vbf.color,
    xsecs=process_hh_vbf.xsecs,
    aux={"is_signal": True, "is_bsm": True},
)

process_hh_vbf_1_1_0 = process_hh_vbf_bsm.add_process(
    "hh_vbf_1_1_0", 1350,
    label=r"$HH_{VBF}^{(1,1,0)}$",
    color=process_hh_vbf.color,
    xsecs=process_hh_vbf.xsecs,
    aux={"is_signal": True, "is_bsm": True},
)

process_hh_vbf_1_1_2 = process_hh_vbf_bsm.add_process(
    "hh_vbf_1_1_2", 1360,
    label=r"$HH_{VBF}^{(1,1,2)}$",
    color=process_hh_vbf.color,
    xsecs=process_hh_vbf.xsecs,
    aux={"is_signal": True, "is_bsm": True},
)

process_hh_vbf_c2v = process_hh.add_process(
    "hh_vbf_c2v", 1370,
    label=r"$HH_{VBF}^{C2V}$",
    color=process_hh_vbf.color,
    xsecs=process_hh_vbf.xsecs,
    aux={"is_signal": True, "is_bsm": True},
)

process_hh_vbf_sm_c2v = process_hh.add_process(
    "hh_vbf_sm_c2v", 1380,
    label=r"$HH_{VBF}^{SM,C2V}$",
    color=process_hh_vbf.color,
    xsecs=process_hh_vbf.xsecs,
    processes=[process_hh_vbf, process_hh_vbf_c2v],
    aux={"is_signal": True},
)

process_hh_all = process_hh.add_process(
    "hh_all", 1390,
    label=r"$HH$",
    color=process_hh_vbf.color,
    xsecs=process_hh_vbf.xsecs,
    processes=[process_hh_ggf, process_hh_vbf, process_hh_vbf_c2v],
    aux={"is_signal": True},
)


#
# ttbar processes
# https://twiki.cern.ch/twiki/bin/view/LHCPhysics/TtbarNNLO?rev=16#Top_quark_pair_cross_sections_at
# use mtop = 172.5 GeV, see
# https://twiki.cern.ch/twiki/bin/view/CMS/TopMonteCarloSystematics?rev=7#mtop
#

process_tt = od.Process(
    "tt", 2000,
    label=r"$t\bar{t}$",
    xsecs={
        13: Number(831.76, {
            "scale": (19.77, 29.20),
            "pdf": 35.06,
            "mtop": (23.18, 22.45),
        }),
    },
)

process_tt_sl = process_tt.add_process(
    "tt_sl", 2100,
    label=process_tt.label + " (sl)",
    color=(255, 153, 0),
    xsecs={
        13: process_tt.get_xsec(13) * BR_WW_SL,
    },
)

process_tt_dl = process_tt.add_process(
    "tt_dl", 2200,
    label=process_tt.label + " (dl)",
    color=(205, 0, 9),
    xsecs={
        13: process_tt.get_xsec(13) * BR_WW_DL,
    },
)

process_tt_fh = process_tt.add_process(
    "tt_fh", 2300,
    label=process_tt.label + " (fh)",
    color=(131, 38, 10),
    xsecs={
        13: process_tt.get_xsec(13) * BR_WW_FH,
    },
)

# additional parent process for tt_sl and tt_dl
process_tt_lep = process_tt.add_process(
    "tt_lep", 2050,
    label=process_tt.label + " (lep)",
    color=(255, 153, 0),
    processes=[process_tt_sl, process_tt_dl],
)


#
# single-top processes
# https://twiki.cern.ch/twiki/bin/viewauth/CMS/SingleTopSigma?rev=12#Single_Top_Cross_sections_at_13
#

process_st = od.Process(
    "st", 3000,
    label=r"Single $t$/$\bar{t}$",
    color=(2, 210, 209),
)

process_st_tchannel = process_st.add_process(
    "st_tchannel", 3100,
    label=process_st.label + ", t-channel",
    xsecs={
        13: Number(216.99, {
            "scale": (6.62, 4.64),
            "pdf_alphas": 6.16,
            "m_top": 1.81,
        }),
    },
)

process_st_twchannel = process_st.add_process(
    "st_twchannel", 3200,
    label=process_st.label + ", tW-channel",
    xsecs={
        13: Number(71.7, {
            "scale": 1.8,
            "pdf_alphas": 3.4,
        }),
    },
)

process_st_schannel = process_st.add_process(
    "st_schannel", 3300,
    label=process_st.label + ", s-channel",
    xsecs={
        13: Number(11.36, {
            "scale": 0.18,
            "pdf_alphas": (0.40, 0.45),
        }),
    },
)

process_st.set_xsec(13, process_st_tchannel.get_xsec(13) + process_st_twchannel.get_xsec(13)
    + process_st_schannel.get_xsec(13))


#
# drell-yan processes
# https://twiki.cern.ch/twiki/bin/viewauth/CMS/SummaryTable1G25ns?rev=153#DY_Z
#

process_dy = od.Process(
    "dy", 4000,
    label="DY",
    color=(255, 102, 102),
)

process_dy_ll = process_dy.add_process(
    "dy_ll", 4100,
    label="DY(ll)",
)

process_dy_ll_m5to50 = process_dy_ll.add_process(
    "dy_ll_m5to50", 4110,
    label=process_dy_ll.label + r", $5 \lt m_{ll} \leq 50$",
    xsecs={
        13: Number(71310, {
            "scale": 70,
        }),
    },
)

process_dy_ll_m50 = process_dy_ll.add_process(
    "dy_ll_m50", 4170,
    label=process_dy_ll.label + r", $m_{ll} \lt 50$",
    xsecs={
        13: Number(6077.22, {
            "scale": (REL, 0.02),
            "pdf_alphas": 14.78,
            "integration": 1.49,
        }),
    },
)

process_dy_ll.set_xsec(13, process_dy_ll_m5to50.get_xsec(13) + process_dy_ll_m50.get_xsec(13))
process_dy.set_xsec(13, process_dy_ll.get_xsec(13) / BR_Z_CLEP)


#
# ttH processes
#

process_tth = od.Process(
    "tth", 5000,
    label=r"$t\bar{t}H$",
    color=(81, 142, 25),
    xsecs={
        13: Number(0.5065, {
            "scale": (REL, 0.058, 0.092),
            "pdf_alphas": (REL, 0.036),
        }),
    },
)

process_tth_bb = process_tth.add_process(
    "tth_bb", 5100,
    label=process_tth.label + r"($b\bar{b}$)",
    color=(81, 142, 25),
    xsecs={
        13: process_tth.get_xsec(13) * BR_H_BB,
    },
)

process_tth_tautau = process_tth.add_process(
    "tth_tautau", 5200,
    label=process_tth.label + r"($\tau^{+}\tau^{-}$)",
    color=(133, 230, 40),
    xsecs={
        13: process_tth.get_xsec(13) * BR_H_TAUTAU,
    },
)

process_tth_nonbb = process_tth.add_process(
    "tth_nonbb", 5300,
    label=process_tth.label + r"(non $b\bar{b}$)",
    xsecs={
        13: process_tth.get_xsec(13) * (1. - BR_H_BB),
    },
)

# exclusive grouping by "bb" and "nonbb"
process_tth_bbgroup = process_tth.add_process(
    "tth_bbgroup", 5500,
    label=process_tth.label,
    color=process_tth.color,
    xsecs={
        13: process_tth.get_xsec(13),
    },
    processes=[process_tth_bb, process_tth_nonbb],
)


#
# QCD
#

process_qcd = od.Process(
    "qcd", 6000,
    label="QCD",
    color=(235, 230, 10),
)


#
# Other processes
#

process_others = od.Process(
    "others", 7000,
    label="Others",
    color=(134, 136, 138),
)
