# coding: utf-8

"""
Factory functions and tools to create common configuration objects.
"""

__all__ = []


import os
import math
import glob
import json
import functools
from collections import OrderedDict

import six
import law
import order as od
from order.util import join_root_selection as jrs

import hmc.config.processes as pc
import hmc.config.features as fexp
from hmc.util import DotDict


#
# common channel definition
#

channels = od.UniqueObjectIndex(od.Channel, [
    od.Channel("mutau", 1, label=r"$\mu\tau_{h}$",
        aux={"selection": ["pairType == 0"], "inv_selection": ["pairType != 0"]}),
    od.Channel("etau", 2, label=r"$e\tau_{h}$",
        aux={"selection": ["pairType == 1"], "inv_selection": ["pairType != 1"]}),
    od.Channel("tautau", 3, label=r"$\tau_{h}\tau_{h}$",
        aux={"selection": ["pairType == 2"], "inv_selection": ["pairType != 2"]}),
])


#
# selection expressions
#

os_sel = ["isOS == 1"]

reject_sel = ["pairType == -31415"]

mass_ellipse_sel = ["((tauH_SVFIT_mass - 116.)^2 / 35.^2 + (bH_mass_raw - 111.)^2 / 45.^2) < 1"]
mass_rect_sel = ["tauH_SVFIT_mass > 79.5", "tauH_SVFIT_mass < 152.5", "fatjet_softdropMass > 90",
    "fatjet_softdropMass < 160"]

eo_sel = DotDict(
    even=["EventNumber % 2 == 0"],
    odd=["EventNumber % 2 != 0"],
)

baseline_sel = DotDict(
    mutau=channels.n.mutau.x.selection + [
        "dau1_pt > 20", "abs(dau1_eta) < 2.1", "dau2_pt > 20", "abs(dau1_eta) < 2.3", "nleps == 0",
        "nbjetscand > 1", "isVBFtrigger == 0",
    ],
    etau=channels.n.etau.x.selection + [
        "dau1_pt > 20", "abs(dau1_eta) < 2.1", "dau2_pt > 20", "abs(dau1_eta) < 2.3", "nleps == 0",
        "nbjetscand > 1", "isVBFtrigger == 0",
    ],
    tautau=channels.n.tautau.x.selection + [
        "dau1_pt > 40", "abs(dau1_eta) < 2.1", "dau2_pt > 40", "abs(dau1_eta) < 2.1", "nleps == 0",
        "nbjetscand > 1", "isVBFtrigger == 0",
    ],
)

region_sel = DotDict(
    # opposite-sign, isolated (signal region)
    mutau_os_iso=channels.n.mutau.x.selection + [
        "isOS == 1", "dau1_iso < 0.15", "dau2_deepTauVsJet >= 5",
    ],
    etau_os_iso=channels.n.etau.x.selection + [
        "isOS == 1", "dau1_iso < 0.10", "dau2_deepTauVsJet >= 5",
    ],
    tautau_os_iso=channels.n.tautau.x.selection + [
        "isOS == 1", "dau1_deepTauVsJet >= 5", "dau2_deepTauVsJet >= 5",
    ],

    # opposite-sign, inverted isolation
    mutau_os_inviso=channels.n.mutau.x.selection + [
        "isOS == 1", "dau1_iso < 0.15", "dau2_deepTauVsJet >= 2", "dau2_deepTauVsJet < 5",
    ],
    etau_os_inviso=channels.n.etau.x.selection + [
        "isOS == 1", "dau1_iso < 0.10", "dau2_deepTauVsJet >= 2", "dau2_deepTauVsJet < 5",
    ],
    tautau_os_inviso=channels.n.tautau.x.selection + [
        "isOS == 1", "dau1_deepTauVsJet >= 5", "dau2_deepTauVsJet >= 2", "dau2_deepTauVsJet < 5",
    ],

    # same-sign, isolated
    mutau_ss_iso=channels.n.mutau.x.selection + [
        "isOS == 0", "dau1_iso < 0.15", "dau2_deepTauVsJet >= 5",
    ],
    etau_ss_iso=channels.n.etau.x.selection + [
        "isOS == 0", "dau1_iso < 0.10", "dau2_deepTauVsJet >= 5",
    ],
    tautau_ss_iso=channels.n.tautau.x.selection + [
        "isOS == 0", "dau1_deepTauVsJet >= 5", "dau2_deepTauVsJet >= 5",
    ],

    # same-sign, inverted isolation
    mutau_ss_inviso=channels.n.mutau.x.selection + [
        "isOS == 0", "dau1_iso < 0.15", "dau2_deepTauVsJet >= 2", "dau2_deepTauVsJet < 5",
    ],
    etau_ss_inviso=channels.n.etau.x.selection + [
        "isOS == 0", "dau1_iso < 0.10", "dau2_deepTauVsJet >= 2", "dau2_deepTauVsJet < 5",
    ],
    tautau_ss_inviso=channels.n.tautau.x.selection + [
        "isOS == 0", "dau1_deepTauVsJet >= 5", "dau2_deepTauVsJet >= 2", "dau2_deepTauVsJet < 5",
    ],
)


#
# helper functions
#

# helper to scan a skim directory
def scan_dataset_files(skim_directory, skip_files=None, input_file_cache=None):
    files = []

    def read_basenames(index):
        if not os.path.exists(index):
            return None

        # read basenames from file
        try:
            with open(index, "r") as f:
                return [
                    os.path.basename(line.strip())
                    for line in f.readlines()
                    if line.strip().endswith(".root")
                ]
        except (IOError, OSError):
            return None

    for skim_directory in law.util.make_list(skim_directory):
        if input_file_cache and skim_directory in input_file_cache:
            files.extend(input_file_cache[skim_directory])
            continue

        if not os.path.exists(skim_directory):
            raise IOError("skim_directory '{}' does not exist".format(skim_directory))

        # try to read from goodfiles index
        index = os.path.join(skim_directory, "goodfiles.txt")
        basenames = read_basenames(index)

        if not basenames:
            # get all root files in that directory
            basenames = [
                os.path.basename(path)
                for path in glob.glob(os.path.join(skim_directory, "output_*.root"))
            ]

            # try to remove files from badfiles index
            index = os.path.join(skim_directory, "badfiles.txt")
            bad_basenames = read_basenames(index)
            if bad_basenames:
                basenames = [
                    basename for basename in basenames
                    if basename not in bad_basenames
                ]

        # build absolute paths
        directory_files = [os.path.join(skim_directory, basename) for basename in basenames]

        # add to the cache
        if isinstance(input_file_cache, dict):
            input_file_cache[skim_directory] = directory_files

        # extend files
        files.extend(directory_files)

    # filter by skip_files
    if skip_files:
        # deprecated, broken files no produce empty outputs that are handled properly
        # files = [f for f in files if f not in skip_files]
        raise Exception("skip_files should not be used any longer")

    return files


# helper to combine selections, but keep them separated per channel
# all selections should have the structure "{mutau: [], etau: [], tautau: []}"
# the returned structure is identical with all selections combined via "or" per channel
def combine_selections_per_channel(*selections):
    selections = law.util.make_list(selections)
    return DotDict(
        (ch, [jrs([jrs(sel[ch], op="and") for sel in selections], op="or")])
        for ch in channels.names()
    )


# helper to combine selections, across channels
# selections should have the structure "{mutau: [], etau: [], tautau: []}"
# the returned value is a string describing the "or" combination across channels
def combine_selections_across_channels(selections):
    return jrs([jrs(sel, op="and") for sel in selections.values()], op="or")


# helper to create a category object
def category(name, children=None, weight="1", aux=None, **kwargs):
    aux = aux or {}
    aux.setdefault("children", law.util.make_list(children) if children else [])
    aux.setdefault("weight", weight)
    aux.setdefault("composite", False)

    # add even / odd flags
    aux.setdefault("is_even", name.endswith("_even"))
    aux.setdefault("is_odd", name.endswith("_odd"))
    aux["is_eo"] = aux["is_even"] or aux["is_odd"]

    # when the category has no children and is even/odd,
    # set the corresponding baseline_even/odd category as the "base_category"
    base_category = None
    if not aux["children"] and aux["is_eo"]:
        base_category = "baseline_" + ("even" if aux["is_even"] else "odd")
    aux.setdefault("base_category", base_category)

    return od.Category(name, selection_mode="root", aux=aux, **kwargs)


# helper to create a region object
def region(name, weight=None, aux=None, **kwargs):
    aux = aux or {}
    aux.setdefault("weight", weight)

    return od.Category(name, selection_mode="root", aux=aux, **kwargs)


# helper to create a dataset object
def dataset(name, id, dirnames, process, tags=None, merging=None, splitting=None, selection=None,
        weight=None, aux=None, skip_files=None, input_file_cache=None, **kwargs):
    # read input files
    input_files = scan_dataset_files(dirnames, skip_files=skip_files,
        input_file_cache=input_file_cache)

    if not merging:
        merging = {}

    default_splitting = 0.75
    if not splitting:
        splitting = {}
    elif not isinstance(splitting, dict):
        default_splitting = splitting
        splitting = {}

    processes = law.util.make_list(process)

    aux = aux or {}
    aux.setdefault("default_splitting", default_splitting)
    aux.setdefault("splitting", splitting)
    aux.setdefault("merging", merging)
    aux.setdefault("multi_dir", isinstance(dirnames, (list, tuple)) and len(dirnames) > 1)
    aux.setdefault("selection", selection)
    aux.setdefault("weight", weight)

    dataset = od.Dataset(name, id, keys=input_files, processes=processes, tags=tags, aux=aux,
        **kwargs)
    return dataset


# helper to create a feature object
def feature(name, expression=None, default=-999., missing=None, overflow=False, underflow=False,
        aux=None, **kwargs):
    expression = expression or name
    missing = set(missing or ()) | {-999., -9999., -1.e3, -1.e4}

    aux = aux or {}
    aux.setdefault("default", default)
    aux.setdefault("missing", missing)
    aux.setdefault("overflow", overflow)
    aux.setdefault("underflow", underflow)
    kwargs.setdefault("binning", (10, 0., 1.))
    kwargs.setdefault("x_title", name)
    kwargs.setdefault("y_title", "Events")

    return od.Variable(name, expression=expression, selection_mode="root", aux=aux,
        **kwargs)


# helper to create a unique object index
def unique_index(cls, objects, **kwargs):
    # flatten and filter
    objects = [obj for obj in law.util.flatten(objects) if obj is not None]

    return od.UniqueObjectIndex(cls, objects, **kwargs)


#
# factory functions
#

def create_config(name, year, ecm, lumi, plot_weights, btag):
    # combined plot weight
    combined_plot_weight = " * ".join(
        "(({}) + (({}) * ({})))".format(
            jrs(channel.x.inv_selection),
            jrs(channel.x.selection),
            plot_weights[channel.name],
        ) for channel in channels
    )

    config = DotDict(
        name=name,
        year=year,
        ecm=ecm,
        lumi=lumi,
        plot_weights=plot_weights,
        combined_plot_weight=combined_plot_weight,
        btag=btag,
        x=DotDict(),
    )

    return config


def add_selections(config):
    sel = DotDict()

    # selections are synchronized to (e.g.)
    # https://github.com/LLRCMS/KLUBAnalysis/blob/VBF_legacy/config/selectionCfg_TauTau_Legacy2018.cfg  # noqa

    # btag selections
    df = lambda i, op, wp: "bjet{}_bID_deepFlavor {} {}".format(i, op, config.btag[wp])
    sel["btag"] = DotDict(
        m_first=[df(1, ">", "medium")],
        m_any=[jrs(df(1, ">", "medium"), df(2, ">", "medium"), op="or")],
        m_one_any=[jrs(
            jrs(df(1, ">", "medium"), df(2, "<", "medium"), op="and"),
            jrs(df(2, ">", "medium"), df(1, "<", "medium"), op="and"),
            op="or",
        )],
        l=[df(1, ">", "loose"), df(2, "<", "loose")],
        ll=[df(1, ">", "loose"), df(2, ">", "loose")],
        m=[df(1, ">", "medium"), df(2, "<", "medium")],
        mm=[df(1, ">", "medium"), df(2, ">", "medium")],
        not_mm=[df(1, "<", "medium"), df(2, "<", "medium")],
    )

    # baseline
    sel["baseline"] = baseline_sel

    # exclusive loose vbf cuts to be used in actual vbf loose selection on top of baseline
    # and via inversion (!) in boosted and resolved selections, both w/ and w/o b cuts
    _excl_vbf_loose_nob = ["isVBF == 1", "VBFjj_mass > 500", "VBFjj_deltaEta > 3"]
    _excl_vbf_loose = _excl_vbf_loose_nob + sel.btag.m_any  # TODO: use m_any?

    # inverted version
    _excl_non_vbf_loose = ["!" + jrs(_excl_vbf_loose, bracket=True)]

    # VBF loose, w/ and w/o b cuts
    sel["vbf_loose_nob"] = DotDict({
        ch: sel.baseline[ch] + _excl_vbf_loose_nob
        for ch in channels.names()
    })
    sel["vbf_loose"] = DotDict({
        ch: sel.baseline[ch] + _excl_vbf_loose
        for ch in channels.names()
    })

    sel["vbf_tight_nob"] = DotDict(
        mutau=reject_sel,  # category not used, should always reject
        etau=reject_sel,  # category not used, should always reject
        tautau=[
            "pairType == 2", "dau1_pt > 25", "abs(dau1_eta) < 2.1", "dau2_pt > 25",
            "abs(dau2_eta) < 2.1", "nleps == 0", "nbjetscand > 1", "isVBFtrigger == 1",
            "tauH_SVFIT_mass > 50", "VBFjj_mass > 800", "VBFjet1_pt > 140", "VBFjet2_pt > 60",
        ],
    )
    sel["vbf_tight"] = DotDict(
        mutau=reject_sel,  # category not used, should always reject
        etau=reject_sel,  # category not used, should always reject
        tautau=sel.vbf_tight_nob.tautau + sel.btag.m_any,
    )

    # combined VBF, w/ and w/o b cuts
    sel["vbf_nob"] = combine_selections_per_channel(sel.vbf_tight_nob, sel.vbf_loose_nob)
    sel["vbf"] = combine_selections_per_channel(sel.vbf_tight, sel.vbf_loose)

    # selection common to vbf_loose and vbf_tight
    sel["vbf_common"] = DotDict(
        mutau=[
            "pairType == 0", "dau1_pt > 20", "abs(dau1_eta) < 2.1", "dau2_pt > 20",
            "abs(dau1_eta) < 2.1", "nleps == 0", "nbjetscand > 1", "VBFjj_mass > 500",
        ],
        etau=[
            "pairType == 1", "dau1_pt > 20", "abs(dau1_eta) < 2.1", "dau2_pt > 20",
            "abs(dau1_eta) < 2.1", "nleps == 0", "nbjetscand > 1", "VBFjj_mass > 500",
        ],
        tautau=[
            "pairType == 2", "dau1_pt > 25", "abs(dau1_eta) < 2.1", "dau2_pt > 25",
            "abs(dau1_eta) < 2.1", "nleps == 0", "nbjetscand > 1", "VBFjj_mass > 500",
        ],
    )

    # resolved 1b
    sel["resolved_1b"] = DotDict({
        ch: (sel.baseline[ch] + sel.btag.m_one_any + mass_ellipse_sel + ["isBoosted != 1"]
            + _excl_non_vbf_loose)
        for ch in channels.names()
    })

    # resolved 2b
    sel["resolved_2b"] = DotDict({
        ch: (sel.baseline[ch] + sel.btag.mm + mass_ellipse_sel + ["isBoosted != 1"]
            + _excl_non_vbf_loose)
        for ch in channels.names()
    })

    # combined resolved
    sel["resolved"] = combine_selections_per_channel(sel.resolved_1b, sel.resolved_2b)

    # boosted
    sel["boosted"] = DotDict({
        ch: (sel.baseline[ch] + sel.btag.ll + mass_rect_sel + ["isBoosted == 1"]
            + _excl_non_vbf_loose)
        for ch in channels.names()
    })

    # vr
    sel["vr"] = combine_selections_per_channel(sel.resolved_1b, sel.resolved_2b, sel.vbf_tight,
        sel.vbf_loose)

    # combinations across channels
    sel["baseline_combined"] = [combine_selections_across_channels(sel.baseline)]
    sel["vbf_loose_nob_combined"] = [combine_selections_across_channels(sel.vbf_loose_nob)]
    sel["vbf_tight_nob_combined"] = [combine_selections_across_channels(sel.vbf_tight_nob)]
    sel["vbf_nob_combined"] = [combine_selections_across_channels(sel.vbf_nob)]
    sel["vbf_loose_combined"] = [combine_selections_across_channels(sel.vbf_loose)]
    sel["vbf_tight_combined"] = [combine_selections_across_channels(sel.vbf_tight)]
    sel["vbf_combined"] = [combine_selections_across_channels(sel.vbf)]
    sel["vbf_common_combined"] = [combine_selections_across_channels(sel.vbf_common)]
    sel["resolved_1b_combined"] = [combine_selections_across_channels(sel.resolved_1b)]
    sel["resolved_2b_combined"] = [combine_selections_across_channels(sel.resolved_2b)]
    sel["resolved_combined"] = [combine_selections_across_channels(sel.resolved)]
    sel["boosted_combined"] = [combine_selections_across_channels(sel.boosted)]
    sel["vr_combined"] = [combine_selections_across_channels(sel.vr)]

    config.selections = sel


def add_categories(config):
    _category = functools.partial(category, weight=config.combined_plot_weight)
    with od.uniqueness_context(config.name):
        categories = unique_index(od.Category, [
            # all events
            _category("all", children=["all_even", "all_odd"], label="All events"),
            _category("all_even", label="Even event numbers", selection=eo_sel.even),
            _category("all_odd", label="odd event numbers", selection=eo_sel.odd),

            # baseline selection
            _category("baseline", children=["baseline_even", "baseline_odd"],
                label="Baseline category"),
            _category("baseline_even", label="Baseline category, even event numbers",
                selection=eo_sel.even + config.selections.baseline_combined),
            _category("baseline_odd", label="Baseline category, odd event numbers",
                selection=eo_sel.odd + config.selections.baseline_combined),

            # baseline_os selection
            _category("baseline_os", children=["baseline_os_even", "baseline_os_odd"],
                label="Baseline (OS) category", tags=["qcd_incompatible"]),
            _category("baseline_os_even", label="Baseline (OS) category, even event numbers",
                selection=os_sel + eo_sel.even + config.selections.baseline_combined,
                tags=["qcd_incompatible"]),
            _category("baseline_os_odd", label="Baseline (OS) category, odd event numbers",
                selection=os_sel + eo_sel.odd + config.selections.baseline_combined,
                tags=["qcd_incompatible"]),

            # vbf selection, without b-tag cuts
            _category("vbf_nob", children=["vbf_nob_even", "vbf_nob_odd"], label="VBF' category"),
            _category("vbf_nob_even", children=["vbf_tight_nob_even", "vbf_loose_nob_even"],
                label="VBF' category, even event numbers"),
            _category("vbf_nob_odd", children=["vbf_tight_nob_odd", "vbf_loose_nob_odd"],
                label="VBF' category, odd event numbers"),

            # vbf tight selection, without b-tag cuts
            _category("vbf_tight_nob", children=["vbf_tight_nob_even", "vbf_tight_nob_odd"],
                label="VBF' (tight) category"),
            _category("vbf_tight_nob_even", label="VBF' (tight) category, even event numbers",
                selection=eo_sel.even + config.selections.vbf_tight_nob_combined),
            _category("vbf_tight_nob_odd", label="VBF' (tight) category, odd event numbers",
                selection=eo_sel.odd + config.selections.vbf_tight_nob_combined),

            # vbf loose selection, without b-tag cuts
            _category("vbf_loose_nob", children=["vbf_loose_nob_even", "vbf_loose_nob_odd"],
                label="VBF' (loose) category"),
            _category("vbf_loose_nob_even", label="VBF' (loose) category, even event numbers",
                selection=eo_sel.even + config.selections.vbf_loose_nob_combined),
            _category("vbf_loose_nob_odd", label="VBF' (loose) category, odd event numbers",
                selection=eo_sel.odd + config.selections.vbf_loose_nob_combined),

            # vbf selection
            _category("vbf", children=["vbf_even", "vbf_odd"], label="VBF category"),
            _category("vbf_even", label="VBF category, even event numbers",
                selection=eo_sel.even + config.selections.vbf_combined),
            _category("vbf_odd", label="VBF category, odd event numbers",
                selection=eo_sel.odd + config.selections.vbf_combined),

            # vbf tight selection
            _category("vbf_tight", children=["vbf_tight_even", "vbf_tight_odd"],
                label="VBF (tight) category"),
            _category("vbf_tight_even", label="VBF (tight) category, even event numbers",
                selection=eo_sel.even + config.selections.vbf_tight_combined),
            _category("vbf_tight_odd", label="VBF (tight) category, odd event numbers",
                selection=eo_sel.odd + config.selections.vbf_tight_combined),

            # vbf loose selection
            _category("vbf_loose", children=["vbf_loose_even", "vbf_loose_odd"],
                label="VBF (loose) category"),
            _category("vbf_loose_even", label="VBF (loose) category, even event numbers",
                selection=eo_sel.even + config.selections.vbf_loose_combined),
            _category("vbf_loose_odd", label="VBF (loose) category, odd event numbers",
                selection=eo_sel.odd + config.selections.vbf_loose_combined),

            # vbf loose os selection
            _category("vbf_loose_os", children=["vbf_loose_os_even", "vbf_loose_os_odd"],
                label="VBF (loose, OS) category", tags=["qcd_incompatible"]),
            _category("vbf_loose_os_even", label="VBF (loose, OS) category, even event numbers",
                selection=os_sel + eo_sel.even + config.selections.vbf_loose_combined,
                tags=["qcd_incompatible"]),
            _category("vbf_loose_os_odd", label="VBF (loose, OS) category, odd event numbers",
                selection=os_sel + eo_sel.odd + config.selections.vbf_loose_combined,
                tags=["qcd_incompatible"]),

            # vbf_nob_os selection
            _category("vbf_nob_os", children=["vbf_nob_os_even", "vbf_nob_os_odd"],
                label="VBF' (OS) category", tags=["qcd_incompatible"]),
            _category("vbf_nob_os_even", label="VBF' (OS) category, even event numbers",
                selection=os_sel + eo_sel.even + config.selections.vbf_nob_combined,
                tags=["qcd_incompatible"]),
            _category("vbf_nob_os_odd", label="VBF' (OS) category, odd event numbers",
                selection=os_sel + eo_sel.odd + config.selections.vbf_nob_combined,
                tags=["qcd_incompatible"]),

            # vbf_os selection
            _category("vbf_os", children=["vbf_os_even", "vbf_os_odd"],
                label="VBF (OS) category", tags=["qcd_incompatible"]),
            _category("vbf_os_even", label="VBF (OS) category, even event numbers",
                selection=os_sel + eo_sel.even + config.selections.vbf_combined,
                tags=["qcd_incompatible"]),
            _category("vbf_os_odd", label="VBF (OS) category, odd event numbers",
                selection=os_sel + eo_sel.odd + config.selections.vbf_combined,
                tags=["qcd_incompatible"]),

            # vbf_common selection
            _category("vbf_common", children=["vbf_common_even", "vbf_common_odd"],
                label="Common VBF category"),
            _category("vbf_common_even", label="Common VBF category, even event numbers",
                selection=eo_sel.even + config.selections.vbf_common_combined),
            _category("vbf_common_odd", label="Common VBF category, odd event numbers",
                selection=eo_sel.odd + config.selections.vbf_common_combined),

            # vbf_common_os selection
            _category("vbf_common_os", children=["vbf_common_os_even", "vbf_common_os_odd"],
                label="Common VBF (OS) category", tags=["qcd_incompatible"]),
            _category("vbf_common_os_even", label="Common VBF (OS) category, even event numbers",
                selection=os_sel + eo_sel.even + config.selections.vbf_common_combined,
                tags=["qcd_incompatible"]),
            _category("vbf_common_os_odd", label="Common VBF (OS) category, odd event numbers",
                selection=os_sel + eo_sel.odd + config.selections.vbf_common_combined,
                tags=["qcd_incompatible"]),

            # resolved selection
            _category("resolved", children=["resolved_even", "resolved_odd"],
                label="Resolved category"),
            _category("resolved_even", children=["resolved_1b_even", "resolved_2b_even"],
                label="Resolved category, even event numbers"),
            _category("resolved_odd", children=["resolved_1b_odd", "resolved_2b_odd"],
                label="Resolved category, odd event numbers"),

            # resolved 1b selection
            _category("resolved_1b", children=["resolved_1b_even", "resolved_1b_odd"],
                label="Resolved (1b) category"),
            _category("resolved_1b_even", label="Resolved (1b) category, even event numbers",
                selection=eo_sel.even + config.selections.resolved_1b_combined),
            _category("resolved_1b_odd", label="Resolved (1b) category, odd event numbers",
                selection=eo_sel.odd + config.selections.resolved_1b_combined),

            # resolved 2b selection
            _category("resolved_2b", children=["resolved_2b_even", "resolved_2b_odd"],
                label="Resolved (2b) category"),
            _category("resolved_2b_even", label="Resolved (2b) category, even event numbers",
                selection=eo_sel.even + config.selections.resolved_2b_combined),
            _category("resolved_2b_odd", label="Resolved (2b) category, odd event numbers",
                selection=eo_sel.odd + config.selections.resolved_2b_combined),

            # resolved_os selection
            _category("resolved_os", children=["resolved_os_even", "resolved_os_odd"],
                label="Resolved (OS) category", tags=["qcd_incompatible"]),
            _category("resolved_os_even", label="Resolved (OS) category, even event numbers",
                selection=os_sel + eo_sel.even + config.selections.resolved_combined,
                tags=["qcd_incompatible"]),
            _category("resolved_os_odd", label="Resolved (OS) category, odd event numbers",
                selection=os_sel + eo_sel.odd + config.selections.resolved_combined,
                tags=["qcd_incompatible"]),

            # boosted selection
            _category("boosted", children=["boosted_even", "boosted_odd"],
                label="Boosted category"),
            _category("boosted_even", label="Boosted category, even event numbers",
                selection=eo_sel.even + config.selections.boosted_combined),
            _category("boosted_odd", label="Boosted category, odd event numbers",
                selection=eo_sel.odd + config.selections.boosted_combined),

            # boosted_os selection
            _category("boosted_os", children=["boosted_os_even", "boosted_os_odd"],
                label="Boosted (OS) category"),
            _category("boosted_os_even", label="Boosted (OS) category, even event numbers",
                selection=os_sel + eo_sel.even + config.selections.boosted_combined),
            _category("boosted_os_odd", label="Boosted (OS) category, odd event numbers",
                selection=os_sel + eo_sel.odd + config.selections.boosted_combined),

            # vr (vbf+resolved) category
            _category("vr", children=["vr_even", "vr_odd"], label="VR category",
                aux={"vr_weights": True}),
            _category("vr_even", label="VR category, even event numbers",
                selection=eo_sel.even + config.selections.vr_combined, aux={"vr_weights": True}),
            _category("vr_odd", label="VR category, odd event numbers",
                selection=eo_sel.odd + config.selections.vr_combined, aux={"vr_weights": True}),

            # vr_os (vbf+resolved) category
            _category("vr_os", children=["vr_os_even", "vr_os_odd"], label="VR (OS) category",
                aux={"vr_weights": True}),
            _category("vr_os_even", label="VR (OS) category, even event numbers",
                selection=os_sel + eo_sel.even + config.selections.vr_combined,
                aux={"vr_weights": True}),
            _category("vr_os_odd", label="VR (OS) category, odd event numbers",
                selection=os_sel + eo_sel.odd + config.selections.vr_combined,
                aux={"vr_weights": True}),
        ])

        # define composite categories
        for parent in categories:
            if not parent.x.children:
                continue
            parent.x.composite = True
            for name in parent.x.children:
                parent.add_category(categories.get(name))

    config["categories"] = categories


def add_regions(config):
    with od.uniqueness_context(config.name):
        regions = od.UniqueObjectIndex(od.Category)

        # os/ss regions
        regions.add(region("os", selection=["isOS == 1"], label="OS"))
        regions.add(region("ss", selection=["isOS != 1"], label="SS"))

        # region per channel
        for channel in channels:
            # plain channel as a region
            regions.add(region(channel.name, selection=channel.x.selection,
                label="{} channel".format(channel.label)))

            # os/ss regions
            regions.add(region("{}_os".format(channel.name),
                selection=channel.x.selection + ["isOS == 1"],
                label="{} channel, OS".format(channel.label)))
            regions.add(region("{}_ss".format(channel.name),
                selection=channel.x.selection + ["isOS != 1"],
                label="{} channel, SS".format(channel.label)))

            # os/ss and isolation regions for QCD estimation
            regions.add(region("{}_os_iso".format(channel.name), tags=["qcd_os_iso"],
                label="Signal region ({} channel)".format(channel.label),
                selection=region_sel["{}_os_iso".format(channel.name)]),
                aux={"signal_region": True})
            regions.add(region("{}_os_inviso".format(channel.name), tags=["qcd_os_inviso"],
                label="OS inv. iso ({} channel)".format(channel.label),
                selection=region_sel["{}_os_inviso".format(channel.name)]))
            regions.add(region("{}_ss_iso".format(channel.name), tags=["qcd_ss_iso"],
                label="SS iso ({} channel)".format(channel.label),
                selection=region_sel["{}_ss_iso".format(channel.name)]))
            regions.add(region("{}_ss_inviso".format(channel.name), tags=["qcd_ss_inviso"],
                label="SS inv. iso ({} channel)".format(channel.label),
                selection=region_sel["{}_ss_inviso".format(channel.name)]))

        # combined QCD regions
        for name in ["os_iso", "os_inviso", "ss_iso", "ss_inviso"]:
            sel = combine_selections_across_channels({
                channel.name: regions.get("{}_{}".format(channel.name, name)).selection
                for channel in channels
            })
            label = "Signal region" if name == "os_iso" else name
            regions.add(region(name, label=label, selection=sel, tags=["qcd_" + name],
                aux={"signal_region": name == "os_iso"}))

    config["regions"] = regions


def add_datasets(config, dataset_specs):
    # create a hash over all involved directory names
    all_dirnames = sum((specs["dirnames"] for specs in dataset_specs.values()), [])
    dir_hash = law.util.create_hash(all_dirnames)
    input_file_cache_path = os.path.expandvars("$HMC_TMP_DIR/input_files_{}.json".format(dir_hash))
    input_file_cache = {}
    if os.path.exists(input_file_cache_path):
        try:
            with open(input_file_cache_path, "r") as f:
                input_file_cache = json.load(f)
            # print("using input file cache from {} for config {}".format(
            #     input_file_cache_path, config.name))
        except:
            print("error reading existing input file cache from {} for config {}".format(
                input_file_cache_path, config.name))
            os.remove(input_file_cache_path)

    with od.uniqueness_context(config.name):
        datasets = unique_index(od.Dataset, [
            dataset(name, input_file_cache=input_file_cache, **specs)
            for name, specs in dataset_specs.items()
        ])

        # save an aux flag to simplify the query if the config has the tth_tautau dataset
        config.x.has_tth_tautau = "tth_tautau" in datasets

        # define process groups for plotting
        process_groups = DotDict(
            default=unique_index(od.Process, [
                pc.process_hh_ggf,
                pc.process_hh_vbf,
                pc.process_tth_bbgroup,
                pc.process_tt_sl,
                pc.process_tt_dl,
                pc.process_tt_fh,
                pc.process_dy,
                pc.process_others,
                pc.process_data,
            ]),
            full=unique_index(od.Process, [
                pc.process_hh_ggf,
                pc.process_hh_vbf,
                pc.process_tth_bb,
                pc.process_tth_nonbb,
                pc.process_tt_sl,
                pc.process_tt_dl,
                pc.process_tt_fh,
                pc.process_dy,
                pc.process_others,
                pc.process_data,
            ]),
            confusion=unique_index(od.Process, [
                pc.process_hh_ggf,
                pc.process_hh_vbf,
                pc.process_hh_vbf_c2v,
                pc.process_tth_bb,
                pc.process_tth_tautau if config.x.has_tth_tautau else None,
                pc.process_tt_sl,
                pc.process_tt_dl,
                pc.process_tt_fh,
                pc.process_dy,
                pc.process_data,
            ]),
            confusion_merged=unique_index(od.Process, [
                pc.process_hh_ggf,
                pc.process_hh_vbf_sm_c2v,
                pc.process_tth_bbgroup,
                pc.process_tt_lep,
                pc.process_tt_fh,
                pc.process_dy,
                pc.process_data,
            ]),
            vbf_only=unique_index(od.Process, [
                pc.process_hh_vbf,
                pc.process_hh_vbf_0p5_1_1,
                pc.process_hh_vbf_1p5_1_1,
                pc.process_hh_vbf_1_0_1,
                pc.process_hh_vbf_1_2_1,
                pc.process_hh_vbf_1_1_0,
                pc.process_hh_vbf_1_1_2,
            ]),
        )

    config["datasets"] = datasets
    config["process_groups"] = process_groups

    # save the cache when not existing yet
    if not os.path.exists(input_file_cache_path):
        with open(input_file_cache_path, "w") as f:
            json.dump(input_file_cache, f, indent=4)


def add_training_config(config):
    with od.uniqueness_context(config.name):
        # add multiple, named training configurations, each one consisting of
        # processes, process_group_ids and datasets
        config["training"] = DotDict(
            default=DotDict(
                processes=unique_index(od.Process, [
                    pc.process_hh_ggf,
                    pc.process_hh_vbf,
                    pc.process_hh_vbf_c2v,
                    pc.process_tth_bb,
                    pc.process_tth_tautau if config.x.has_tth_tautau else None,
                    pc.process_tt_dl,
                    pc.process_tt_sl,
                    pc.process_tt_fh,
                    pc.process_dy,
                ]),
                common_processes=unique_index(od.Process, [
                    pc.process_hh_ggf,
                    pc.process_hh_vbf,
                    pc.process_hh_vbf_c2v,
                    pc.process_tth_bb,
                    pc.process_tth_tautau,
                    pc.process_tt_dl,
                    pc.process_tt_sl,
                    pc.process_tt_fh,
                    pc.process_dy,
                ]),
                process_group_ids=(
                    (1.0, (0, 1, 2)),  # HH combined
                    (1.0, tuple(range(3, 9 if config.x.has_tth_tautau else 8))),  # bkg's combined
                ),
                datasets=unique_index(od.Dataset, [
                    config.datasets.n.hh_ggf,
                    config.datasets.n.hh_vbf,
                    config.datasets.n.hh_vbf_c2v,
                    config.datasets.n.tth_bb,
                    config.datasets.n.tth_tautau if config.x.has_tth_tautau else None,
                    config.datasets.n.tt_dl,
                    config.datasets.n.tt_sl,
                    config.datasets.n.tt_fh,
                    config.datasets.n.dy_low,
                    config.datasets.n.dy_high,
                ])
            ),
        )


def add_features(config):
    with od.uniqueness_context(config.name):
        features = unique_index(od.Variable, law.util.flatten([
            # global features
            feature("event", "EventNumber", binning=(50, 0., 2.e8), x_title="Event number",
                tags=["eval_observer"]),
            feature("rho", "rho", binning=(40, 0., 80.), x_title=r"Energy density $\rho$"),
            feature("pu", "npu", binning=(40, 0., 80.), x_title="Pileup"),
            feature("channel", "pairType", default=-1, binning=(3, -0.5, 2.5), x_title="Channel",
                x_labels=[r"$\mu\tau$ (0)", r"$e\tau$ (1)", r"$\tau\tau$ (2)"],
                tags=["eval_observer"], underflow=False, overflow=False),
            [
                feature("is_" + channel.name, jrs(channel.x.selection), default=-1,
                    binning=(2, -0.5, 1.5), x_title=channel.label + " flag",
                    tags=["training", "lbn", "lbn_light"])
                for channel in channels
            ],
            [
                feature("is_{}".format(y), "{} == {}".format(y, config.year), default=-1.,
                    binning=(2, -0.5, 1.5), x_title="Year {}".format(y), aux={"is_flag": True},
                    tags=["training", "lbn", "lbn_light"])
                for y in [2016, 2017, 2018]
            ],
            feature("n_jets", "njets", default=-1, binning=(21, -0.5, 20.5),
                x_title="Number of jets"),
            feature("n_jets_20", "njets20", default=-1, binning=(16, -0.5, 15.5),
                x_title=r"Number of jets ($p_{T} > 20$)", tags=["training"]),
            feature("n_bjets_20", "nbjets20", default=-1, binning=(16, -0.5, 15.5),
                x_title=r"Number of b-tagged jets ($p_{T} > 20$)", tags=["training"]),

            # generator features
            feature("gen_dm1", "genDecMode1", default=-1, binning=(20, 0., 20.),
                x_title="Decaymode 1", tags=["skip_shards"]),
            feature("gen_dm2", "genDecMode2", default=-1, binning=(20, 0., 20.),
                x_title="Decaymode 2", tags=["skip_shards"]),
            feature("gen_hh_m", "genMHH", default=-1, unit="GeV", binning=(50, 0., 1500.),
                x_title=r"Generator $m_{HH}$", tags=["skip_shards"]),

            # simple categorical features
            feature("is_boosted", "isBoosted", default=-2, binning=(3, -1.5, 1.5),
                x_title="Boosted flag", aux={"is_flag": True},
                tags=["training", "lbn"]),
            feature("is_vbf", "isVBF", default=-1, binning=(2, -0.5, 1.5), x_title="VBF flag",
                aux={"is_flag": True}, tags=["training", "lbn"]),
            feature("is_vbf_trigger", "isVBFtrigger", default=-1, binning=(2, -0.5, 1.5),
                x_title="VBFTrigger flag", aux={"is_flag": True},
                tags=["training", "lbn"]),
            feature("is_os", "isOS", default=-1, binning=(2, -0.5, 1.5), x_title="OS flag",
                aux={"is_flag": True}, tags=["eval_observer"]),

            # full category flags
            [
                feature("is_{}_cat".format(name), config.selections[name + "_combined"][0] + "== 1",
                    default=-1, binning=(2, -0.5, 1.5), x_title="'{}' category".format(name),
                    aux={"is_flag": True}, tags=["training", "lbn"])
                for name in [
                    "vbf_loose", "vbf_tight", "vbf", "resolved_1b", "resolved_2b", "resolved",
                    "boosted",
                ]
            ],

            # weights
            feature("weight_mc", "MC_weight", default=-1, binning=(40, -2., 2.),
                tags=["eval_observer"]),
            feature("weight_pu", "PUReweight", default=-1, binning=(40, 0., 2.),
                tags=["eval_observer"]),
            feature("weight_trig", "trigSF", default=-1, binning=(40, 0.5, 1.5),
                tags=["eval_observer"]),
            feature("weight_l1", "L1pref_weight", default=-1, binning=(40, 0., 1.5),
                tags=["eval_observer"]),
            feature("weight_prescale", "prescaleWeight", default=-1, binning=(40, 0., 1.5),
                tags=["eval_observer"]),
            feature("weight_id_iso_fake_deep", "IdAndIsoAndFakeSF_deep", default=-1,
                binning=(40, 0.5, 1.5), tags=["eval_observer"]),
            feature("weight_id_iso_fake_deep_pt", "IdAndIsoAndFakeSF_deep_pt", default=-1,
                binning=(40, 0.5, 1.5), tags=["eval_observer"]),
            feature("weight_dy_scale_mtt", "DYscale_MTT", default=-1, binning=(40, 0., 2.),
                tags=["eval_observer"]),
            feature("weight_pujetid", "PUjetID_SF", default=-1, binning=(40, 0., 2.),
                tags=["eval_observer"]),
            feature("weight_top_pt", "TTtopPtreweight", default=-1, binning=(40, 0., 2.),
                tags=["eval_observer"]),
            feature("weight_plot", config.combined_plot_weight, default=-1, binning=(40, 0., 2.),
                tags=["training_event_weight"]),

            # b jet features
            ([
                feature("bjet{}_pt".format(i), "bjet{}_pt".format(i), default=-1., unit="GeV",
                    binning=(40, 0., 400.), x_title=r"b jet {} $p_{{T}}$".format(i),
                    tags=["training", "lbn", "lbn_light", "lbn_pt"]),
                feature("bjet{}_eta".format(i), "bjet{}_eta".format(i), default=-5.,
                    binning=(36, -3., 3.), x_title=r"b jet {} $\eta$".format(i),
                    tags=["training", "lbn", "lbn_light", "lbn_eta"]),
                feature("bjet{}_phi".format(i), "bjet{}_phi".format(i), default=-4., missing={-1.},
                    binning=(50, -3.2, 3.2), x_title=r"b jet {} $\phi$".format(i),
                    tags=["training", "lbn", "lbn_light", "lbn_phi"]),
                feature("bjet{}_e".format(i), "bjet{}_e".format(i), default=-1., unit="GeV",
                    binning=(50, 0., 1000.), x_title=r"b jet {} energy".format(i),
                    tags=["training", "lbn", "lbn_light", "lbn_e"]),
                feature("bjet{}_deepcsv_b".format(i), "bjet{}_bID_deepCSV".format(i), default=-1.,
                    binning=(50, 0., 1.), x_title=r"b jet {} DeepCSV".format(i)),
                feature("bjet{}_deepflavor_b".format(i), "bjet{}_bID_deepFlavor".format(i),
                    default=-1., binning=(50, 0., 1.), x_title=r"b jet {} DeepFlavor (b)".format(i),
                    tags=["training", "lbn", "lbn_light"]),
                feature("bjet{}_deepflavor_cvsb".format(i), "bjet{}_CvsB".format(i),
                    default=-1., binning=(50, 0., 1.),
                    x_title=r"b jet {} DeepFlavor (c vs. b)".format(i)),
                feature("bjet{}_deepflavor_cvsl".format(i), "bjet{}_CvsL".format(i),
                    default=-1., binning=(50, 0., 1.),
                    x_title=r"b jet {} DeepFlavor (c vs. l)".format(i)),
                feature("bjet{}_pujetid".format(i), "bjet{}_PUjetIDupdated".format(i), default=-1.,
                    binning=(10, -0.5, 9.5), x_title=r"b jet {} PUJetID".format(i)),
                feature("bjet{}_hhbtag".format(i), "bjet{}_HHbtag".format(i), default=-1.,
                    binning=(10, 0., 2.), x_title=r"b jet {} HHbtag".format(i),
                    tags=["training", "lbn", "lbn_light"]),
                feature("bjet{}_z".format(i), fexp.vbf_centrality("bjet{}".format(i)), default=-5.,
                    binning=(40, -4., 4.), x_title=r"b jet {} centrality".format(i),
                    tags=["training", "lbn"]),
            ] for i in range(1, 2 + 1)),

            # additional central jet features
            ([
                feature("ctjet{}_pt".format(i), "addJetCentr{}_pt".format(i), default=-1.,
                    unit="GeV", binning=(40, 0., 400.),
                    x_title=r"Central jet {} $p_{{T}}$".format(i),
                    tags=["training", "lbn", "lbn_light", "lbn_pt"] if i <= 3 else []),
                feature("ctjet{}_eta".format(i), "addJetCentr{}_eta".format(i), default=-5.,
                    binning=(50, -5., 5.), x_title=r"Central jet {} $\eta$".format(i),
                    tags=["training", "lbn", "lbn_light", "lbn_eta"] if i <= 3 else []),
                feature("ctjet{}_phi".format(i), "addJetCentr{}_phi".format(i), default=-4.,
                    binning=(50, -3.2, 3.2), x_title=r"Central jet {} $\phi$".format(i),
                    tags=["training", "lbn", "lbn_light", "lbn_phi"] if i <= 3 else []),
                feature("ctjet{}_e".format(i), "addJetCentr{}_e".format(i), default=-1., unit="GeV",
                    binning=(50, 0., 1000.), x_title=r"Central jet {} energy".format(i),
                    tags=["training", "lbn", "lbn_light", "lbn_e"] if i <= 3 else []),
                feature("ctjet{}_deepflavor_b".format(i), "addJetCentr{}_btag_deepFlavor".format(i),
                    default=-1., binning=(50, 0., 1.),
                    x_title=r"Central jet {} DeepFlavor (b)".format(i),
                    tags=["training", "lbn", "lbn_light"] if i <= 3 else []),
                feature("ctjet{}_hhbtag".format(i), "addJetCentr{}_HHbtag".format(i),
                    default=-1., binning=(10, 0., 2.), x_title=r"Central jet {} HHbtag".format(i),
                    tags=["training", "lbn", "lbn_light"] if i <= 3 else []),
            ] for i in range(1, 5 + 1)),

            # additional forward jet features
            ([
                feature("fwjet{}_pt".format(i), "addJetForw{}_pt".format(i), default=-1.,
                    unit="GeV", binning=(40, 0., 400.),
                    x_title=r"Forward jet {} $p_{{T}}$".format(i),
                    tags=["training", "lbn", "lbn_light", "lbn_pt"] if i <= 2 else []),
                feature("fwjet{}_eta".format(i), "addJetForw{}_eta".format(i), default=-5.,
                    binning=(50, -5., 5.), x_title=r"Forward jet {} $\eta$".format(i),
                    tags=["training", "lbn", "lbn_light", "lbn_eta"] if i <= 2 else []),
                feature("fwjet{}_phi".format(i), "addJetForw{}_phi".format(i), default=-4.,
                    binning=(50, -3.2, 3.2), x_title=r"Forward jet {} $\phi$".format(i),
                    tags=["training", "lbn", "lbn_light", "lbn_phi"] if i <= 2 else []),
                feature("fwjet{}_e".format(i), "addJetForw{}_e".format(i), default=-1., unit="GeV",
                    binning=(50, 0., 1000.), x_title=r"Forward jet {} energy".format(i),
                    tags=["training", "lbn", "lbn_light", "lbn_e"] if i <= 2 else []),
            ] for i in range(1, 5 + 1)),

            # VBF jet features
            ([
                feature("vbfjet{}_pt".format(i), "VBFjet{}_pt".format(i), default=-1., unit="GeV",
                    binning=(50, 0., 500.), x_title=r"VBF jet {} $p_{{T}}$".format(i),
                    underflow=False, tags=["training", "lbn", "lbn_light", "lbn_pt"]),
                feature("vbfjet{}_eta".format(i), "VBFjet{}_eta".format(i), default=-5.,
                    binning=(50, -5., 5.), x_title=r"VBF jet {} $\eta$".format(i),
                    underflow=False, tags=["training", "lbn", "lbn_light", "lbn_eta"]),
                feature("vbfjet{}_phi".format(i), "VBFjet{}_phi".format(i), default=-4.,
                    binning=(50, -3.2, 3.2), x_title=r"VBF jet {} $\phi$".format(i),
                    underflow=False, tags=["training", "lbn", "lbn_light", "lbn_phi"]),
                feature("vbfjet{}_e".format(i), "VBFjet{}_e".format(i), default=-1., unit="GeV",
                    binning=(50, 0., 2000.), x_title=r"VBF jet {} energy".format(i),
                    underflow=False, tags=["training", "lbn", "lbn_light", "lbn_e"]),
                feature("vbfjet{}_deepcsv_b".format(i), "VBFjet{}_btag_deepCSV".format(i),
                    default=-1., binning=(50, 0., 1.), x_title=r"VBF jet {} DeepCSV".format(i)),
                feature("vbfjet{}_deepflavor_b".format(i), "VBFjet{}_btag_deepFlavor".format(i),
                    default=-1., binning=(50, 0., 1.),
                    x_title=r"VBF jet {} DeepFlavor (b)".format(i),
                    tags=["training", "lbn", "lbn_light"]),
                feature("vbfjet{}_deepflavor_cvsb".format(i), "VBFjet{}_CvsB".format(i),
                    default=-1., binning=(50, 0., 1.),
                    x_title=r"VBF jet {} DeepFlavor (c vs. b)".format(i)),
                feature("vbfjet{}_deepflavor_cvsl".format(i), "VBFjet{}_CvsL".format(i),
                    default=-1., binning=(50, 0., 1.),
                    x_title=r"VBF jet {} DeepFlavor (c vs. l)".format(i)),
                feature("vbfjet{}_pujetid".format(i), "VBFjet{}_PUjetIDupdated".format(i),
                    default=-1., binning=(10, -0.5, 9.5), x_title=r"VBF jet {} PUJetID".format(i)),
                feature("vbfjet{}_hhbtag".format(i), "VBFjet{}_HHbtag".format(i), default=-1.,
                    binning=(10, 0., 2.), x_title=r"VBF jet {} HHbtag".format(i),
                    tags=["training", "lbn", "lbn_light"]),
            ] for i in range(1, 2 + 1)),

            # generic lepton features
            ([
                feature("lep{}_pt".format(i), "dau{}_pt".format(i), default=-1., unit="GeV",
                    binning=(40, 0., 400.), x_title=r"Lepton {} $p_{{T}}$".format(i),
                    tags=["training", "lbn", "lbn_light", "lbn_pt"]),
                feature("lep{}_eta".format(i), "dau{}_eta".format(i), default=-5.,
                    binning=(50, -3., 3.), x_title=r"Lepton {} $\eta$".format(i),
                    tags=["training", "lbn", "lbn_light", "lbn_eta"]),
                feature("lep{}_phi".format(i), "dau{}_phi".format(i), default=-4.,
                    binning=(50, -3.2, 3.2), x_title=r"Lepton {} $\phi$".format(i),
                    tags=["training", "lbn", "lbn_light", "lbn_phi"]),
                feature("lep{}_e".format(i), "dau{}_e".format(i), default=-1.,
                    binning=(50, 0., 500.), x_title=r"Lepton {} energy".format(i),
                    tags=["training", "lbn", "lbn_light", "lbn_e"]),
                feature("lep{}_iso".format(i), "dau{}_iso".format(i), default=-1.,
                    binning=(50, 0., 1.), x_title=r"Lepton {} isolation".format(i),
                    tags=["eval_observer"]),
                feature("lep{}_flav".format(i), "dau{}_flav".format(i), default=-1.,
                    binning=(11, -5.5, 5.5), x_title=r"Lepton {} flavor".format(i),
                    tags=["training"]),
                feature("lep{}_dxy".format(i), "dau{}_dxy".format(i), default=-1., unit="cm",
                    binning=(48, -0.06, 0.06), x_title=r"Lepton {} dxy".format(i)),
                feature("lep{}_dz".format(i), "dau{}_dz".format(i), default=-1., unit="cm",
                    binning=(48, -0.06, 0.06), x_title=r"Lepton {} dz".format(i)),
                feature("lep{}_z".format(i), fexp.vbf_centrality("dau{}".format(i)), default=-5.,
                    binning=(40, -4., 4.), x_title=r"Lepton {} centrality".format(i),
                    tags=["training", "lbn"]),
            ] for i in range(1, 2 + 1)),

            # tau features
            ([
                feature("tau{}_deeptau_jet".format(i), "dau{}_deepTauVsJet".format(i), default=-1.,
                    binning=(50, 0., 10.), x_title=r"Lepton {} DeepTauVsJet".format(i),
                    tags=["eval_observer"]),
                feature("tau{}_deeptau_mu".format(i), "dau{}_deepTauVsMu".format(i), default=-1.,
                    binning=(50, 0., 10.), x_title=r"Lepton {} DeepTauVsMu".format(i)),
                feature("tau{}_deeptau_e".format(i), "dau{}_deepTauVsEle".format(i), default=-1.,
                    binning=(50, 0., 10.), x_title=r"Lepton {} DeepTauVsEle".format(i)),
                feature("tau{}_dm".format(i), "dau{}_decayMode".format(i), default=-1.,
                    binning=(20, -0.5, 19.5), x_title=r"$\tau_{{h}}$ {} decay mode".format(i)),
                [
                    feature("tau{}_dm{}".format(i, dm), "dau{}_decayMode == {}".format(i, dm),
                        default=-1, binning=(2, -0.5, 1.5), aux={"is_flag": True},
                        x_title=r"$\tau_{{h}}$ {} DM '{}' flag".format(i, dm),
                        tags=["training"])
                    for dm in [0, 1, 10, 11]
                ],
            ] for i in range(1, 2 + 1)),

            # met features
            feature("met_pt", "met_et", default=-1., unit="GeV", binning=(40, 0., 400.),
                x_title=r"MET $p_{T}$", tags=["training", "lbn", "lbn_light", "lbn_met", "lbn_pt"]),
            feature("met_phi", "met_phi", default=-4., binning=(50, 0., 6.4),
                x_title=r"MET $\phi$", tags=["training", "lbn", "lbn_light", "lbn_met", "lbn_phi"]),
            feature("met_cov00", "met_cov00", default=-1., unit="$GeV^{2}$",
                binning=(40, 0., 4000.), x_title=r"MET $Cov_{00}$", tags=["training", "lbn"]),
            feature("met_cov01", "met_cov01", default=-1., unit="$GeV^{2}$",
                binning=(40, 0., 400.), x_title=r"MET $Cov_{01}$", tags=["training", "lbn"]),
            feature("met_cov10", "met_cov10", default=-1., unit="$GeV^{2}$",
                binning=(40, 0., 400.), x_title=r"MET $Cov_{10}$"),
            feature("met_cov11", "met_cov11", default=-1., unit="$GeV^{2}$",
                binning=(40, 0., 4000.), x_title=r"MET $Cov_{11}$"),

            # bH features (from kinematic bb reco)
            feature("bh_pt", "bH_pt", default=-1., unit="GeV", binning=(40, 0., 400.),
                x_title=r"$H (b\bar{b})$ $p_{T}$",
                tags=["training", "lbn", "lbn_light", "lbn_pt"]),
            feature("bh_eta", "bH_eta", default=-5., binning=(50, -5., 5.),
                x_title=r"$H (b\bar{b})$ $\eta$",
                tags=["training", "lbn", "lbn_light", "lbn_eta"]),
            feature("bh_phi", "bH_phi", default=-4., binning=(50, -3.2, 3.2),
                x_title=r"$H (b\bar{b})$ $\phi$",
                tags=["training", "lbn", "lbn_light", "lbn_phi"]),
            feature("bh_e", "bH_e", default=-1., unit="GeV", binning=(50, 0., 1000.),
                x_title=r"$H (b\bar{b})$ energy",
                tags=["training", "lbn", "lbn_light", "lbn_e"]),
            feature("bh_m", "bH_mass", default=-1., unit="GeV", binning=(40, 0., 400.),
                x_title=r"$H (b\bar{b})$ mass", tags=["training", "lbn"]),
            feature("bh_z", fexp.vbf_centrality("bH"), default=-5., binning=(40, -4., 4.),
                x_title=r"$H (b\bar{b})$ centrality", tags=["training", "lbn"]),

            # tauH features (from kinematic tau reco, without MET considerations)
            feature("tauh_kin_pt", "tauH_pt", default=-1., unit="GeV", binning=(40, 0., 400.),
                x_title=r"$H (\tau^{+}\tau^{-})$ (kin. fit) $p_{T}$"),
            feature("tauh_kin_eta", "tauH_eta", default=-5., binning=(50, -5., 5.),
                x_title=r"$H (\tau^{+}\tau^{-})$ (kin. fit) $\eta$"),
            feature("tauh_kin_phi", "tauH_phi", default=-4., binning=(50, -3.2, 3.2),
                x_title=r"$H (\tau^{+}\tau^{-})$ (kin. fit) $\phi$"),
            feature("tauh_kin_e", "tauH_e", default=-1., unit="GeV", binning=(50, 0., 1000.),
                x_title=r"$H (\tau^{+}\tau^{-})$ (kin. fit) energy"),
            feature("tauh_kin_m", "tauH_mass", default=-1., unit="GeV", binning=(40, 0., 400.),
                x_title=r"$H (\tau^{+}\tau^{-})$ (kin. fit) mass"),
            feature("tauh_kin_z", fexp.vbf_centrality("tauH"), default=-5., binning=(40, -4., 4.),
                x_title=r"$H (\tau^{+}\tau^{-})$ (kin. fit) centrality"),

            # tauH SVFit features (from SVFit tau reco, including MET considerations)
            feature("tauh_sv_pt", "tauH_SVFIT_pt", default=-1., unit="GeV", binning=(40, 0., 400.),
                x_title=r"$H (\tau^{+}\tau^{-})$ (SVFit) $p_{T}$",
                tags=["training", "lbn", "lbn_light", "lbn_pt"]),
            feature("tauh_sv_eta", "tauH_SVFIT_eta", default=-5., binning=(50, -5., 5.),
                x_title=r"$H (\tau^{+}\tau^{-})$ (SVFit) $\eta$",
                tags=["training", "lbn", "lbn_light", "lbn_eta"]),
            feature("tauh_sv_phi", "tauH_SVFIT_phi", default=-4., binning=(50, -3.2, 3.2),
                x_title=r"$H (\tau^{+}\tau^{-})$ (SVFit) $\phi$",
                tags=["training", "lbn", "lbn_light", "lbn_phi"]),
            feature("tauh_sv_e", fexp.energy_from_pt_eta_m("tauH_SVFIT"), default=-1., unit="GeV",
            # feature("tauh_sv_e", fexp.energy_z_from_pt_eta_m("tauH_SVFIT"), default=-1., unit="GeV",
                binning=(40, 0., 400.), x_title=r"$H (\tau^{+}\tau^{-})$ (SVFit) energy",
                tags=["training", "lbn", "lbn_light", "lbn_e"]),
            feature("tauh_sv_m", "tauH_SVFIT_mass", default=-1., unit="GeV",
                binning=(40, 0., 400.), x_title=r"$H (\tau^{+}\tau^{-})$ (SVFit) mass",
                tags=["training", "lbn"]),
            feature("tauh_sv_z", fexp.vbf_centrality("tauH_SVFIT"), default=-5.,
                binning=(40, -4., 4.), x_title=r"$H (\tau^{+}\tau^{-})$ (SVFIT) centrality",
                tags=["training", "lbn"]),

            # pairwise and composite features
            feature("mt1", "mT1", default=-1, unit="GeV", binning=(50, 0., 500.),
                x_title=r"MT_{1}", tags=["training", "lbn"]),
            feature("mt2", "mT2", default=-1, unit="GeV", binning=(50, 0., 500.),
                x_title=r"MT_{2}", tags=["training", "lbn"]),
            feature("ht_20", "HT20", default=-1, binning=(40, 0., 1200.),
                x_title=r"$H_{T}$ ($p_{T} > 20$)", tags=["training", "lbn"]),
            feature("bh_dr", "dib_deltaR", default=-1, binning=(36, 0., 6.),
                x_title=r"$\Delta R(b,\bar{b})$", tags=["training", "lbn"]),
            feature("bh_deta", "dib_deltaEta", default=-1, binning=(25, 0., 5.),
                x_title=r"$\Delta\eta(b,\bar{b})$", tags=["training", "lbn"]),
            feature("bh_dphi", "dib_deltaPhi", default=-1, binning=(32, 0., 3.2),
                x_title=r"$\Delta\phi(b,\bar{b})$", tags=["training", "lbn"]),
            feature("bh_met_dr", "bH_MET_deltaR", default=-1, binning=(36, 0., 6.),
                x_title=r"$\Delta R(b+\bar{b},MET)$", tags=["training", "lbn"]),
            feature("bh_met_deta", "bH_MET_deltaEta", default=-1, binning=(25, 0., 5.),
                x_title=r"$\Delta\eta(b+\bar{b},MET)$", tags=["training", "lbn"]),
            feature("tauh_dr", "ditau_deltaR", default=-1, binning=(36, 0., 6.),
                x_title=r"$\Delta R(\tau^{+},\tau^{-})$", tags=["training", "lbn"]),
            feature("tauh_deta", "ditau_deltaEta", default=-1, binning=(25, 0., 5.),
                x_title=r"$\Delta\eta(\tau^{+},\tau^{-})$", tags=["training", "lbn"]),
            feature("tauh_dphi", "ditau_deltaPhi", default=-1, binning=(32, 0., 3.2),
                x_title=r"$\Delta\phi(\tau^{+},\tau^{-})$", tags=["training", "lbn"]),
            feature("lep1_met_dphi", "dau1MET_deltaphi", default=-1, binning=(32, 0., 3.2),
                x_title=r"$\Delta\phi(lep1,MET)$", tags=["training", "lbn"]),
            feature("lep2_met_dphi", "dau2MET_deltaphi", default=-1, binning=(32, 0., 3.2),
                x_title=r"$\Delta\phi(lep2,MET)$", tags=["training", "lbn"]),
            feature("btau_dr_min", "btau_deltaRmin", default=-1., binning=(36, 0., 6.),
                x_title=r"Minimum $\Delta R_{b,\tau}$", tags=["training", "lbn"]),
            feature("btau_dr_max", "btau_deltaRmax", default=-1., binning=(36, 0., 6.),
                x_title=r"Maximum $\Delta R_{b,\tau}$", tags=["training", "lbn"]),
            feature("vbfjj_deta", "VBFjj_deltaEta", default=-1., binning=(40, 0., 8.),
                x_title=r"$VBF_{jj} \Delta\eta$", tags=["training", "lbn"]),
            feature("vbfjj_dphi", "VBFjj_deltaPhi", default=-1., binning=(32, 0., 3.2),
                x_title=r"$VBF_{jj} \Delta\phi$", tags=["training", "lbn"]),
            feature("vbfjj_m", "VBFjj_mass", default=-1., unit="GeV", binning=(40, 0., 1200.),
                x_title=r"$M_{jj,VBF}$", tags=["training", "lbn"]),
            feature("vbfjj_eta_min", fexp.min_eta("VBFjet1", "VBFjet2"), default=-5.,
                binning=(25, 0., 5.), x_title=r"Minimum $\eta_{VBFjj}$", tags=["training", "lbn"]),
            feature("deta_hbb_svfit", fexp.abs_delta_eta("tauH_SVFIT", "bH"), default=-1.,
                binning=(50, 0., 10.), x_title=r"$\Delta\eta(H(bb),H(\tau\tau, SVFit))$",
                tags=["training", "lbn"]),

            # hh features
            feature("hh_m", "HH_mass", default=-1., unit="GeV", binning=(40, 0., 1200.),
                x_title=r"$M_{HH}$"),
            feature("hh_e", "HH_e", default=-1., unit="GeV", binning=(40, 0., 1200.),
                x_title=r"$E_{HH}$"),
            feature("hh_dr", "HH_deltaR", default=-1., binning=(32, 0., 6.),
                x_title=r"$\Delta R_{HH}$", tags=["training", "lbn"]),
            feature("hh_z", fexp.vbf_centrality("HH"), default=-5., binning=(40, -4., 4.),
                x_title=r"HH centrality", tags=["training", "lbn"]),
            feature("hh_deta", "HH_deltaEta", default=-1., binning=(25, 0., 5.),
                x_title=r"$\Delta\eta_{HH}$", tags=["training", "lbn"]),
            feature("hh_dphi", "HH_deltaPhi", default=-1., binning=(32, 0., 3.2),
                x_title=r"$\Delta\phi_{HH}$", tags=["training", "lbn"]),
            feature("hh_m_kin", "HHKin_mass", default=-1., missing={-1., 0.}, unit="GeV",
                binning=(40, 0., 1200.), x_title=r"$M_{HH}$ (kin. fit)", tags=["training", "lbn"]),
            feature("hh_chi2_kin", "HHKin_chi2", default=-1., binning=(40, 0., 1200.),
                x_title=r"\chi^{2} (kin. fit)", tags=["training", "lbn"]),
            feature("hh_m_sv", "HHsvfit_mass", default=-1., unit="GeV", binning=(40, 0., 1200.),
                x_title=r"$M_{HH}$ (svfit)"),
            feature("hh_m_kinsv", "HHkinsvfit_m", default=-1., unit="GeV", binning=(40, 0., 1200.),
                x_title=r"$M_{HH}$ (kinsvfit)"),

            # features as used by the BDT
            feature("BDT_dib_abs_deltaPhi", "BDT_dib_abs_deltaPhi", default=-1,
                binning=(32, 0., 3.2), tags=["bdt", "skip_shards"]),
            feature("BDT_dib_deltaPhi", "BDT_dib_deltaPhi", default=-1, binning=(32, 0., 3.2),
                tags=["bdt", "skip_shards"]),
            feature("BDT_dau1MET_deltaPhi", "BDT_dau1MET_deltaPhi", default=-1,
                binning=(32, 0., 3.2), tags=["bdt", "skip_shards"]),
            feature("BDT_tauHsvfitMet_abs_deltaPhi", "BDT_tauHsvfitMet_abs_deltaPhi", default=-1,
                binning=(32, 0., 3.2), tags=["bdt", "skip_shards"]),
            feature("BDT_tauHsvfitMet_deltaPhi", "BDT_tauHsvfitMet_deltaPhi", default=-1,
                binning=(32, 0., 3.2), tags=["bdt", "skip_shards"]),
            feature("BDT_bHMet_deltaPhi", "BDT_bHMet_deltaPhi", default=-1, binning=(32, 0., 3.2),
                tags=["bdt", "skip_shards"]),
            feature("BDT_HHsvfit_abs_deltaPhi", "BDT_HHsvfit_abs_deltaPhi", default=-1,
                binning=(32, 0., 3.2), tags=["bdt", "skip_shards"]),
            feature("BDT_HT20", "BDT_HT20", default=-1, unit="GeV", binning=(50, 0., 500),
                tags=["bdt", "skip_shards"]),
            feature("BDT_topPairMasses", "BDT_topPairMasses", default=-1, unit="GeV",
                binning=(50, 0., 500.), tags=["bdt", "skip_shards"]),
            feature("BDT_topPairMasses2", "BDT_topPairMasses2", default=-1, unit="GeV",
                binning=(50, 0., 500.), tags=["bdt", "skip_shards"]),
            feature("BDT_MX", "BDT_MX", default=-1, unit="GeV", binning=(50, 0., 1500.),
                tags=["bdt", "skip_shards"]),
            feature("BDT_bH_tauH_MET_InvMass", "BDT_bH_tauH_MET_InvMass", default=-1, unit="GeV",
                binning=(50, 0., 1500.), tags=["bdt", "skip_shards"]),
            feature("BDT_bH_tauH_SVFIT_InvMass", "BDT_bH_tauH_SVFIT_InvMass", default=-1,
                unit="GeV", binning=(50, 0., 1500.), tags=["bdt", "skip_shards"]),
            feature("BDT_bH_tauH_InvMass", "BDT_bH_tauH_InvMass", default=-1, unit="GeV",
                binning=(50, 0., 1500.), tags=["bdt", "skip_shards"]),
            feature("BDT_total_CalcPhi", "BDT_total_CalcPhi", default=-4, binning=(32, 0., 3.2),
                tags=["bdt", "skip_shards"]),
            feature("BDT_ditau_CalcPhi", "BDT_ditau_CalcPhi", default=-4, binning=(32, 0., 3.2),
                tags=["bdt", "skip_shards"]),
            feature("BDT_dib_CalcPhi", "BDT_dib_CalcPhi", default=-4, binning=(32, 0., 3.2),
                tags=["bdt", "skip_shards"]),
            feature("BDT_MET_tauH_SVFIT_cosTheta", "BDT_MET_tauH_SVFIT_cosTheta", default=-2,
                binning=(40, -1., 1.), tags=["bdt", "skip_shards"]),
            feature("BDT_MET_bH_cosTheta", "BDT_MET_bH_cosTheta", default=-2, binning=(40, -1., 1.),
                tags=["bdt", "skip_shards"]),
            feature("BDT_b1_bH_cosTheta", "BDT_b1_bH_cosTheta", default=-2, binning=(40, -1., 1.),
                tags=["bdt", "skip_shards"]),
            feature("BDT_tauH_SVFIT_reson_cosTheta", "BDT_tauH_SVFIT_reson_cosTheta", default=-2,
                binning=(40, -1., 1.), tags=["bdt", "skip_shards"]),

            # other discriminant outputs
            feature("BDToutSM_kl_1", "BDToutSM_kl_1", default=-1, binning=(40, -1., 1.),
                x_title=r"BDT discriminant ($\kappa_{\lambda} = 1$)", tags=["skip_shards"]),
            feature("DNNoutSM_kl_1", "DNNoutSM_kl_1", default=-1, binning=(40, -0., 1.),
                x_title=r"DNN discriminant ($\kappa_{\lambda} = 1$)", tags=["skip_shards"]),

            # custom features
            feature("min_dr_vbfj_tau", fexp.min_delta_r_vbfj_tau, default=-1.,
                binning=(50, 0., 6.), x_title=r"Minimum $\Delta\eta(VBFj, \tau)$",
                tags=["custom", "training", "lbn"]),
            feature("min_dr_vbfj_b", fexp.min_delta_r_vbfj_b, default=-1., binning=(50, 0., 6.),
                x_title=r"Minimum $\Delta\eta(VBFj, b)$", tags=["custom", "training", "lbn"]),
            feature("costheta_star_cs", fexp.abs_costheta_star_cs, default=-2.,
                binning=(50, 0., 1.), x_title=r"$cos(\theta_{CS}^{*})$",
                tags=["custom", "training", "lbn"]),
        ]))

        # features representing raw node outputs
        for proc in config.training.default.processes:
            features.add(feature("dnn_{}".format(proc.name),
                expression="min(0.999,dnn_{})".format(proc.name), default=-1., binning=(20, 0., 1.),
                x_title="DNN output {}".format(proc.label), overflow=True, underflow=True,
                aux={"process": proc}, tags=["skip_shards", "dnn_output"]))

        # add features of merged nodes
        merged_nodes = OrderedDict([
            (pc.process_hh_ggf, [pc.process_hh_ggf]),
            (pc.process_hh_vbf_sm_c2v, [pc.process_hh_vbf, pc.process_hh_vbf_c2v]),
            # uncomment the next and comment the two previous lines to merge all
            # (pc.process_hh, [pc.process_hh_ggf, pc.process_hh_vbf, pc.process_hh_vbf_c2v]),
            (pc.process_tth, [pc.process_tth_bb] + (
                [pc.process_tth_tautau] if config.x.has_tth_tautau else []
            )),
            (pc.process_tt_lep, [pc.process_tt_sl, pc.process_tt_dl]),
            (pc.process_tt_fh, [pc.process_tt_fh]),
            (pc.process_dy, [pc.process_dy]),
        ])
        merged_expressions = {
            mproc: jrs(["dnn_{}".format(proc.name) for proc in procs], op="+", bracket=True)
            for mproc, procs in merged_nodes.items()
        }
        for mproc, procs in merged_nodes.items():
            if mproc in config.training.default.processes:
                continue
            features.add(feature("dnn_{}_merged".format(mproc.name),
                expression="min(0.999,{})".format(merged_expressions[mproc]), default=-1.,
                binning=(20, 0., 1.), x_title="DNN output {}".format(proc.label), overflow=True,
                underflow=True, aux={"process": mproc, "merged_processes": procs, "merged": True},
                tags=["skip_shards", "dnn_output_merged"]))

        # two sets of mpp selections: w/o and w/ node merging
        # without merging
        for proc in config.training.default.processes:
            mpp_sel = jrs([
                "dnn_{} > dnn_{}".format(proc.name, other_proc.name)
                for other_proc in config.training.default.processes
                if other_proc != proc
            ])
            x_min = math.floor(100. / len(config.training.default.processes)) / 100.
            features.add(feature("dnn_{}_mpp".format(proc.name),
                expression="min(0.999,dnn_{})".format(proc.name), selection=mpp_sel, default=-1.,
                binning=(20, 0., 1.), x_title="DNN output {}".format(proc.label), overflow=True,
                underflow=True, aux={"process": proc, "mpp": True, "lower_bound": x_min,
                "upper_bound": 1.0}, tags=["skip_shards", "dnn_output_mpp"]))

        # with merging
        for mproc, procs in merged_nodes.items():
            expr = merged_expressions[mproc]
            mpp_sel = jrs([
                "{} > {}".format(expr, merged_expressions[other_proc])
                for other_proc in merged_nodes
                if other_proc != mproc
            ])
            x_min = math.floor(100. / len(merged_nodes)) / 100.
            features.add(feature("dnn_{}_merged_mpp".format(mproc.name),
                expression="min(0.999,{})".format(expr), selection=mpp_sel, default=-1.,
                binning=(20, 0., 1.), x_title="DNN output {}".format(mproc.label), overflow=True,
                underflow=True,
                aux={"process": mproc, "merged_processes": procs, "merged": True, "mpp": True,
                "lower_bound": x_min, "upper_bound": 1.0},
                tags=["skip_shards", "dnn_output_merged_mpp"]))

    config["features"] = features


def add_helpers(config):
    def get_dataset_process_mapping(datasets, process_group_name):
        mapping = {}
        _processes = []
        _datasets = []
        for process in config.process_groups[process_group_name]:
            for i, dataset in enumerate(datasets):
                _process = dataset.processes.get_first()
                if process == _process or process.has_process(_process, deep=True):
                    mapping[dataset] = process
                    _processes.append((i, process))
                    _datasets.append((i, dataset))

        # sort processes and make them unique
        unique_processes = []
        for _, process in sorted(_processes, key=lambda tpl: tpl[0]):
            if process not in unique_processes:
                unique_processes.append(process)

        # same for datasets
        unique_datasets = []
        for _, dataset in sorted(_datasets, key=lambda tpl: tpl[0]):
            if dataset not in unique_datasets:
                unique_datasets.append(dataset)

        return mapping, unique_processes, unique_datasets

    def get_qcd_regions(region, category):
        # the region must be set and tagged os_iso
        if not region:
            raise Exception("region must not be empty")
        if not region.has_tag("qcd_os_iso"):
            raise Exception("region must be tagged as 'qcd_os_iso' but isn't")

        # the category must be compatible with the estimation technique
        if category.has_tag("qcd_incompatible"):
            raise Exception("category '{}' incompatible with QCD estimation".format(category.name))

        # get other qcd regions
        prefix = region.name[:-len("os_iso")]
        return DotDict({
            key: config.regions.get(prefix + key)
            for key in ["os_inviso", "ss_iso", "ss_inviso"]
        })

    def get_signal_region(qcd_region_name):
        # split the name
        if not isinstance(qcd_region_name, six.string_types):
            qcd_region_name = qcd_region_name.name
        parts = qcd_region_name.rsplit("_", 2)
        if len(parts) not in (2, 3):
            raise Exception("invalid region name format '{}'".format(qcd_region_name))

        # build the prefix (usually the channel name)
        prefix = (parts[0] + "_") if len(parts) == 3 else ""

        # get the signal region
        signal_region = config.regions.get(prefix + "os_iso")
        if not signal_region.has_tag("qcd_os_iso"):
            raise Exception("signal region must be tagged as 'qcd_os_iso' but isn't")

        return signal_region

    def get_dataset_category_weight(dataset, category, evt_den=None, rescale_signal=True):
        weights = []

        # dataset weight
        if dataset.x.weight:
            weights.append(dataset.x.weight)

        # category weight
        if category.x.weight:
            weights.append(category.x.weight)

        # add lumi weight when evt_den is given
        if evt_den:
            lumi_weight = config.lumi / float(evt_den)
            # dataset_process = dataset.processes.get_first()
            # evt_den does not account for the proper xsec for signals (it corresponds to 1pb)
            # note: this has been changed in the latest production version
            # if dataset_process.x("is_signal", False) and rescale_signal:
            #     lumi_weight *= dataset_process.get_xsec(config.ecm)()
            weights.append(lumi_weight)

        return jrs(weights, op="mul")

    def get_opposite_eo_category(category):
        assert(category.x.is_eo)
        if category.x.is_even:
            opposite_name = category.name[:-5] + "_odd"
        else:
            opposite_name = category.name[:-4] + "_even"
        return config.categories.get(opposite_name)

    def get_parent_eo_category(category):
        assert(category.x.is_eo)
        if category.x.is_even:
            parent_name = category.name[:-5]
        else:
            parent_name = category.name[:-4]
        return config.categories.get(parent_name)

    def get_child_eo_category(category, postfix):
        assert(postfix in ["even", "odd"])
        assert(not category.x.is_eo)
        child_name = category.name + "_" + postfix
        return config.categories.get(child_name)

    def get_run_text():
        return "{}, {} TeV ({:.2f} /fb)".format(config.year, law.util.try_int(config.ecm),
            config.lumi / 1000.)

    config["helpers"] = DotDict(
        get_dataset_process_mapping=get_dataset_process_mapping,
        get_qcd_regions=get_qcd_regions,
        get_signal_region=get_signal_region,
        get_dataset_category_weight=get_dataset_category_weight,
        get_opposite_eo_category=get_opposite_eo_category,
        get_parent_eo_category=get_parent_eo_category,
        get_child_eo_category=get_child_eo_category,
        get_run_text=get_run_text,
    )
