# coding: utf-8

"""
Custom feature expressions.
"""

__all__ = []


import math

from hmc.util import feature_expression


EMPTY = -999.


def vbf_centrality(obj):
    # default: -5.
    return ("(({0}_eta == -999 || VBFjet1_eta == -999 || VBFjet2_eta == -999) * -5.) "
        "+ ({0}_eta != -999 && VBFjet1_eta != -999 && VBFjet2_eta != -999) "
        "* (({0}_eta - 0.5 * (VBFjet1_eta + VBFjet2_eta)) / abs(VBFjet1_eta - VBFjet2_eta))"
        .format(obj))


def mass(obj):
    print("this function should not be used as it actually computes Ez")

    # default: -1.
    return ("(({0}_pt == -999 || {0}_eta == -999 || {0}_mass == -999) * -1) "
        "+ (({0}_pt != -999 && {0}_eta != -999 && {0}_mass != -999) "
        "* (((sinh({0}_eta) * {0}_pt)**2 + {0}_mass**2)**0.5))"
        .format(obj))


def energy_z_from_pt_eta_m(obj):
    # default: -1.
    return ("(({0}_pt == -999 || {0}_eta == -999 || {0}_mass == -999) * -1) "
        "+ (({0}_pt != -999 && {0}_eta != -999 && {0}_mass != -999) "
        "* (((sinh({0}_eta) * {0}_pt)**2 + {0}_mass**2)**0.5))"
        .format(obj))


def energy_from_pt_eta_m(obj):
    # default: -1.
    # we used to use this guy:
    # "* (((sinh({0}_eta) * {0}_pt)**2 + {0}_mass**2)**0.5))"
    return ("(({0}_pt == -999 || {0}_eta == -999 || {0}_mass == -999) * -1) "
        "+ (({0}_pt != -999 && {0}_eta != -999 && {0}_mass != -999) "
        "* ((1 + sinh({0}_eta)**2) * {0}_pt**2 + {0}_mass**2)**0.5)"
        .format(obj))


def min_eta(obj1, obj2):
    # default: -5.
    return ("(({0}_eta == -999 || {1}_eta == -999) * -5) "
        "+ (({0}_eta != -999 && {1}_eta != -999) "
        "* (min({0}_eta, {1}_eta)))".format(obj1, obj2))


def abs_delta_eta(obj1, obj2):
    # default: -1.
    return ("(({0}_eta == -999 || {1}_eta == -999) * -1) "
        "+ (({0}_eta != -999 && {1}_eta != -999) "
        "* (abs({0}_eta - {1}_eta)))".format(obj1, obj2))


def _delta_r(entry, obj1, obj2):
    # default: EMPTY
    if any(getattr(entry, "{}_eta".format(o)) == EMPTY for o in [obj1, obj2]):
        return EMPTY

    de = getattr(entry, "{}_eta".format(obj1)) - getattr(entry, "{}_eta".format(obj2))
    dp = abs(getattr(entry, "{}_phi".format(obj1)) - getattr(entry, "{}_phi".format(obj2)))
    if dp > math.pi:
        dp = 2 * math.pi - dp

    return (de**2. + dp**2.)**0.5


@feature_expression(branches=["VBFjet1_eta", "VBFjet1_phi", "VBFjet2_eta", "VBFjet2_phi",
    "bjet1_eta", "bjet1_phi", "bjet2_eta", "bjet2_phi"])
def min_delta_r_vbfj_b(entry):
    # default: -1.
    drs = [
        _delta_r(entry, "VBFjet1", "bjet1"),
        _delta_r(entry, "VBFjet1", "bjet2"),
        _delta_r(entry, "VBFjet2", "bjet1"),
        _delta_r(entry, "VBFjet2", "bjet2"),
    ]
    drs = [dr for dr in drs if dr != EMPTY]
    return min(drs) if drs else -1


@feature_expression(branches=["VBFjet1_eta", "VBFjet1_phi", "VBFjet2_eta", "VBFjet2_phi",
    "dau1_eta", "dau1_phi", "dau2_eta", "dau2_phi"])
def min_delta_r_vbfj_tau(entry):
    # default: -1.
    drs = [
        _delta_r(entry, "VBFjet1", "dau1"),
        _delta_r(entry, "VBFjet1", "dau2"),
        _delta_r(entry, "VBFjet2", "dau1"),
        _delta_r(entry, "VBFjet2", "dau2"),
    ]
    drs = [dr for dr in drs if dr != EMPTY]
    return min(drs) if drs else -1


@feature_expression(branches=["tauH_SVFIT_pt", "tauH_SVFIT_eta", "tauH_SVFIT_phi",
    "tauH_SVFIT_mass", "bH_pt", "bH_eta", "bH_phi", "bH_mass"])
def abs_costheta_star_cs(entry):
    # default: -2.
    if any(getattr(entry, branch) == EMPTY for branch in abs_costheta_star_cs._required_branches):
        return -2.

    import ROOT
    ROOT.PyConfig.IgnoreCommandLineOptions = True
    ROOT.gROOT.SetBatch()

    p1 = ROOT.TLorentzVector()
    p2 = ROOT.TLorentzVector()
    p1.SetPxPyPzE(0, 0, 6500., 6500.)
    p2.SetPxPyPzE(0, 0, -6500., 6500.)

    h1 = ROOT.TLorentzVector()
    h2 = ROOT.TLorentzVector()
    h1.SetPtEtaPhiM(entry.tauH_SVFIT_pt, entry.tauH_SVFIT_eta, entry.tauH_SVFIT_phi,
        entry.tauH_SVFIT_mass)
    h2.SetPtEtaPhiM(entry.bH_pt, entry.bH_eta, entry.bH_phi, entry.bH_mass)
    hh = h1 + h2

    if hh.E() < hh.P():
        # no boost possible
        return -2.

    boost = -hh.BoostVector()
    p1.Boost(boost)
    p2.Boost(boost)
    h1.Boost(boost)

    cs_axis = p1.Vect().Unit() - p2.Vect().Unit()
    cs_axis = cs_axis.Unit()

    return abs(math.cos(cs_axis.Angle(h1.Vect().Unit())))
