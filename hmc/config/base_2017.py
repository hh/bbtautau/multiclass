# coding: utf-8

"""
Basic configuration for 2017 data based on ntuples provided by LLR.
"""

__all__ = ["config"]


import os
from collections import OrderedDict

import hmc.config.common as cm
from hmc.util import DotDict
import hmc.config.processes as pc


# miscellaneous
data_eras = ["B", "C", "D", "E", "F"]

skim_directory = "/eos/home-d/dzuolo/SKIMMED_Legacy2017_31Aug2020_HHbtag"
sd = lambda names: [os.path.join(skim_directory, name) for name in names]
skim_directory_c2v0 = "/eos/user/f/fbrivio/Hhh_1718/SKIMS_July2020_VBF_C2V_0"
sd_c2v0 = lambda names: [os.path.join(skim_directory_c2v0, name) for name in names]

plot_weights = DotDict(
    mutau="MC_weight * PUReweight * PUjetID_SF * L1pref_weight * prescaleWeight * trigSF * "
          "IdAndIsoAndFakeSF_deep_pt * DYscale_MTT",
    etau="MC_weight * PUReweight * PUjetID_SF * L1pref_weight * prescaleWeight * trigSF * "
         "IdAndIsoAndFakeSF_deep_pt * DYscale_MTT",
    tautau="MC_weight * PUReweight * PUjetID_SF * L1pref_weight * prescaleWeight * trigSF * "
           "IdAndIsoAndFakeSF_deep_pt * DYscale_MTT",
)

# create the config
config = cm.create_config(
    name="base_2017",
    year=2017,
    ecm=13.,
    lumi=41557,  # 1/pb
    plot_weights=plot_weights,
    btag=DotDict(medium=0.3033, loose=0.0521),
)

# dataset specifications
dataset_specs = OrderedDict()
dataset_specs["hh_ggf"] = dict(
    id=0,
    process=pc.process_hh_ggf,
    dirnames=sd(["SKIM_GGHH_NLO_cHHH1_xs"]),
    tags=["ggf"],
)
dataset_specs["hh_ggf_0"] = dict(
    id=1,
    process=pc.process_hh_ggf_0,
    dirnames=sd(["SKIM_GGHH_NLO_cHHH0_xs"]),
    tags=["ggf", "ggf_bsm", "bsm"],
)
dataset_specs["hh_ggf_2p45"] = dict(
    id=2,
    process=pc.process_hh_ggf_2p45,
    dirnames=sd(["SKIM_GGHH_NLO_cHHH2p45_xs"]),
    tags=["ggf", "ggf_bsm", "bsm"],
)
dataset_specs["hh_ggf_5"] = dict(
    id=3,
    process=pc.process_hh_ggf_5,
    dirnames=sd(["SKIM_GGHH_NLO_cHHH5_xs"]),
    tags=["ggf", "ggf_bsm", "bsm"],
)
dataset_specs["hh_vbf"] = dict(
    id=10,
    process=pc.process_hh_vbf,
    dirnames=sd(["SKIM_VBFHH_CV_1_C2V_1_C3_1_xs"]),
    tags=["vbf"],
)
dataset_specs["hh_vbf_0p5_1_1"] = dict(
    id=11,
    process=pc.process_hh_vbf_0p5_1_1,
    dirnames=sd_c2v0(["SKIM2017_VBFHHTo2B2Tau_CV_0p5_C2V_1_C3_1_xs"]),
    tags=["vbf", "vbf_bsm", "bsm"],
)
dataset_specs["hh_vbf_1p5_1_1"] = dict(
    id=12,
    process=pc.process_hh_vbf_1p5_1_1,
    dirnames=sd(["SKIM_VBFHH_CV_1p5_C2V_1_C3_1_xs"]),
    tags=["vbf", "vbf_bsm", "bsm"],
)
dataset_specs["hh_vbf_1_0_1"] = dict(
    id=13,
    process=pc.process_hh_vbf_1_0_1,
    dirnames=sd_c2v0(["SKIM2017_VBFHHTo2B2Tau_CV_1_C2V_0_C3_1"]),
    tags=["vbf", "vbf_bsm", "bsm"],
)
dataset_specs["hh_vbf_1_2_1"] = dict(
    id=14,
    process=pc.process_hh_vbf_1_2_1,
    dirnames=sd(["SKIM_VBFHH_CV_1_C2V_2_C3_1_xs"]),
    tags=["vbf", "vbf_bsm", "bsm"],
)
dataset_specs["hh_vbf_1_1_0"] = dict(
    id=15,
    process=pc.process_hh_vbf_1_1_0,
    dirnames=sd(["SKIM_VBFHH_CV_1_C2V_1_C3_0_xs"]),
    tags=["vbf", "vbf_bsm", "bsm"],
)
dataset_specs["hh_vbf_1_1_2"] = dict(
    id=16,
    process=pc.process_hh_vbf_1_1_2,
    dirnames=sd(["SKIM_VBFHH_CV_1_C2V_1_C3_2_xs"]),
    tags=["vbf", "vbf_bsm", "bsm"],
)
dataset_specs["hh_vbf_c2v"] = dict(
    id=18,
    process=pc.process_hh_vbf_c2v,
    dirnames=sd_c2v0(["SKIM2017_VBFHHTo2B2Tau_CV_1_C2V_0_C3_1"])
    + sd(["SKIM_VBFHH_CV_1_C2V_2_C3_1_xs"]),
    tags=["vbf"],
)
dataset_specs["tth_bb"] = dict(
    id=21,
    process=pc.process_tth_bb,
    dirnames=sd(["SKIM_ttHJetToBB"]),
    merging={
    },
)
dataset_specs["tth_nonbb"] = dict(
    id=22,
    process=pc.process_tth_nonbb,
    dirnames=sd(["SKIM_ttHJetTononBB"]),
    merging={
    },
    tags=["skip_shards"],
)
dataset_specs["tth_tautau"] = dict(
    id=23,
    process=pc.process_tth_tautau,
    dirnames=sd(["SKIM_ttHToTauTau"]),
    merging={
        "vbf_loose_even": 3, "vbf_loose_odd": 3,
        "vbf_loose_os_even": 2, "vbf_loose_os_odd": 2,
        "vbf_common_even": 4, "vbf_common_odd": 4,
        "vbf_common_os_even": 3, "vbf_common_os_odd": 3,
        "vr_even": 6, "vr_odd": 6,
        "vr_os_even": 4, "vr_os_odd": 4,
        "vbf_even": 3, "vbf_odd": 3,
        "vbf_os_even": 2, "vbf_os_odd": 2,
    },
)
dataset_specs["tt_dl"] = dict(
    id=31,
    process=pc.process_tt_dl,
    dirnames=sd(["SKIM_TT_fullyLep"]),
    merging={
        "vbf_loose_even": 2, "vbf_loose_odd": 2,
        "vbf_loose_os_even": 2, "vbf_loose_os_odd": 2,
        "vbf_common_even": 3, "vbf_common_odd": 3,
        "vbf_common_os_even": 2, "vbf_common_os_odd": 2,
        "vr_even": 5, "vr_odd": 5,
        "vr_os_even": 4, "vr_os_odd": 4,
        "vbf_even": 2, "vbf_odd": 2,
        "vbf_os_even": 2, "vbf_os_odd": 2,

    },
    weight="TTtopPtreweight",
)
dataset_specs["tt_sl"] = dict(
    id=32,
    process=pc.process_tt_sl,
    dirnames=sd(["SKIM_TT_semiLep"]),
    merging={
        "vbf_loose_even": 3, "vbf_loose_odd": 3,
        "vbf_loose_os_even": 2, "vbf_loose_os_odd": 2,
        "vbf_common_even": 4, "vbf_common_odd": 4,
        "vbf_common_os_even": 3, "vbf_common_os_odd": 3,
        "vr_even": 8, "vr_odd": 8,
        "vr_os_even": 5, "vr_os_odd": 5,
        "vbf_even": 3, "vbf_odd": 3,
        "vbf_os_even": 2, "vbf_os_odd": 2,
    },
    splitting={
        "vr_even": 0.78, "vr_odd": 0.78,
    },
    weight="TTtopPtreweight",
)
dataset_specs["tt_fh"] = dict(
    id=33,
    process=pc.process_tt_fh,
    dirnames=sd(["SKIM_TT_fullyHad"]),
    weight="TTtopPtreweight",
)
dataset_specs["dy_low"] = dict(
    id=41,
    process=pc.process_dy_ll_m5to50,
    dirnames=sd(["SKIM_DYJets_M_10_50_PU_Safe"]),
)
dataset_specs["dy_high"] = dict(
    id=42,
    process=pc.process_dy_ll_m50,
    dirnames=sd(["SKIM_DY"]),
    merging={
        "vbf_common_even": 2, "vbf_common_odd": 2,
        "vbf_common_os_even": 2, "vbf_common_os_odd": 2,
    },
)
dataset_specs["data_mutau"] = dict(
    id=1001,
    is_data=True,
    process=pc.process_data_mutau,
    dirnames=sd(["SKIM_Mu_2017{}".format(era) for era in data_eras]),
    selection=cm.channels.n.mutau.x.selection,
    merging={
        "vbf_common_even": 2, "vbf_common_odd": 2,
        "vr_even": 2, "vr_odd": 2,
    },
    tags=["data", "skip_shards"],
)
dataset_specs["data_etau"] = dict(
    id=1002,
    is_data=True,
    process=pc.process_data_etau,
    dirnames=sd(["SKIM_Ele_2017{}".format(era) for era in data_eras]),
    selection=cm.channels.n.etau.x.selection,
    merging={
    },
    tags=["data", "skip_shards"],
)
dataset_specs["data_tautau"] = dict(
    id=1003,
    is_data=True,
    process=pc.process_data_tautau,
    dirnames=sd(["SKIM_Tau_2017{}".format(era) for era in data_eras]),
    selection=cm.channels.n.tautau.x.selection,
    merging={
    },
    tags=["data", "skip_shards"],
)

# add additional datasets
other_backgrounds = [
    # singletop
    "ST_tW_top", "ST_tW_antitop", "ST_tchannel_top", "ST_tchannel_antitop",
    # electroweak
    "EWKWMinus2Jets_WToLNu", "EWKWPlus2Jets_WToLNu", "EWKZ2Jets_ZToLL",
    # W + jets
    "WJets_HT_0_70", "WJets_HT_100_200", "WJets_HT_200_400", "WJets_HT_400_600", "WJets_HT_600_800",
    "WJets_HT_70_100", "WJets_HT_800_1200", "WJets_HT_1200_2500", "WJets_HT_2500_Inf",
    # diboson
    "WWTo2L2Nu", "WWTo4Q", "WWToLNuQQ",
    "WZTo1L1Nu2Q", "WZTo1L3Nu", "WZTo2L2Q", "WZTo3LNu",
    "ZZTo2L2Nu", "ZZTo2L2Q", "ZZTo2Q2Nu", "ZZTo4L",
    # triboson
    "WWW", "WWZ", "WZZ", "ZZZ",
    # ttbar + boson
    "TTWJetsToLNu", "TTWJetsToQQ",
    "TTZToLLNuNu", "TTZToQQ",
    # ttbar + diboson
    "TTWW", "TTWZ", "TTZZ",
    # processes with single Higgs bosons
    "ggHTauTau", "VBFHTauTau",
    "WminusHTauTau", "WplusHTauTau",
    "ZH_HBB_ZLL", "ZH_HBB_ZQQ", "ZH_HTauTau",
    "TTWH", "TTZH",
]
for i, name in enumerate(other_backgrounds):
    dataset_specs["other_" + name] = dict(
        id=51 + i,
        process=pc.process_others,
        dirnames=sd(["SKIM_" + name]),
        tags=["skip_shards", "other_dataset", "other"],
    )

# add common objects
cm.add_selections(config)
cm.add_categories(config)
cm.add_regions(config)
cm.add_datasets(config, dataset_specs)
cm.add_training_config(config)
cm.add_features(config)
cm.add_helpers(config)


# list of currently used versions per task, used by Task.vreq
config["versions"] = {
    "Categorization": "prod3",
    "MergeCategorization": "prod3",
    "MergeCategorizationStats": "prod3",
    "CreateShards": "prod6",
    "MergeShards": "prod6",
}
