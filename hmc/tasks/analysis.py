# coding: utf-8

"""
Analysis and statistical inference tasks.
"""

__all__ = []


from collections import OrderedDict

import law
import luigi

from hmc.tasks.base import ConfigTaskWithCategory, DatasetWrapperTask
from hmc.tasks.training import MultiSeedTrainingTask
from hmc.tasks.plotting import TrainingOutputPlotBase, TrainingOutputPlot, CrossTrainingOutputPlot
from hmc.util import import_root, hist_to_array, optimize_binning


class BinningOptimizationBase(ConfigTaskWithCategory, DatasetWrapperTask):

    feature_tags = law.CSVParameter(default=("dnn_output_mpp", "dnn_output_merged_mpp"),
        description="list of tags of dnn output features to plot, default: "
        "(dnn_output_mpp,dnn_output_merged_mpp)")
    view_command = luigi.ChoiceParameter(default=law.NO_STR, choices=(law.NO_STR, "imgcat"),
        significant=False, description="command to use for visualizing histograms live during "
        "optimization, not used when empty, default: empty")
    interactive = luigi.BoolParameter(default=False, significant=False, description="when True, "
        "wait for input after each visualization, default: False")
    n_start_bins = luigi.IntParameter(default=10, description="the number of bins to start with, "
        "default: 10")
    auto_range = luigi.BoolParameter(default=False, significant=False, description="when True, "
        "enforce automatic central range finding independent of how features are configured, "
        "default: False")
    reuse_existing = luigi.BoolParameter(default=False, significant=False, description="reuse "
        "existing binning rules when targets exist, default: False")
    process_group_name = TrainingOutputPlotBase.process_group_name

    n_mini_bins = 10000
    n_min_bins = 1
    y_low = 8.
    y_high = y_low

    allow_composite_category = True

    def output(self):
        postfix = "__n{}".format(self.n_start_bins)
        if self.region:
            postfix += "__" + self.region.name

        return law.SiblingFileCollection({
            feature.name: self.local_target("binning__{}{}.json".format(feature.name, postfix))
            for feature in self.requires().features
        })

    @law.decorator.notify
    def run(self):
        import numpy as np
        from scinum import Number
        from hmc.config.processes import process_qcd
        ROOT = import_root()
        import plotlib.root as r

        # get all processes
        processes = self.config.helpers.get_dataset_process_mapping(self.datasets,
            self.process_group_name)[1]
        processes.append(process_qcd)

        # prepare outputs
        outputs = self.output()

        # do the optimization per feature
        targets = self.input()["root"].targets.items()
        progress_cb = self.create_progress_callback(len(targets))
        for i, (feature_name, target) in enumerate(targets):
            feature = self.config.features.get(feature_name)
            outp = outputs.targets[feature_name]
            if self.reuse_existing and outp.exists():
                self.publish_message("binning for feature {} already existing, skip".format(
                    feature_name))
                progress_cb(i)
                continue

            # read histograms and convert them to numpy arrays, including errors
            full_edges = None
            full_data = OrderedDict()
            with target.load(formatter="root") as tfile:
                tdir = tfile.Get("histograms")
                for tkey in tdir.GetListOfKeys():
                    thist = tdir.Get(tkey.GetName())
                    if not isinstance(tdir.Get(tkey.GetName()), ROOT.TH1F):
                        continue
                    # move under/overflow bins before converting
                    r.show_hist_underflow(thist, clear=True)
                    r.show_hist_overflow(thist, clear=True)
                    # convert
                    full_edges, contents, errors = hist_to_array(thist, errors=True, asymm=False,
                        overflow=False, underflow=False, edges=True)
                    full_data[tkey.GetName()] = Number(contents, {tkey.GetName(): errors})

            # separate into signal and background
            s = Number(np.zeros((self.n_mini_bins,), dtype=np.float32))
            b = Number(np.zeros((self.n_mini_bins,), dtype=np.float32))
            for process_name, d in full_data.items():
                if process_name == "data":
                    continue
                for process in processes:
                    if process.name == process_name or process.has_process(process_name):
                        if process.is_data:
                            continue
                        n = s if process.x("is_signal", False) else b
                        n.add(d, rho=0., inplace=True)
                        break
                else:
                    raise Exception("could not find process {}".format(process_name))
            # extract values after combining errors
            s_vals, s_errs = s(), s("up", diff=True)
            b_vals, b_errs = b(), b("up", diff=True)

            # show the initial histogram with the fill amount of bins
            def visualize(opt_edges, interactive=None):
                if interactive is None:
                    interactive = self.interactive
                self.visualize_histogram(feature, s_vals, b_vals, full_edges, opt_edges,
                    interactive=interactive)
            visualize(full_edges, interactive=False)

            # start with the actual optimization algorithm
            n_start_bins = feature.x("bin_opt_start_bins", self.n_start_bins)
            n_min_bins = feature.x("bin_opt_n_min_bins", self.n_min_bins)
            y_low = float(feature.x("bin_opt_y_low", self.y_low))
            y_high = float(feature.x("bin_opt_y_high", self.y_high))
            x_min = float(feature.x("lower_bound", feature.binning[1]))
            x_max = float(feature.x("upper_bound", feature.binning[2]))
            if self.auto_range:
                x_min, x_max = "auto", "auto"
            with self.publish_step("optimizing feature '{}' ...".format(feature_name)):
                print x_min, x_max
                opt_edges = optimize_binning(full_edges, s_vals, b_vals, s_errs, b_errs,
                    n_start_bins, n_min_bins, y_low, y_high, x_min=x_min, x_max=x_max,
                    callback=visualize)

            # store the optimized edges
            outp.dump(np.array(opt_edges).tolist(), indent=4, formatter="json")

            progress_cb(i)

    def visualize_histogram(self, feature, s_vals, b_vals, full_edges, opt_edges, interactive=True):
        if self.view_command == law.NO_STR:
            return

        # create a temporary plot
        from hmc.plots import bin_opt_plot
        tmp = law.LocalFileTarget(is_tmp=".pdf")
        bin_opt_plot(tmp.path, feature.name, s_vals, b_vals, full_edges, opt_edges,
            x_title=feature.get_full_x_title(root=True),
            y_title=feature.get_full_y_title(root=True, bin_width=False),
            top_right_text=self.config.helpers.get_run_text())

        # build the command to visualize it
        if self.view_command == "imgcat":
            cmd = "imgcat {}".format(tmp.path)
        else:
            raise NotImplementedError

        # run it
        code = law.util.interruptable_popen(cmd, shell=True, executable="/bin/bash")[0]
        if code != 0:
            raise Exception("visualization command failed: {}".format(cmd))

        # when for input when interactive
        if interactive:
            raw_input("Press any key to continue: ")


class BinningOptimization(BinningOptimizationBase, MultiSeedTrainingTask):

    def requires(self):
        return TrainingOutputPlot.req(self, stack=True, n_bins=self.n_mini_bins, do_qcd=True,
            save_root=True, bin_opt_version=law.NO_STR, _prefer_cli=["version"])


class CrossBinningOptimization(BinningOptimizationBase):

    training_id_even = CrossTrainingOutputPlot.training_id_even
    training_id_odd = CrossTrainingOutputPlot.training_id_odd

    def requires(self):
        return CrossTrainingOutputPlot.req(self, stack=True, n_bins=self.n_mini_bins, do_qcd=True,
            save_root=True, bin_opt_version=law.NO_STR, version="prod3", _prefer_cli=["version"])
