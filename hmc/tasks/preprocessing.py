# coding: utf-8

"""
Preprocessing tasks.
"""

__all__ = []


import abc
import contextlib
import itertools
from collections import OrderedDict, defaultdict

import law
import luigi
from six.moves import zip

from hmc.tasks.base import DatasetTaskWithCategory, DatasetWrapperTask, HTCondorWorkflow, InputData
from hmc.util import iter_tree


split_names = ["train", "valid"]


class DatasetCategoryWrapperTask(DatasetWrapperTask, law.WrapperTask):

    category_names = law.CSVParameter(default=("baseline_even",), description="names of categories "
        "to run, default: (baseline_even,)")

    exclude_index = True

    def __init__(self, *args, **kwargs):
        super(DatasetCategoryWrapperTask, self).__init__(*args, **kwargs)

        # tasks wrapped by this class do not allow composite categories, so split them here
        self.categories = []
        for name in self.category_names:
            category = self.config.categories.get(name)
            if category.x("composite", False):
                self.categories.extend(category.get_leaf_categories())
            else:
                self.categories.append(category)

    @abc.abstractmethod
    def atomic_requires(self, dataset, category):
        return None

    def requires(self):
        return OrderedDict(
            ((dataset.name, category.name), self.atomic_requires(dataset, category))
            for dataset, category in itertools.product(self.datasets, self.categories)
        )


class Categorization(DatasetTaskWithCategory, law.LocalWorkflow, HTCondorWorkflow):

    # regions not supported
    region_name = None

    tree_name = "HTauTauTree"

    default_store = "$HMC_STORE_EOS_CATEGORIZATION"
    default_wlcg_fs = "wlcg_fs_categorization"

    def create_branch_map(self):
        return len(self.dataset.keys)

    def workflow_requires(self):
        return {"data": InputData.req(self)}

    def requires(self):
        return InputData.req(self, file_index=self.branch)

    def output(self):
        return {
            "data": self.dynamic_target("data_{}.root".format(self.branch)),
            "stats": self.dynamic_target("stats_{}.json".format(self.branch)),
        }

    @law.decorator.notify
    @law.decorator.localize(input=False)
    def run(self):
        from order.util import join_root_selection as jrs

        # prepare outputs
        outputs = self.output()
        outputs["data"].parent.touch()

        # helper to write stats
        def write_stats(n_total, n_sel, output_size, evt_den, broken=False):
            stats = dict(
                counts=dict(n_total=n_total, n_selected=n_sel),
                output_size=output_size,
                evt_den=evt_den,
                input_broken=broken,
            )
            outputs["stats"].dump(stats, indent=4, formatter="json")

        # read the input file
        inp = self.input()
        in_file = inp.load(formatter="root")
        in_tree = in_file.Get(self.tree_name)
        histo = in_file.Get("h_eff")

        # when the file is broken write empty outputs
        broken = not bool(in_tree)
        if broken:
            self.publish_message(law.util.colored("input file {} of branch {} is broken".format(
                inp.path, self.branch), "red"))
            with outputs["data"].dump("RECREATE", formatter="root") as out_file:
                out_file.cd()
            write_stats(0, 0, 0, 0., True)
            return

        # read file stats
        n_total = in_tree.GetEntries()
        self.publish_message("input file size: {}".format(law.util.human_bytes(
            inp.stat.st_size, fmt=True)))
        self.publish_message("found {} events ".format(n_total))

        # read the event weight denominator
        evt_den = histo.GetBinContent(1)

        # build the full selection
        selection = self.category.selection
        if self.dataset.x.selection and self.dataset.x.selection != "1":
            selection = jrs(self.dataset.x.selection, selection, op="and")

        # open the output root file for wriring
        with outputs["data"].dump("RECREATE", formatter="root") as out_file:
            out_file.cd()

            # create the new tree based on the category selection
            with self.publish_step("creating tree with category selection ...", runtime=True):
                out_tree = in_tree.CopyTree(selection)
                out_tree.Write()

            n_sel = out_tree.GetEntries()
            eff = (100. * n_sel / n_total) if n_total else 0.

        self.publish_message("selected {} events".format(n_sel))
        self.publish_message("efficiency {:.1f} %".format(eff))
        output_size = outputs["data"].stat.st_size
        self.publish_message("output file size: {}".format(law.util.human_bytes(
            output_size, fmt=True)))

        # write statistics
        write_stats(n_total, n_sel, output_size, evt_den)


class CategorizationWrapper(DatasetCategoryWrapperTask):

    def atomic_requires(self, dataset, category):
        return Categorization.req(self, dataset_name=dataset.name, category_name=category.name)


class MergeCategorizationStats(DatasetTaskWithCategory, law.tasks.ForestMerge):

    # regions not supported
    region_name = None

    merge_factor = 16

    default_store = "$HMC_STORE_EOS_CATEGORIZATION"
    default_wlcg_fs = "wlcg_fs_categorization"

    def merge_workflow_requires(self):
        return Categorization.req(self, _prefer_cli=["workflow"])

    def merge_requires(self, start_leaf, end_leaf):
        return Categorization.req(self, branch=-1, workflow="local", start_branch=start_leaf,
            end_branch=end_leaf)

    def trace_merge_inputs(self, inputs):
        return [inp["stats"] for inp in inputs["collection"].targets.values()]

    def merge_output(self):
        return self.local_target("stats.json")

    def merge(self, inputs, output):
        # output content
        stats = dict(counts=defaultdict(int), output_sizes=[], evt_den=0., n_broken=0)

        # merge
        for inp in inputs:
            try:
                _stats = inp.load(formatter="json")
            except:
                print("error leading input target {}".format(inp))
                raise

            # add counts
            for key, count in _stats["counts"].items():
                stats["counts"][key] += count

            # append to or extend output sizes
            if self.is_leaf():
                stats["output_sizes"].append(_stats["output_size"])
            else:
                stats["output_sizes"].extend(_stats["output_sizes"])

            # add evt_den
            stats["evt_den"] += _stats["evt_den"]

            # increment broken counter
            if self.is_leaf():
                stats["n_broken"] += int(_stats.get("input_broken", False))
            else:
                stats["n_broken"] += _stats["n_broken"]

        output.parent.touch()
        output.dump(stats, indent=4, formatter="json")


class MergeCategorization(DatasetTaskWithCategory, law.tasks.ForestMerge):

    # regions not supported
    region_name = None

    merge_factor = 10

    default_store = "$HMC_STORE_EOS_CATEGORIZATION"
    default_wlcg_fs = "wlcg_fs_categorization"

    def merge_workflow_requires(self):
        return Categorization.req(self, _prefer_cli=["workflow"])

    def merge_requires(self, start_leaf, end_leaf):
        # the requirement is a workflow, so start_leaf and end_leaf correspond to branches
        return Categorization.req(self, branch=-1, workflow="local", start_branch=start_leaf,
            end_branch=end_leaf)

    def trace_merge_inputs(self, inputs):
        return [inp["data"] for inp in inputs["collection"].targets.values()]

    def merge_output(self):
        return law.SiblingFileCollection([
            self.local_target("data_{}.root".format(i))
            for i in range(self.n_files_after_merging)
        ])

    def merge(self, inputs, output):
        with output.localize("w") as tmp_out:
            law.root.hadd_task(self, inputs, tmp_out, local=True)


class MergeCategorizationWrapper(DatasetCategoryWrapperTask):

    def atomic_requires(self, dataset, category):
        return MergeCategorization.req(self, dataset_name=dataset.name, category_name=category.name)


class CreateShards(DatasetTaskWithCategory, law.LocalWorkflow, HTCondorWorkflow):

    # regions not supported
    region_name = None

    tree_name = Categorization.tree_name

    default_store = "$HMC_STORE_EOS_SHARDS"
    default_wlcg_fs = "wlcg_fs_shards"

    @property
    def priority(self):
        # give bsm signal a lower task priority
        if self.process.x("is_signal", False) and self.process.x("is_bsm", False):
            return -10
        else:
            return 0

    def create_branch_map(self):
        return len(self.dataset.keys)

    def workflow_requires(self):
        return {"data": InputData.req(self)}

    def requires(self):
        return InputData.req(self, file_index=self.branch)

    def output(self):
        outputs = OrderedDict(
            (split, self.dynamic_target("data_{}_{}.pb".format(split, self.branch)))
            for split in split_names
        )
        outputs["stats"] = self.dynamic_target("stats_{}.json".format(self.branch))
        return outputs

    @law.decorator.notify
    @law.decorator.localize
    def run(self):
        import numpy as np
        import tensorflow as tf
        import tabulate
        from order.util import join_root_selection as jrs

        np.random.seed(self.category.id + 100 * self.branch)

        # get the splitting fractions
        splitting = self.dataset.x.splitting.get(self.category_name,
            self.dataset.x.default_splitting)
        splitting = (splitting, 1. - splitting)

        # list of features to consider
        features = [
            feature for feature in self.config.features
            if not feature.has_tag("skip_shards")
        ]
        self.publish_message("found {} features to convert".format(len(features)))

        # helpers to create a tf feature
        def tf_feature(value):
            return tf.train.Feature(float_list=tf.train.FloatList(value=[value]))

        # helpers to create a tf example
        def tf_example(values):
            tf_values = {f.name: tf_feature(v) for f, v in zip(features, values)}
            example = tf.train.Example(features=tf.train.Features(feature=tf_values))
            return example

        # prepare outputs
        outputs = self.output()
        writers = [tf.io.TFRecordWriter(outputs[split].path) for split in split_names]

        # prepare input
        inp = self.input()
        self.publish_message("input file size: {}".format(law.util.human_bytes(
            inp.stat.st_size, fmt=True)))
        tfile = inp.load(formatter="root")
        tree = tfile.Get(self.tree_name)

        # when the input file is broken, create empty outputs
        broken = not bool(tree)
        if broken:
            with contextlib.nested(*writers):
                pass
            stats = {"counts": {name: 0 for name in split_names}}
            outputs["stats"].dump(stats, indent=4, formatter="json")
            return

        # get the total events
        n_total = tree.GetEntries()
        self.publish_message("found {} events ".format(n_total))

        # build the full selection
        selection = self.category.selection
        if self.dataset.x.selection and self.dataset.x.selection != "1":
            selection = jrs(self.dataset.x.selection, selection, op="and")

        # iterate through the tree
        expressions = [feature.expression for feature in features]
        defaults = [feature.x.default for feature in features]
        missing_values = [feature.x.missing for feature in features]
        counts = [0] * len(split_names)
        progress = self.create_progress_callback(n_total)
        with self.publish_step("converting ...", runtime=True):
            with contextlib.nested(*writers):
                n_converted = 0
                for idx, values in iter_tree(tree, expressions, defaults, selection=selection,
                        missing_values=missing_values, yield_index=True):
                    if not np.isfinite(values).all():
                        raise Exception("not all values are finite in entry {}: {}".format(
                            idx, values))
                    example = tf_example(values)

                    # add the process id
                    example.features.feature["process_id"].float_list.value.append(self.process.id)

                    # decide which writer to use and track counts
                    r = np.random.choice(len(splitting), p=splitting)
                    writers[r].write(example.SerializeToString())
                    counts[r] += 1

                    n_converted += 1
                    progress(idx)

                tfile.Close()
                self.publish_message("converted {} events".format(n_converted))

        # save stats
        stats = {"counts": dict(zip(split_names, counts))}
        outputs["stats"].dump(stats, indent=4, formatter="json")

        # print some helpful quantities
        headers = ["Split", "Count", "Fraction / %", "File size / MB"]
        rows = []
        sum_counts = sum(counts)
        for split_name, count in zip(split_names, counts):
            rows.append([
                split_name,
                count,
                (100. * count / sum_counts) if sum_counts else "-",
                law.util.parse_bytes(outputs[split_name].stat.st_size, unit="MB"),
            ])
        table = tabulate.tabulate(rows, headers=headers, floatfmt=".2f", tablefmt="grid")
        self.publish_message(table)


class CreateShardsWrapper(DatasetCategoryWrapperTask):

    def atomic_requires(self, dataset, category):
        tasks_per_job = 2
        if dataset.name == "dy":
            tasks_per_job = 5
        elif dataset.name in ("tt_sl", "tt_dl", "tth_tautau"):
            tasks_per_job = 1

        return CreateShards.req(self, dataset_name=dataset.name, category_name=category.name,
            tasks_per_job=tasks_per_job, _prefer_cli=["tasks_per_job"])

    def get_default_dataset_names(self):
        return [
            dataset.name for dataset in self.config.datasets
            if not dataset.has_tag("skip_shards")
        ]


class MergeShards(DatasetTaskWithCategory, law.tasks.ForestMerge):

    split = luigi.ChoiceParameter(default="train", choices=split_names, description="the dataset "
        "split to merge, default: train")

    # regions not supported
    region_name = None

    merge_factor = 20

    default_store = "$HMC_STORE_EOS_SHARDS"
    default_wlcg_fs = "wlcg_fs_shards"

    def merge_workflow_requires(self):
        return CreateShards.req(self, _prefer_cli=["workflow"])

    def merge_requires(self, start_leaf, end_leaf):
        return CreateShards.req(self, branch=-1, workflow="local", start_branch=start_leaf,
            end_branch=end_leaf)

    def trace_merge_inputs(self, inputs):
        return [inp[self.split] for inp in inputs["collection"].targets.values()]

    def merge_output(self):
        return law.SiblingFileCollection([
            self.local_target("data_{}_{}.pb".format(self.split, i))
            for i in range(self.n_files_after_merging)
        ])

    def merge(self, inputs, output):
        with output.localize("w") as tmp_out:
            # pb files containing tf examples are headless and can be cat'ed
            paths = [inp.path for inp in inputs]
            cmd = "cat {} > {}".format(" ".join(paths), tmp_out.path)

            # run the command
            code = law.util.interruptable_popen(cmd, shell=True, executable="/bin/bash")[0]
            if code != 0:
                raise Exception("pb file merging via cat failed")

            self.publish_message("output file size: {}".format(law.util.human_bytes(
                tmp_out.stat.st_size, fmt=True)))


class MergeShardsWrapper(DatasetCategoryWrapperTask):

    def atomic_requires(self, dataset, category):
        return {
            split: MergeShards.req(self, dataset_name=dataset.name, category_name=category.name,
                split=split)
            for split in split_names
        }

    def get_default_dataset_names(self):
        return [
            dataset.name for dataset in self.config.datasets
            if not dataset.has_tag("skip_shards")
        ]
