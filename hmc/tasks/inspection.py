# coding: utf-8

"""
Data and config inspection tasks.
"""

__all__ = []


import math
import itertools
from collections import OrderedDict

import law
import luigi

from hmc.tasks.base import ConfigTaskWithCategory, DatasetTask, DatasetWrapperTask, InputData
from hmc.tasks.preprocessing import DatasetCategoryWrapperTask, MergeCategorizationStats
from hmc.tasks.training import CrossTrainingWrapper
from hmc.tasks.plotting import CrossTrainingOutputPlot
from hmc.util import import_root, get_expression_infos
import hmc.config.processes as pc


class ExpressionCheck(DatasetTask, law.tasks.RunOnceTask):

    version = None
    tree_name = "HTauTauTree"

    def requires(self):
        return InputData.req(self, file_index=0)

    def run(self):
        ROOT = import_root()
        from order.util import join_root_selection as jrs

        # get expressions of all features, categories, regions, weights, etc.
        expressions = []
        for feature in self.config.features:
            if feature.has_tag("dnn_output*"):
                continue
            expressions.append(("fe:{}".format(feature.name), feature.expression))
        for dataset in self.config.datasets:
            if dataset.x.selection:
                expressions.append(("ds:{}".format(dataset.name), jrs(dataset.x.selection)))
            if dataset.x.weight:
                expressions.append(("dw:{}".format(dataset.name), jrs(dataset.x.weight)))
        for category in self.config.categories:
            if category.selection:
                expressions.append(("cs:{}".format(category.name), jrs(category.selection)))
            if category.x.weight:
                expressions.append(("cw:{}".format(category.name), jrs(category.x.weight)))
        for region in self.config.regions:
            if region.selection:
                expressions.append(("cs:{}".format(region.name), jrs(region.selection)))
            if region.x.weight:
                expressions.append(("cw:{}".format(region.name), jrs(region.x.weight)))
        self.publish_message("checking {} expressions".format(len(expressions)))

        # read the input file
        in_file = self.input().load(formatter="root")
        in_tree = in_file.Get(self.tree_name)

        # check all expressions
        failed_origins = []
        for i, (origin, expression) in enumerate(expressions):
            expression = get_expression_infos(expression, delimiter="&&")[1]
            f = ROOT.TTreeFormula("expression_{}".format(i), expression, in_tree)
            if not f.GetNdim():
                self.publish_message("expression '{}' failed".format(expression))
                failed_origins.append(origin)

        if failed_origins:
            raise Exception("{} expressions failed:\n    {}".format(
                len(failed_origins), "\n    ".join(failed_origins)))

        self.mark_complete()


class ExpressionCheckWrapper(DatasetWrapperTask, law.WrapperTask):

    version = None

    def requires(self):
        return OrderedDict(
            (dataset.name, ExpressionCheck.req(self, dataset_name=dataset.name))
            for dataset in self.datasets
        )


class MergeCategorizationStatsWrapper(law.tasks.RunOnceTask, DatasetCategoryWrapperTask):

    max_file_size = luigi.Parameter(default="250MB", description="the desired maximum file size "
        "per dataset after merging, default unit is MB, default: 250MB")
    min_train_fraction = luigi.FloatParameter(default=0.75, description="the desired minimum "
        "fraction of training events, default: 0.75")
    max_train_events = luigi.IntParameter(default=125000, description="the desired maximum number "
        "of training events, default: 125,000")
    table_format = luigi.Parameter(default="fancy_grid", significant=False, description="the "
        "tabulate table format, default: fancy_grid")

    def atomic_requires(self, dataset, category):
        return MergeCategorizationStats.req(self, dataset_name=dataset.name,
            category_name=category.name)

    @law.tasks.RunOnceTask.complete_on_success
    def run(self):
        import tabulate

        inputs = self.input()

        merging_data = OrderedDict()
        splitting_data = OrderedDict()

        # highlight helper
        def hl(val, condition, color="red"):
            color = color if condition else "default"
            return law.util.colored(val, color=color)

        for category in self.categories:
            # load the stats
            stats = OrderedDict([
                (dataset_name, inp.load(formatter="json"))
                for (dataset_name, category_name), inp in inputs.items()
                if category_name == category.name
            ])

            # print a table including merging info
            headers = ["Dataset", "Total events", "Selected events", "Efficiency / %",
                "'train' fraction", "Input files", "Broken files", "Total size", "Mean size",
                "Merged Files"]
            rows = []

            max_file_size = int(law.util.parse_bytes(self.max_file_size, input_unit="MB"))
            for dataset_name, stats in stats.items():
                dataset = self.config.datasets.get(dataset_name)
                sizes = stats["output_sizes"]
                sum_sizes = float(sum(sizes))
                mean_size = sum_sizes / len(sizes)
                n_merged = int(math.ceil(sum_sizes / max_file_size))
                sel_frac = 0.
                if dataset.is_data:
                    train_frac = "-"
                elif stats["counts"]["n_selected"] == 0:
                    train_frac = self.min_train_fraction
                else:
                    train_frac = 1. - float(self.max_train_events) / stats["counts"]["n_selected"]
                    train_frac = max(self.min_train_fraction, train_frac)
                if stats["counts"]["n_total"] > 0:
                    sel_frac = float(stats["counts"]["n_selected"]) / stats["counts"]["n_total"]

                rows.append([
                    dataset_name,
                    stats["counts"]["n_total"],
                    stats["counts"]["n_selected"],
                    round(100. * sel_frac, 1),
                    hl(train_frac, train_frac != 0.75, "cyan"),
                    len(sizes),
                    hl(stats["n_broken"], stats["n_broken"] > 0),
                    law.util.human_bytes(sum_sizes, fmt=True),
                    law.util.human_bytes(mean_size, fmt=True),
                    hl(n_merged, n_merged > 1, "magenta"),
                ])

                # fill merging and splitting data
                merging_data.setdefault(dataset, OrderedDict())[category.name] = n_merged
                if not dataset.is_data and train_frac != self.min_train_fraction:
                    splitting_data.setdefault(dataset, OrderedDict())[category.name] = train_frac

            # produce and print the table
            print("\nStats for category '{}':".format(category.name))
            print(tabulate.tabulate(rows, headers=headers, tablefmt=self.table_format,
                floatfmt=".2f", stralign="right"))

        # print merging and splitting data that goes into the config
        for dataset in merging_data:
            has_merging = any(v > 1 for v in merging_data[dataset].values())
            has_splitting = dataset in splitting_data
            if not has_merging and not has_splitting:
                continue
            print("\n{}:".format(dataset.name))
            if has_merging:
                print("    merging:")
                seen = []
                for category_name, n_merged in merging_data[dataset].items():
                    if category_name in seen or n_merged == 1:
                        continue
                    if category_name.endswith(("_even", "_odd")):
                        name = category_name.rsplit("_", 1)[0]
                        seen.extend([name + "_even", name + "_odd"])
                        print("        \"{1}\": {0}, \"{2}\": {0},".format(n_merged, *seen[-2:]))
                    else:
                        seen.append(category_name)
                        print("        \"{1}\": {0},".format(n_merged, category_name))
            if has_splitting:
                print("    spliting:")
                seen = []
                for category_name, train_frac in splitting_data[dataset].items():
                    if category_name in seen:
                        continue
                    train_frac = round(train_frac, 2)
                    if category_name.endswith(("_even", "_odd")):
                        name = category_name.rsplit("_", 1)[0]
                        seen.extend([name + "_even", name + "_odd"])
                        print("        \"{1}\": {0:.2f}, \"{2}\": {0:.2f},".format(
                            train_frac, *seen[-2:]))
                    else:
                        seen.append(category_name)
                        print("        \"{1}\": {0:.2f},".format(train_frac, category_name))
            print("\n" + 80 * "-")


class ShowMCStats(law.tasks.RunOnceTask, DatasetWrapperTask):

    category_names = law.CSVParameter(default=("baseline",), description="names of categories "
        "for which MC statistics are shown, default: (baseline,)")
    table_format = luigi.Parameter(default="fancy_grid", significant=False, description="the "
        "tabulate table format, default: fancy_grid")

    def __init__(self, *args, **kwargs):
        super(ShowMCStats, self).__init__(*args, **kwargs)

        # get categories, map them to their leaf categories (or itself if it has none)
        self.categories = OrderedDict()
        for name in self.category_names:
            category = self.config.categories.get(name)
            if category.x("composite", False):
                self.categories[category] = list(category.get_leaf_categories())
            else:
                self.categories[category] = [category]

    def requires(self):
        # get all unique required categories
        categories = set(sum(list(self.categories.values()), []))
        return OrderedDict(
            ((dataset.name, category.name), MergeCategorizationStats.req(self,
                dataset_name=dataset.name, category_name=category.name))
            for dataset, category in itertools.product(self.datasets, categories)
        )

    @law.tasks.RunOnceTask.complete_on_success
    def run(self):
        import tabulate

        # prepare the table
        headers = ["MC events"] + [dataset.name for dataset in self.datasets]
        rows = []

        # fill rows
        inputs = self.input()
        for category, leaf_categories in self.categories.items():
            row = [category.name] + [0] * len(self.datasets)
            for leaf_category in leaf_categories:
                for i, dataset in enumerate(self.datasets):
                    stats = inputs[(dataset.name, leaf_category.name)].load(formatter="json")
                    row[i + 1] += stats["counts"]["n_selected"]
            rows.append(row)

        # produce and print the table
        print(tabulate.tabulate(rows, headers=headers, tablefmt=self.table_format,
            stralign="right"))


class ShowDNNYields(law.tasks.RunOnceTask, ConfigTaskWithCategory, DatasetWrapperTask,
        CrossTrainingWrapper):

    merged = luigi.BoolParameter(default=False, description="when True, use the merged dnn mpp "
        "rules, default: False")
    mpp = luigi.BoolParameter(default=True, description="when False, show numbers without mpp "
        "categorization as a cross check, default: True")
    show_entries = luigi.BoolParameter(default=False, description="when True, show the raw entries "
        "rather than weighted yields, default: False")
    table_format = luigi.Parameter(default="fancy_grid", significant=False, description="the "
        "tabulate table format, default: fancy_grid")

    allow_composite_category = True

    def __init__(self, *args, **kwargs):
        super(ShowDNNYields, self).__init__(*args, **kwargs)

        if self.mpp:
            self.feature_tags = ["dnn_output{}_mpp".format("_merged" if self.merged else "")]
        else:
            self.feature_tags = ["dnn_output", "dnn_output_merged"]

    def requires(self):
        return CrossTrainingOutputPlot.req(self, feature_tags=self.feature_tags, stack=True,
            do_qcd=True, hide_data=True, save_yields=True,
            _prefer_cli=["do_qcd", "hide_data", "blinded"])

    @law.tasks.RunOnceTask.complete_on_success
    def run(self):
        import tabulate

        # string formatting based on table format
        if self.table_format == "pipe":
            fmt = lambda s: s.replace("_", r"\_")
        else:
            fmt = lambda s: s

        # prepare orders of dnn categories and plot processes
        mpp_features = [
            feature for feature in self.config.features
            if feature.has_tag(self.feature_tags)
        ]
        process_names = ["hh_ggf", "hh_vbf", "tth", "tt_lep", "tt_fh", "dy", "qcd", "data"]
        scales = {} if self.show_entries else {"hh_ggf": 3, "hh_vbf": 3}

        # prepare the table
        cat_name = self.category_name
        if self.region:
            cat_name += "_" + self.region.name
        title = "Entries" if self.show_entries else "Yields"
        headers = ["{} '{}'".format(title, cat_name)]
        for process_name in process_names:
            header = process_name
            if process_name in scales:
                header = "{} (x1e{})".format(header, scales[process_name])
            headers.append(fmt(header))
        headers.append("Sum")
        rows = []

        # fill rows
        inputs = self.input()["yields"]
        for mpp_feature in mpp_features:
            if mpp_feature.name not in inputs.targets:
                continue
            yields = inputs.targets[mpp_feature.name].load(formatter="json")
            row = [fmt(mpp_feature.name)]
            for process_name in process_names:
                # entries cannot be determined properly for QCD
                if process_name == "qcd" and self.show_entries:
                    row.append(0)
                    continue
                p = getattr(pc, "process_" + process_name)
                for yield_process_name, yield_data in yields.items():
                    yp = getattr(pc, "process_" + yield_process_name)
                    if yp == p or p.has_process(yp):
                        value = yields[yp.name]["entries" if self.show_entries else "value"]
                        value *= 10.**scales.get(process_name, 0)
                        row.append(int(value) if self.show_entries else value)
                        break
                else:
                    raise Exception("process '{}' not found in yields".format(process_name))
            row.append(sum(row[1:]))
            rows.append(row)
        rows.append(["Sum"] + [
            sum(row[i] for row in rows)
            for i in range(1, len(process_names) + 2)
        ])

        # produce and print the table
        print(tabulate.tabulate(rows, headers=headers, tablefmt=self.table_format,
            floatfmt=".2f", stralign="right"))
