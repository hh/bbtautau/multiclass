# coding: utf-8

"""
Plotting tasks.
"""

__all__ = []


import random
import itertools
from collections import OrderedDict

import law
import luigi

from hmc.tasks.base import (
    ConfigTaskWithCategory, DatasetTaskWithCategory, DatasetWrapperTask, HTCondorWorkflow,
)
from hmc.tasks.preprocessing import MergeCategorization, MergeCategorizationStats
from hmc.tasks.training import TrainingTask, MultiSeedTrainingTask
from hmc.tasks.evaluation import EvaluateTraining, EvaluateData, FeatureRanking
from hmc.util import import_root, extract_branch_names, get_expression_infos


class FeaturePlot(ConfigTaskWithCategory, DatasetWrapperTask, law.WrapperTask):

    feature_names = law.CSVParameter(default=(), description="names of features to plot, uses all "
        "features when empty, default: ()")
    feature_tags = law.CSVParameter(default=(), description="list of tags for filtering features "
        "selected via feature_names, default: ()")
    skip_feature_names = law.CSVParameter(default=(), description="names or name pattern of "
        "features to skip, default: ()")
    skip_feature_tags = law.CSVParameter(default=("dnn_output",), description="list of tags of "
        "features to skip, default: (dnn_output,)")
    parallelize = luigi.IntParameter(default=law.NO_INT, description="when set, the features to "
        "plot are split into chunks of this size triggered by multiple tasks, default: empty")
    stack = luigi.BoolParameter(default=False, description="when set, stack backgrounds, weight "
        "them with dataset and category weights, and normalize afterwards, default: False")
    do_qcd = luigi.BoolParameter(default=False, description="whether to compute the QCD shape, "
        "default: False")
    hide_data = luigi.BoolParameter(default=True, description="hide data events, default: True")
    blinded = luigi.BoolParameter(default=False, description="whether to blind bins above a "
        "certain feature value, default: False")
    save_pdf = luigi.BoolParameter(default=True, description="whether to save created histograms "
        "in pdf, default: True")
    save_root = luigi.BoolParameter(default=False, description="whether to save created histograms "
        "in root files, default: False")
    save_yields = luigi.BoolParameter(default=True, description="whether to save event yields per "
        "process, default: True")
    process_group_name = luigi.Parameter(default="default", description="the name of the process "
        "grouping, only encoded into output paths when changed, default: default")

    default_process_group_name = "default"
    tree_name = "HTauTauTree"
    allow_composite_category = True

    def __init__(self, *args, **kwargs):
        super(FeaturePlot, self).__init__(*args, **kwargs)

        if not self.save_pdf and not self.save_root and not self.save_yields:
            raise Exception("either save_pdf, save_root or save_yields must be True")

        # get QCD regions when requested
        self.qcd_regions = None
        if self.do_qcd:
            self.qcd_regions = self.config.helpers.get_qcd_regions(self.region, self.category)

            # complain when no data is present
            if not any(dataset.is_data for dataset in self.datasets):
                raise Exception("no real dataset passed for QCD estimation")

        # select features
        self.features = []
        for feature in self.config.features:
            if self.feature_names and not law.util.multi_match(feature.name, self.feature_names):
                continue
            if self.feature_tags and not feature.has_tag(self.feature_tags):
                continue
            if self.skip_feature_names and \
                    law.util.multi_match(feature.name, self.skip_feature_names):
                continue
            if self.skip_feature_tags and feature.has_tag(self.skip_feature_tags):
                continue
            self.features.append(feature)

        self.is_wrapper = self.parallelize != law.NO_INT

    def complete(self):
        if self.is_wrapper:
            return law.WrapperTask.complete(self)

        return ConfigTaskWithCategory.complete(self)

    def _repr_flags(self):
        flags = super(FeaturePlot, self)._repr_flags()
        if not self.is_wrapper and "wrapper" in flags:
            flags.remove("wrapper")
        return flags

    def requires(self):
        if self.is_wrapper:
            dataset_names = [dataset.name for dataset in self.datasets]
            feature_names = [feature.name for feature in self.features]
            random.shuffle(feature_names)
            return [
                self.req(self, dataset_names=tuple(dataset_names), dataset_tags=(),
                    skip_dataset_names=(), skip_dataset_tags=(), feature_names=chunk,
                    feature_tags=(), skip_feature_names=(), skip_feature_tags=(),
                    parallelize=law.NO_INT)
                for chunk in law.util.iter_chunks(feature_names, self.parallelize)
            ]

        reqs = {}
        if self.do_qcd:
            reqs["qcd"] = {
                key: self.req(self, region_name=region.name, blinded=False, hide_data=False,
                    do_qcd=False, stack=True, save_root=True, save_pdf=False, save_yields=False)
                for key, region in self.qcd_regions.items()
            }

        reqs["data"] = OrderedDict(
            ((dataset.name, category.name), MergeCategorization.vreq(self,
                dataset_name=dataset.name, category_name=category.name,
                _prefer_cli=["version"]))
            for dataset, category in itertools.product(self.datasets, self.expand_category())
        )

        if self.stack:
            reqs["stats"] = OrderedDict(
                ((dataset.name, category.name), MergeCategorizationStats.vreq(self,
                    dataset_name=dataset.name, category_name=category.name,
                    _prefer_cli=["version"]))
                for dataset, category in itertools.product(self.datasets, self.expand_category())
            )

        return reqs

    def get_output_postfix(self, feature, key="pdf"):
        postfix = ""
        if self.process_group_name != self.default_process_group_name:
            postfix += "__pg_" + self.process_group_name
        if self.do_qcd:
            postfix += "__qcd"
        if self.hide_data:
            postfix += "__nodata"
        elif self.blinded and key not in ("root", "yields"):
            postfix += "__blinded"
        if self.region:
            postfix += "__" + self.region.name
        if self.stack and key not in ("root", "yields"):
            postfix += "__stack"
        return postfix

    def output(self):
        if self.is_wrapper:
            return []

        # output definitions, i.e. key, file prefix, extension
        output_data = []
        if self.save_pdf:
            output_data.append(("pdf", "", "pdf"))
        if self.save_root:
            output_data.append(("root", "root/", "root"))
        if self.save_yields:
            output_data.append(("yields", "yields/", "json"))

        return {
            key: law.SiblingFileCollection(OrderedDict(
                (feature.name, self.local_target("{}{}{}.{}".format(
                    prefix, feature.name, self.get_output_postfix(feature, key), ext)))
                for feature in self.features
            ))
            for key, prefix, ext in output_data
        }

    def get_category_text(self, feature):
        lines = [s.strip() for s in self.category.label_root.split(",") if s.strip()]
        if self.region and not self.region.x("signal_region", False):
            lines.extend([s.strip() for s in self.region.label_root.split(",") if s.strip()])
        return lines

    def get_top_right_text(self, feature):
        return self.config.helpers.get_run_text()

    def get_data_range(self, feature):
        return (-1.e7, 1.e7)

    def get_binning(self, feature, inputs=None):
        return feature.binning

    @law.decorator.notify
    @law.decorator.safe_output
    def run(self):
        if self.is_wrapper:
            return law.WrapperTask.run(self)

        ROOT = import_root()
        from order.util import join_root_selection as jrs
        from hmc.plots import feature_plot
        from hmc.config.processes import process_qcd

        # collect processes to plot
        process_mapping, processes, _ = self.config.helpers.get_dataset_process_mapping(
            self.datasets, self.process_group_name)

        # create root tchains for inputs
        inputs = self.input()
        tchains = OrderedDict((process.name, []) for process in processes)
        for (dataset, process), category in itertools.product(
                process_mapping.items(), self.expand_category()):
            inp = inputs["data"][(dataset.name, category.name)]

            # inp is allowed to be either the output of a workflow or a target collection
            if isinstance(inp, dict):
                targets = list(inp["collection"].targets.values())
            else:
                targets = inp.targets

            # create and fill the tchain
            tchain = ROOT.TChain()
            for target in targets:
                tchain.Add(target.path + "/" + self.tree_name)

            # store the region selection, also add the category selection when use base categories
            # when using base categories, store the actual selection in the tchain
            selection = []
            if self.region:
                selection.append(self.region.selection)
            if self.use_base_category:
                selection.append(category.selection)
            tchain.hmc_selection = jrs(selection, op="and") if selection else ""

            # create the tchain weight, composed of dataset and category weight,
            # and the normalization weight when stacking
            weight = "1"
            if not dataset.is_data:
                evt_den = None
                if self.stack:
                    stats = inputs["stats"][(dataset.name, category.name)].load(formatter="json")
                    evt_den = stats["evt_den"]
                weight = self.config.helpers.get_dataset_category_weight(dataset, category, evt_den)
            tchain.hmc_weight = weight

            # make reading from the tchains faster by enabling only branches that we need
            expressions = [get_expression_infos(feature.expression)[1] for feature in self.features]
            expressions.extend([feature.selection for feature in self.features])
            expressions.append(tchain.hmc_selection)
            expressions.append(tchain.hmc_weight)
            try:
                branches = extract_branch_names(expressions, tchain)
            except:
                print("cannot extract branches from input file {}".format(target.path))
                raise
            tchain.SetBranchStatus("*", False)
            for branch in branches:
                tchain.SetBranchStatus(branch, True)

            tchains[process.name].append(tchain)

        # prepare legend titles and colors
        titles = []
        colors = []
        signal_names = []
        data_names = []
        for process in processes:
            titles.append(process.label_root)
            colors.append(ROOT.TColor.GetColor(*process.color))
            if process.x("is_signal", False):
                signal_names.append(process.name)
            elif process.is_data:
                data_names.append(process.name)

        # create plots
        outputs = self.output()
        progress = self.create_progress_callback(len(self.features))
        for i, feature in enumerate(self.features):
            self.publish_message("plot {}".format(feature.name))

            # qcd shape files
            qcd_shape_files = None
            if self.do_qcd:
                qcd_shape_files = {
                    key: inputs["qcd"][key]["root"].targets[feature.name].path
                    for key in self.qcd_regions
                }

            # get the binning
            binning = self.get_binning(feature, inputs=inputs)
            even_binning = isinstance(binning, tuple)

            # define the y axis title
            y_title = feature.get_full_y_title(root=True, bin_width=None if even_binning else False)
            if not self.stack:
                y_title = y_title.replace("Events", "Normalized events")

            # use output localization (i.e. create tmp outputs and move them to the actual location
            # on context closing) as writing to root files on EOS is buggy when there is too much
            # time between opening, writing to, and eventually closing the file
            _out = {key: outputs[key].targets[feature.name] for key in outputs}
            with law.localize_file_targets(_out, "w") as out:
                feature_plot(
                    tchains,
                    feature.name,
                    feature.expression,
                    binning,
                    pdf_output_path=out["pdf"].path if self.save_pdf else None,
                    root_output_path=out["root"].path if self.save_root else None,
                    yields_output_path=out["yields"].path if self.save_yields else None,
                    feature_selection=feature.selection,
                    default=feature.x.default,
                    missing_values=feature.x.missing,
                    x_title=feature.get_full_x_title(root=True),
                    y_title=y_title,
                    x_labels=feature.x_labels_root,
                    x_log=feature.log_x,
                    y_log=feature.log_y,
                    y_min=feature.get_aux("y_min", None),
                    is_flag=feature.x("is_flag", False),
                    legend_titles=titles,
                    colors=colors,
                    show_underflow=feature.get_aux("underflow", False),
                    show_overflow=feature.get_aux("overflow", False),
                    stack=self.stack,
                    signal_names=signal_names,
                    data_names=data_names,
                    hide_data=self.hide_data,
                    data_range=self.get_data_range(feature),
                    qcd_shape_files=qcd_shape_files,
                    qcd_title=process_qcd.label_root,
                    qcd_color=ROOT.TColor.GetColor(*process_qcd.color),
                    normalize_signals_before_feature_selection=True,
                    show_signal_scale="hh_vbf_bsm" not in signal_names,
                    category_text=self.get_category_text(feature),
                    top_right_text=self.get_top_right_text(feature),
                )
            progress(i)


class TrainingPerformancePlot(ConfigTaskWithCategory, MultiSeedTrainingTask):

    splits = law.CSVParameter(default=("valid",), description="the dataset splits to evaluate, "
        "possible values are 'train', 'valid' and 'test', default: (valid,)")

    # regions not supported
    region_name = None

    allow_composite_category = True

    def __init__(self, *args, **kwargs):
        super(TrainingPerformancePlot, self).__init__(*args, **kwargs)

        # validate splits
        for split in self.splits:
            if split not in ["train", "valid", "test"]:
                raise Exception("unknown split: {}".format(split))
        if len(self.splits) != len(set(self.splits)):
            raise Exception("duplicate splits received: {}".format(self.splits))

    def requires(self):
        return {
            split: EvaluateTraining.req(self, split=split, _prefer_cli=["version"])
            for split in self.splits
        }

    def output(self):
        targets = {}
        for split in self.splits:
            targets[split + "_confusion"] = self.local_target(split + "_confusion.pdf")
            targets[split + "_roc"] = self.local_target(split + "_roc.pdf")
        return law.SiblingFileCollection(targets)

    @law.decorator.notify
    def run(self):
        ROOT = import_root()
        import numpy as np
        from hmc.plots import confusion_plot, multi_roc_plot

        inputs = self.input()
        outputs = self.output()
        outputs.dir.touch()

        for split in self.splits:
            # load stats
            stats = inputs[split].load(formatter="json")
            process_names = stats["process_names"]

            # prepare some plot configs
            processes = map(self.training_config.processes.get, process_names)
            colors = [ROOT.TColor.GetColor(*process.color) for process in processes]
            titles = [process.label_root for process in processes]

            # confusion matrix
            counts = np.array([stats["classifications"][p] for p in process_names])
            top_right_text = "#scale[0.8]{{T: '{}', D: '{}' ({})}}".format(
                self.training_category.name, self.category.name, split)
            confusion_plot(outputs[split + "_confusion"].path, counts, process_names,
                top_right_text=top_right_text)

            # multi-roc plot
            aucs = [stats["roc_aucs"][p] for p in process_names]
            fprs = np.array([stats["roc_fprs"][p] for p in process_names])
            tprs = np.array([stats["roc_tprs"][p] for p in process_names])
            top_right_text = "Training category '{}'".format(self.training_category.name)
            category_text = [s.strip() for s in self.category.label_root.split(",")]
            category_text.append("'{}' data".format(split))
            multi_roc_plot(outputs[split + "_roc"].path, fprs, tprs, process_names, aucs=aucs,
                inverted=True, y_max=1e5, legend_titles=titles, colors=colors,
                category_text=category_text, top_right_text=top_right_text)


class TrainingOutputPlotBase(FeaturePlot):

    _default_feature_tags = (
        "dnn_output", "dnn_output_merged", "dnn_output_mpp", "dnn_output_merged_mpp",
    )

    feature_tags = law.CSVParameter(default=_default_feature_tags, description="list of tags of "
        "dnn output features to plot, default: {}".format(_default_feature_tags))
    bin_opt_version = luigi.Parameter(default=law.NO_STR, description="version of the binning "
        "optimization task to use, not used when empty, default: empty")
    bin_opt_n_start_bins = luigi.IntParameter(default=10, description="the number of bins to start "
        "with when running the bin optimization, default: 10")
    n_bins = luigi.IntParameter(default=law.NO_INT, description="custom number of bins for "
        "plotting, defaults to the value configured by the feature when empty, default: empty")

    feature_names = ()
    skip_feature_names = ()
    skip_feature_tags = ()
    parallelize = law.NO_INT

    training_config_name = "default"
    tree_name = "evaluation"
    allow_composite_category = True

    def __init__(self, *args, **kwargs):
        super(TrainingOutputPlotBase, self).__init__(*args, **kwargs)

        # remove non dnn output features
        self.features = [
            feature for feature in self.features
            if feature.has_tag("dnn_output*")
        ]

        # remove dnn features whose process is not covered by the training processes
        self.features = [
            feature for feature in self.features
            if any(
                feature.x.process == proc or feature.x.process.has_process(proc)
                for proc in self.config.training[self.training_config_name].processes
            )
        ]

    def get_output_postfix(self, feature, key="pdf"):
        postfix = super(TrainingOutputPlotBase, self).get_output_postfix(feature, key=key)
        if self.bin_opt_version != law.NO_STR and key in ("pdf", "root"):
            postfix += "__binopt_{}_n{}".format(self.bin_opt_version, self.bin_opt_n_start_bins)
        if self.n_bins != law.NO_INT and key in ("pdf", "root"):
            postfix += "__nbins_" + str(self.n_bins)
        return postfix

    def get_category_text(self, dnn_feature):
        text = FeaturePlot.get_category_text(self, dnn_feature)
        if dnn_feature.x("mpp", False):
            text.append("'{}' class".format(dnn_feature.x.process.name))
        return text

    def get_data_range(self, feature):
        if feature.x.process.x("is_signal", False) and self.blinded:
            return (-1.e7, 0.5)
        else:
            return super(TrainingOutputPlotBase, self).get_data_range(feature)

    def get_binning(self, feature, inputs=None):
        binning = super(TrainingOutputPlotBase, self).get_binning(feature)
        # priotize bin_opt_version over n_bins
        if self.bin_opt_version != law.NO_STR:
            assert(bool(inputs))
            binning = inputs["bin_opt"].targets[feature.name].load(formatter="json")

        elif self.n_bins != law.NO_INT:
            assert(isinstance(binning, tuple))
            assert(len(binning) == 3)
            binning = (self.n_bins,) + binning[1:]

        return binning


class TrainingOutputPlot(TrainingOutputPlotBase, MultiSeedTrainingTask):

    def requires(self):
        reqs = TrainingOutputPlotBase.requires(self)

        # require EvaluateData but do not pass region_name as this is applied during plotting
        reqs["data"] = OrderedDict(
            ((dataset.name, category.name), EvaluateData.req(self, dataset_name=dataset.name,
                category_name=category.name, _exclude=["region_name"], _prefer_cli=["version"]))
            for dataset, category in itertools.product(self.datasets, self.expand_category())
        )

        # add binning optimization
        if self.bin_opt_version != law.NO_STR:
            from hmc.tasks.analysis import BinningOptimization
            # when any region is set, require the bin opt task in the signal region
            signal_region_name = law.NO_STR
            if self.region:
                signal_region_name = self.config.helpers.get_signal_region(self.region).name
            reqs["bin_opt"] = BinningOptimization.req(self, version=self.bin_opt_version,
                region_name=signal_region_name, n_start_bins=self.bin_opt_n_start_bins,
                _prefer_cli=["region_name"])

        return reqs


class CrossTrainingOutputPlot(TrainingOutputPlotBase):

    training_id_even = luigi.IntParameter(description="id of the training on even event numbers")
    training_id_odd = luigi.IntParameter(description="id of the training on odd event numbers")

    def __init__(self, *args, **kwargs):
        super(CrossTrainingOutputPlot, self).__init__(*args, **kwargs)

        # the category must not be even or od
        if self.category.x.is_eo:
            raise Exception("the category must not be even or odd")

        # get the even and odd categories
        self.category_even = self.config.helpers.get_child_eo_category(self.category, "even")
        self.category_odd = self.config.helpers.get_child_eo_category(self.category, "odd")

    def store_parts(self):
        parts = super(CrossTrainingOutputPlot, self).store_parts()
        parts["training_ids"] = "tids_{}_{}".format(self.training_id_even, self.training_id_odd)
        return parts

    def expand_category(self, category=None):
        if category is not None:
            raise NotImplementedError
        else:
            return [self.category_even, self.category_odd]

    def requires(self):
        reqs = TrainingOutputPlotBase.requires(self)

        # require EvaluateData but do not pass region_name as this is applied during plotting
        # and mix category and training id regarding even/odd event numbers
        reqs["data"] = OrderedDict()
        category_training_mix = [
            (self.category_even, self.training_id_odd),
            (self.category_odd, self.training_id_even),
        ]
        for category, training_id in category_training_mix:
            for dataset in self.datasets:
                reqs["data"][(dataset.name, category.name)] = EvaluateData.req(self,
                    dataset_name=dataset.name, category_name=category.name, training_id=training_id,
                    _exclude=["region_name"], _prefer_cli=["version"])

        # add binning optimization
        if self.bin_opt_version != law.NO_STR:
            from hmc.tasks.analysis import CrossBinningOptimization
            # when any region is set, require the bin opt task in the signal region
            signal_region_name = law.NO_STR
            if self.region:
                signal_region_name = self.config.helpers.get_signal_region(self.region).name
            reqs["bin_opt"] = CrossBinningOptimization.req(self, version=self.bin_opt_version,
                region_name=signal_region_name, n_start_bins=self.bin_opt_n_start_bins,
                _prefer_cli=["region_name"])

        return reqs


class TrainingOutputCorrelationPlot(DatasetTaskWithCategory, MultiSeedTrainingTask):

    feature_names = law.CSVParameter(description="names of features to plot in correlation with "
        "the DNN outputs")
    dnn_feature_tag = luigi.ChoiceParameter(default="dnn_output", choices=["dnn_output",
        "dnn_output_mpp"], description="the tag of the dnn features to use, default: dnn_output")
    matching_output_only = luigi.BoolParameter(default=False, description="when true, only create "
        "the plots with the dnn output that corresponds to the requested dataset, default: False")
    normalize = luigi.ChoiceParameter(default="none", choices=("none", "x", "x_max", "y",
        "y_max"), description="the normalization mode, default: none")

    # regions not supported
    region_name = None

    tree_name = "HTauTauTree"
    dnn_tree_name = "evaluation"
    allow_composite_category = True

    def __init__(self, *args, **kwargs):
        super(TrainingOutputCorrelationPlot, self).__init__(*args, **kwargs)

        # select features
        self.features = [self.config.features.get(name) for name in self.feature_names]

        # dnn output features
        self.dnn_features = [
            feature for feature in self.config.features
            if (feature.has_tag(self.dnn_feature_tag)
                and (not self.matching_output_only or feature.x.process == self.process))
        ]
        if not self.dnn_features:
            raise Exception("no dnn feature found")

    def requires(self):
        return {
            "data": OrderedDict(
                (category.name, MergeCategorization.vreq(self, category_name=category.name,
                    _prefer_cli=["version"]))
                for category in self.expand_category()
            ),
            "dnn": OrderedDict(
                (category.name, EvaluateData.req(self, category_name=category.name,
                    _prefer_cli=["version"]))
                for category in self.expand_category()
            ),
        }

    def output(self):
        norm = "" if self.normalize == "none" else "__norm_{}".format(self.normalize)
        gen = itertools.product(self.features, self.dnn_features)
        return law.SiblingFileCollection({
            (feature.name, dnn_feature.name): self.local_target("{}__{}{}.pdf".format(
                feature.name, dnn_feature.name, norm))
            for feature, dnn_feature in gen
        })

    @law.decorator.notify
    def run(self):
        ROOT = import_root()
        from hmc.plots import correlation_plot
        from order.util import join_root_selection

        # create root tchains for both data and dnn outputs
        inputs = self.input()

        tchain = ROOT.TChain("data")
        for coll in inputs["data"].values():
            for target in coll.targets:
                tchain.Add(target.path + "/" + self.tree_name)

        dnn_tchain = ROOT.TChain("dnn")
        for inp in inputs["dnn"].values():
            for target in inp["collection"].targets.values():
                dnn_tchain.Add(target.path + "/" + self.dnn_tree_name)

        # befriend them
        tchain.AddFriend(dnn_tchain)

        # toggle branches in the tchain that are not needed
        tchain.SetBranchStatus("*", False)
        expressions = [feature.expression for feature in self.features]
        expressions += [feature.selection for feature in self.features]
        for branch in extract_branch_names(expressions, tchain):
            tchain.SetBranchStatus(branch, True)
        dnn_expressions = [feature.expression for feature in self.dnn_features]
        dnn_expressions += [feature.selection for feature in self.dnn_features]
        for branch in extract_branch_names(dnn_expressions, dnn_tchain):
            tchain.SetBranchStatus(branch, True)

        # create plots
        outputs = self.output()
        outputs.dir.touch()
        normalize = False if self.normalize == "none" else self.normalize
        top_right_text = "#scale[0.8]{{{}, {}}}".format(self.process.label_root,
            self.category.label_root)
        progress = self.create_progress_callback(len(outputs))
        gen = itertools.product(self.features, self.dnn_features)

        for i, (feature, dnn_feature) in enumerate(gen):
            key = (feature.name, dnn_feature.name)
            expression = "dnn.{}:{}".format(dnn_feature.expression, feature.expression)
            dnn_selection = dnn_feature.selection.replace("dnn_", "dnn.dnn_")
            selection = join_root_selection([feature.selection, dnn_selection], op="and")

            correlation_plot(outputs[key].path, tchain, expression, feature.binning,
                dnn_feature.binning, selection=selection, normalize=normalize,
                x_title=feature.get_full_x_title(root=True), x_labels=feature.x_labels_root,
                y_title=dnn_feature.get_full_x_title(root=True), y_labels=dnn_feature.x_labels_root,
                top_right_text=top_right_text)

            progress(i)


class ConfusionPlotBase(ConfigTaskWithCategory, DatasetWrapperTask):

    merged = luigi.BoolParameter(default=False, description="whether merged dnn output nodes "
        "should be used, default: False")
    do_qcd = luigi.BoolParameter(default=False, description="whether to include QCD accuracies "
        "based on data in the os_inviso region, default: False")
    hide_data = luigi.BoolParameter(default=False, description="hide data events, default: False")

    allow_composite_category = True

    @property
    def process_group_name(self):
        return "confusion_merged" if self.merged else "confusion"

    def get_default_dataset_names(self):
        _, _, datasets = self.config.helpers.get_dataset_process_mapping(self.config.datasets,
            self.process_group_name)
        return [dataset.name for dataset in datasets]

    def output(self):
        postfix = ""
        if self.merged:
            postfix += "__merged"
        if self.do_qcd:
            postfix += "__qcd"
        if self.hide_data:
            postfix += "__nodata"
        if self.region:
            postfix += "__" + self.region.name

        return self.local_target("confusion{}.pdf".format(postfix))

    @law.decorator.notify
    @law.decorator.safe_output
    def run(self):
        import_root()
        import numpy as np
        import hmc.config.processes as pc
        from hmc.plots import confusion_plot

        # collect processes to plot
        _, processes, _ = self.config.helpers.get_dataset_process_mapping(
            self.datasets, self.process_group_name)

        # hide data when requested
        if self.hide_data:
            processes = [p for p in processes if not p.is_data]

        # when doing qcd, inject the qcd process right before the first data process
        if self.do_qcd:
            r_qcd = min([len(processes)] + [r for r, p in enumerate(processes) if p.is_data])
            processes.insert(r_qcd, pc.process_qcd)

        # get dnn mpp features corresponding to training processes
        inputs = self.input()
        mpp_features = [
            self.config.features.get(feature_name)
            for feature_name in inputs["yields"].targets
        ]

        # read yields and errors
        inputs = self.input()
        yields = np.zeros((len(processes), len(mpp_features)), np.float32)
        errors = np.zeros((len(processes), len(mpp_features)), np.float32)
        for j, mpp_feature in enumerate(mpp_features):
            inp = inputs["yields"].targets[mpp_feature.name]
            data = inp.load(formatter="json")
            for i, process in enumerate(processes):
                yields[i, j] = data[process.name]["value"]
                errors[i, j] = data[process.name]["error"]

        # prepare plot options
        process_labels = [process.label_root for process in processes]
        class_labels = [mpp_feature.x.process.label_root for mpp_feature in mpp_features]
        # top_right_text = "#scale[0.85]{{Training: {}, Data: {}{}, {}}}".format(
        #     self.training_category.name, self.category.name,
        #     " ({})".format(self.region.name) if self.region else "", self.config.year)
        top_right_text = self.config.helpers.get_run_text()

        # actual plot
        output = self.output()
        output.parent.touch()
        confusion_plot(output.path, (yields, errors), process_labels, class_labels,
            top_right_text=top_right_text)


class ConfusionPlot(ConfusionPlotBase, MultiSeedTrainingTask):

    def requires(self):
        dataset_names = [dataset.name for dataset in self.datasets]
        feature_tags = ("dnn_output_merged_mpp",) if self.merged else ("dnn_output_mpp",)
        return TrainingOutputPlot.req(self, dataset_names=dataset_names, feature_tags=feature_tags,
            process_group_name=self.process_group_name, stack=True, blinded=False, hide_data=False,
            save_pdf=False, save_root=False, save_yields=True,
            _prefer_cli=["blinded", "save_pdf", "save_root"],
        )


class CrossConfusionPlot(ConfusionPlotBase):

    training_id_even = luigi.IntParameter(description="id of the training on even event numbers")
    training_id_odd = luigi.IntParameter(description="id of the training on odd event numbers")

    def store_parts(self):
        parts = super(CrossConfusionPlot, self).store_parts()
        parts["training_ids"] = "tids_{}_{}".format(self.training_id_even, self.training_id_odd)
        return parts

    def requires(self):
        dataset_names = [dataset.name for dataset in self.datasets]
        feature_tags = ("dnn_output_merged_mpp",) if self.merged else ("dnn_output_mpp",)
        return CrossTrainingOutputPlot.req(self, dataset_names=dataset_names,
            feature_tags=feature_tags, process_group_name=self.process_group_name, stack=True,
            blinded=False, hide_data=False, save_pdf=False, save_root=False, save_yields=True,
            _prefer_cli=["blinded", "save_pdf", "save_root"],
        )


class FeatureRankingPlot(ConfigTaskWithCategory, TrainingTask):

    feature_names = FeatureRanking.feature_names
    split = FeatureRanking.split

    # regions not supported
    region_name = None

    def requires(self):
        return FeatureRanking.req(self, _prefer_cli=["version"])

    def output(self):
        return law.SiblingFileCollection({
            process_name: self.local_target("ranking__{}.pdf".format(process_name))
            for process_name in list(self.training_config.processes.names()) + ["mean"]
        })

    @law.decorator.notify
    def run(self):
        from hmc.plots import plot_feature_ranking

        # prepare inputs and outputs
        inputs = self.input()
        outputs = self.output()
        outputs.dir.touch()

        # to get the features to actually test, we are lazy here and use that fact that
        # the FeatureRanking task already did that
        features_to_test = self.requires().features_to_test

        # obtain the mean score and the scores per process
        mean_scores = []
        scores_per_process = OrderedDict()
        for feature, target in zip(features_to_test, inputs["collection"].targets.values()):
            try:
                scores = target.load(formatter="json")
            except:
                print("scores for feature {} could not be loaded from file {}".format(
                    feature.name, target.path))
                raise
            mean_scores.append((feature.name, scores["mean_score"]))
            for process in self.training_config.processes:
                score = scores["scores"][process.name]
                scores_per_process.setdefault(process, []).append((feature.name, score))

        # plot the mean
        mean_scores = OrderedDict(sorted(mean_scores, key=lambda tpl: tpl[1]))
        plot_feature_ranking(mean_scores.keys(), mean_scores.values(), outputs["mean"].path)

        # plot per process
        for process, scores in scores_per_process.items():
            scores = OrderedDict(sorted(scores, key=lambda tpl: tpl[1]))
            plot_feature_ranking(scores.keys(), scores.values(), outputs[process.name].path)
