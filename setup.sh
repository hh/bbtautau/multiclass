#!/usr/bin/env bash

action() {
    #
    # global variables
    #

    # determine the directory of this file
    local this_file="$( [ ! -z "$ZSH_VERSION" ] && echo "${(%):-%x}" || echo "${BASH_SOURCE[0]}" )"
    local this_dir="$( cd "$( dirname "$this_file" )" && pwd )"
    export HMC_BASE="$this_dir"

    # check if this setup script is sourced by a remote job
    if [ "$HMC_ON_HTCONDOR" = "1" ]; then
        export HMC_REMOTE_JOB="1"
    else
        export HMC_REMOTE_JOB="0"
    fi

    # check if we're on lxplus
    if [[ "$( hostname )" = lxplus*.cern.ch ]]; then
        export HMC_ON_LXPLUS="1"
    else
        export HMC_ON_LXPLUS="0"
    fi

    # default cern name
    if [ -z "$HMC_CERN_USER" ]; then
        if [ "$HMC_ON_LXPLUS" = "1" ]; then
            export HMC_CERN_USER="$( whoami )"
        else
            2>&1 echo "please set HMC_CERN_USER to your CERN user name and try again"
            return "1"
        fi
    fi

    # default data directory
    if [ -z "$HMC_DATA" ]; then
        if [ "$HMC_ON_LXPLUS" = "1" ]; then
            export HMC_DATA="$HMC_BASE/data"
        else
            # TODO: better default when not on lxplus
            export HMC_DATA="$HMC_BASE/data"
        fi
    fi

    # other defaults
    [ -z "$HMC_SOFTWARE" ] && export HMC_SOFTWARE="$HMC_DATA/software"
    [ -z "$HMC_STORE_LOCAL" ] && export HMC_STORE_LOCAL="$HMC_DATA/store"
    [ -z "$HMC_STORE_EOS" ] && export HMC_STORE_EOS="/eos/user/${HMC_CERN_USER:0:1}/$HMC_CERN_USER/hmc"
    [ -z "$HMC_STORE" ] && export HMC_STORE="$HMC_STORE_EOS"
    [ -z "$HMC_JOB_DIR" ] && export HMC_JOB_DIR="$HMC_DATA/jobs"
    [ -z "$HMC_TMP_DIR" ] && export HMC_TMP_DIR="$HMC_DATA/tmp"
    [ -z "$HMC_CMSSW_BASE" ] && export HMC_CMSSW_BASE="$HMC_DATA/cmssw"
    [ -z "$HMC_SCRAM_ARCH" ] && export HMC_SCRAM_ARCH="slc7_amd64_gcc820"
    [ -z "$HMC_CMSSW_VERSION" ] && export HMC_CMSSW_VERSION="CMSSW_11_1_0_pre4"
    [ -z "$HMC_PYTHON_VERSION" ] && export HMC_PYTHON_VERSION="2"

    # specific eos dirs
    [ -z "$HMC_STORE_EOS_CATEGORIZATION" ] && export HMC_STORE_EOS_CATEGORIZATION="$HMC_STORE_EOS"
    [ -z "$HMC_STORE_EOS_SHARDS" ] && export HMC_STORE_EOS_SHARDS="$HMC_STORE_EOS"

    # create some dirs already
    mkdir -p "$HMC_TMP_DIR"


    #
    # helper functions
    #

    hmc_pip_install() {
        if [ "$HMC_PYTHON_VERSION" = "2" ]; then
            env pip install --ignore-installed --no-cache-dir --upgrade --prefix "$HMC_SOFTWARE" "$@"
        else
            env pip3 install --ignore-installed --no-cache-dir --upgrade --prefix "$HMC_SOFTWARE" "$@"
        fi
    }
    export -f hmc_pip_install

    hmc_add_py() {
        export PYTHONPATH="$1:$PYTHONPATH"
    }
    export -f hmc_add_py

    hmc_add_bin() {
        export PATH="$1:$PATH"
    }
    export -f hmc_add_bin

    hmc_add_lib() {
        export LD_LIBRARY_PATH="$1:$LD_LIBRARY_PATH"
    }
    export -f hmc_add_lib


    #
    # minimal software stack
    #

    # add this repo to PATH and PYTHONPATH
    hmc_add_bin "$HMC_BASE/bin"
    hmc_add_py "$HMC_BASE"

    # variables for external software
    export GLOBUS_THREAD_MODEL="none"
    export HMC_GFAL_DIR="$HMC_SOFTWARE/gfal2"
    export GFAL_PLUGIN_DIR="$HMC_GFAL_DIR/lib/gfal2-plugins"

    # certificate proxy handling
    [ "$HMC_REMOTE_JOB" != "1" ] && export X509_USER_PROXY="/tmp/x509up_u$( id -u )"

    # software that is used in this project
    hmc_setup_software() {
        local origin="$( pwd )"
        local mode="$1"

        # remove software directories when forced
        if [ "$mode" = "force" ] || [ "$mode" = "force_cmssw" ]; then
            echo "remove CMSSW checkout in $HMC_CMSSW_BASE/$HMC_CMSSW_VERSION"
            rm -rf "$HMC_CMSSW_BASE/$HMC_CMSSW_VERSION"
        fi

        if [ "$mode" = "force" ] || [ "$mode" = "force_py" ]; then
            echo "remove software stack in $HMC_SOFTWARE"
            rm -rf "$HMC_SOFTWARE"
        fi

        if [ "$mode" = "force" ] || [ "$mode" = "force_gfal" ]; then
            echo "remove gfal installation in $HMC_GFAL_DIR"
            rm -rf "$HMC_GFAL_DIR"
        fi

        # setup cmssw
        export SCRAM_ARCH="$HMC_SCRAM_ARCH"
        source "/cvmfs/cms.cern.ch/cmsset_default.sh" ""
        if [ ! -d "$HMC_CMSSW_BASE/$HMC_CMSSW_VERSION" ]; then
            echo "setting up $HMC_CMSSW_VERSION at $HMC_CMSSW_BASE"
            mkdir -p "$HMC_CMSSW_BASE"
            cd "$HMC_CMSSW_BASE"
            scramv1 project CMSSW "$HMC_CMSSW_VERSION"
        fi
        cd "$HMC_CMSSW_BASE/$HMC_CMSSW_VERSION/src"
        eval `scramv1 runtime -sh`
        cd "$origin"

        # get the python version
        if [ "$HMC_PYTHON_VERSION" = "2" ]; then
            local pyv="$( python -c "import sys; print('{0.major}.{0.minor}'.format(sys.version_info))" )"
        else
            local pyv="$( python3 -c "import sys; print('{0.major}.{0.minor}'.format(sys.version_info))" )"
        fi

        # ammend software paths
        hmc_add_bin "$HMC_SOFTWARE/bin"
        hmc_add_py "$HMC_SOFTWARE/lib/python$pyv/site-packages:$HMC_SOFTWARE/lib64/python$pyv/site-packages"

        # setup custom software
        if [ ! -d "$HMC_SOFTWARE" ]; then
            echo "installing software stack at $HMC_SOFTWARE"
            mkdir -p "$HMC_SOFTWARE"

            hmc_pip_install pip
            hmc_pip_install six
            hmc_pip_install tabulate
            hmc_pip_install python-telegram-bot==12.7
            hmc_pip_install flake8
            hmc_pip_install luigi==2.8.13
            hmc_pip_install git+https://github.com/riga/plotlib
            hmc_pip_install git+https://github.com/riga/order
            hmc_pip_install git+https://github.com/riga/scinum
            hmc_pip_install --no-deps git+https://github.com/riga/law
            hmc_pip_install --no-deps git+https://github.com/riga/LBN
            hmc_pip_install --no-deps gast==0.2.2  # https://github.com/tensorflow/autograph/issues/1
            hmc_pip_install --no-deps cryptography==2.9.2
        fi

        # gfal python bindings
        hmc_add_bin "$HMC_GFAL_DIR/bin"
        hmc_add_py "$HMC_GFAL_DIR/lib/python2.7/site-packages"
        hmc_add_lib "$HMC_GFAL_DIR/lib"

        if [ ! -d "$HMC_GFAL_DIR" ]; then
            local lcg_base="/cvmfs/grid.cern.ch/centos7-ui-4.0.3-1_umd4v3/usr"
            if [ ! -d "$lcg_base" ]; then
                2>&1 echo "LCG software directory $lcg_base not existing"
                return "1"
            fi

            mkdir -p "$HMC_GFAL_DIR"
            (
                cd "$HMC_GFAL_DIR"
                mkdir -p include bin lib/gfal2-plugins lib/python2.7/site-packages
                ln -s "$lcg_base"/include/gfal2* include
                ln -s "$lcg_base"/bin/gfal-* bin
                ln -s "$lcg_base"/lib64/libgfal* lib
                ln -s "$lcg_base"/lib64/gfal2-plugins/libgfal* lib/gfal2-plugins
                ln -s "$lcg_base"/lib64/python2.7/site-packages/gfal* lib/python2.7/site-packages
                cd lib/gfal2-plugins
                rm libgfal_plugin_http.so libgfal_plugin_xrootd.so
                curl https://cernbox.cern.ch/index.php/s/qgrogVY4bwcuCXt/download > libgfal_plugin_xrootd.so
            )
        fi
    }
    export -f hmc_setup_software

    # setup the software initially when no explicitly skipped
    if [ "$HMC_SKIP_SOFTWARE" != "1" ]; then
        if [ "$HMC_FORCE_SOFTWARE" = "1" ]; then
            hmc_setup_software force
        else
            hmc_setup_software silent
        fi
    fi


    #
    # law setup
    #

    export LAW_HOME="$HMC_DATA/law"
    export LAW_CONFIG_FILE="$HMC_BASE/law.cfg"
    [ -z "$HMC_SCHEDULER_PORT" ] && export HMC_SCHEDULER_PORT="80"
    if [ -z "$HMC_LOCAL_SCHEDULER" ]; then
        if [ -z "$HMC_SCHEDULER_HOST" ]; then
            export HMC_LOCAL_SCHEDULER="True"
        else
            export HMC_LOCAL_SCHEDULER="False"
        fi
    fi
    if [ -z "$HMC_LUIGI_WORKER_KEEP_ALIVE" ]; then
        if [ "$HMC_REMOTE_JOB" = "0" ]; then
            export HMC_LUIGI_WORKER_KEEP_ALIVE="False"
        else
            export HMC_LUIGI_WORKER_KEEP_ALIVE="False"
        fi
    fi

    # try to source the law completion script when available
    which law &> /dev/null && source "$( law completion )" ""
}
action "$@"
