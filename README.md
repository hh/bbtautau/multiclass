# HH → bb𝝉𝝉 multiclass classifier

### Setup

This package requires a minimal software stack that provides common tools such as [TensorFlow](https://pypi.org/project/tensorflow), [scikit-learn](https://pypi.org/project/scikit-learn/)/[-optimize](https://pypi.org/project/scikit-optimize), and optionally [law](https://pypi.org/project/law) for running the preprocessing pipeline.

There are two ways to setup the package.


#### 1. On lxplus

When you are on lxplus, a lot of software already ships with the latest CMSSW version, e.g. [TensorFlow 2.1 out-of-the-box](https://github.com/cms-sw/cmssw/pull/28711). To setup the package on top of CMSSW 11, run

```shell
source setup.sh
```

which installs software *once* and defines a few environment variables, prefixed with `HMC_` (check them out via `env | grep -P "^HMC_"`)


#### 2. With a custom software stack

In case your machine already provides all software, run

```shell
HMC_SKIP_SOFTWARE=1 source setup.sh
```

and the script will only define a few environment variables that are currently required for the preprocessing pipeline.


### Training

The training scripts are located in [hmc/training](hmc/training). All python files are placed in the [hmc](hmc) directory so that they can be imported from elsewhere via (e.g.) `from hmc import ...`.

Currently, there is only a single training script [hmc/training/training.py](hmc/training/training.py) that uses keras models and layers for the network definition, and pure TensorFlow for the training loop, providing more flexibility than just doing `model.fit` the keras-way.

Commands to run the training and to create plots can be found on the [Wiki page](https://gitlab.cern.ch/hh/bbtautau/multiclass/-/wikis/home).
